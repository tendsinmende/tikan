

![Icon](resources/IconTikan64.png "Tikan Icon")
# Tikan

The engine is build from several sub crates that are all related to each other in a way.

- tikan: the main engine
- tikan-client: is a client binary that can load an application and connect it to the engine. It also handles window creation.
- tikan-editor: is a editor bianry that should be used when building stuff with the engine.
- tikan-shader: compiles tikan's shader and provides them to the engine.
- tikan-ecs: an entity-component abstraction over game objects

## Dependencies
The engine uses several base librarys, the biggest ones are mentioned here:

- Vulkan-API: uses [Marp](https://gitlab.com/Siebencorgie/marp) which is an wrapper I wrote around [ash](https://github.com/MaikKlein/ash)
- Widkan: And widget-toolkit I wrote based on Marp as well and winit
- [Winit](https://crates.io/crates/winit) for window creation, hidden as a public crate within Widkan.
- [Ron](https://crates.io/crates/ron) for configuration files and similar things.

## Configuration
The tikan engine and its helper crates, the editor and the client are useing the [RustyObjectNotation](https://crates.io/crates/ron) to save the configuration.
The engine will save it to a `engine.ron` file, the editor to an `editor.ron` etc.

## Branches
The `master` branch will always compile on the newest rust stable release and the given dependencies.
The `dev` branch might compile but can also be broken from time to time.

## Building and Running
Before trying to run or build anything make sure that your graphics card supports the Vulkan API and appropriate drivers are installed.

Building:
From the Git-root directory issue
``` Bash
cargo build
```
for a debug build, or if you want performance and speed do
``` Bash
cargo build --release
```

If you want to use the editor run

``` Bash
cargo run --bin tikan-editor --release
```

Move the camera by holding right-click + WASD/EQ.

**NOTE 1**
The binary file that gets loaded on editor start is currently hard coded in `tikan-editor/src/main.rs`. You can create binary files by converting them via the converter from gltf files and then specify them in the `main.rs`.

**NOTE 2**
The client is currently not able to start any baked games, this messages will be replaced when it's ready.

## Converting gltf2 files
There is a small [description](tikan-converter/usage.md) on how to convert **almost** and gltf2 file to the binary format which can be loaded by the engine.

The exporter loads, voxelizes and stores the gltf2 file into some binary file. Note that this can take some time if the resolution exceeds 1024³.

## Note on the tikan-shader crate
This crate uses shaderc-rs to compile all shaders needed by the engine and, when used as a library is able to supply the byte-code.

To use that crate you might have to setup [shaderc's dependencies](https://github.com/google/shaderc-rs) in order to use it. However, this repository will usually have up to date SPIR-V bytecode so this usually won't be necessary.

## Design and Ideas
I wrote down some ideas on what I plan to do with this engine in [this design file](tikan/design.md).
I already wrote another "bigger" hobby graphics engine called [jakar-engine](https://gitlab.com/Siebencorgie/jakar-engine). However, after exploring triangle raytracing there I developed the idea to build my on voxel engine to follow at least two hypes at once:

- Voxels
- Ray Tracing
- currently a component hierachy system that borrows some ideas from ECS systems

Let's see if it works!

## Almost recent visuals

![](tikan/resources/TikanGifGi3.gif "Light play between two stationary and one dynamic emitting voxel sphere")
![](tikan/resources/TikanGate.png "Some other perspective on the gate")

## Current TODOs
- [x] Implement assemble pass
- [x] Robust transition for the final raytracing image to the graphics queue and back
- [x] Empty Triangle pass (currently clears to a 1.0 depth and alpha of 0.0 which should mean "nothing")
- [x] Implement Raytracing -> Triangle merge pass
- [x] Implement Camera Buffer and updates
- [x] Add camera to primary ray
- [x] Create basic debug pipeline including the camera.
- [x] Basic camera movement for tests (later to be split into entity and components)
- [x] Integrate depth based drawing in the debug pass
- [x] Actual scene handling
- [x] Basic, accelerated Voxels
- [x] Robust raytracing of the voxels in an pass agnostic way 
- [x] Implement GBuffer Generation
- [x] Write importer
- [x] Optimize importer for low memory usage
- [x] Implement smaller gpu and cpu voxel
- [x] Find good way to mark and handle emissive voxels
- [x] Implement radiance pass (Single cone tracing or single raytrace at first)
- [x] Implement specular (Single cone tracing)
- [x] Implement GI (several voxels possibly, experiment with more then one thread per pixel (maybe one per cone?))
- [x] Merge primary-pass, specular, radiance and GI into a PBR result
- [x] Check performance
- [x] (From here if ok, else try to make it somehow better) (Is not okay, going down the BVH route)
- [x] Top level, scene object based BVH
- [x] Voxel object binary format for faster loading
- [ ] Implement Triangle pass and blending for debug objects
- [x] Implement Physical based camera
- [x] actual Tone-Mapping
- [ ] Denoising (there is a denoiser, but its not really good atm)
- [ ] Secondary Shadows and maybe reflections


- [ ] IF NEEDED: Custom acceleration structure
- [ ] Ideas for acceleration in general: Optimize memory barriers, optimize image formats, early out raytracing
- [ ] GPU based voxelation of objects. Maybe in another application which then stores the object binary on disk.

- [ ] MUCH LATER: Editor workflow with the levels
- [ ] MUCH LATER: App backing, voxel storing in binary format
- [ ] MUCH LATER: App client

- [ ] EVEN LATER: Test app (some sandbox building stuff maybe)
