# Textures and origin

- LDR_RGBA_0.png: is a 4 channel blue noise texture used when sampling rays and other stuff that needs to be random but uniform. Taken from [here](http://momentsingraphics.de/BlueNoise.html)