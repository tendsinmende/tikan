# Tikan-Engine
## Design ideas and goals
Here I collect mostly ideas I have before implementing them

### Architecture
- Split project into:
  - engine
  - client (game/application)
  - editor
- The engine exposes various function that can be used by the editor and scripts to manipulate the engine or the content it is rendering.
- Check how "Data Driven" concepts can apply within the engine.

### Rendering
- Use AMDs vulkan fast-path examples (one big descriptor set, explicit resource transition etc)
- Use bvh to trace an object on top level, then use a SVO/DAG or something else on the bottom level
- Use a material lookup texture to save material information about voxels?
- For lights, create emitter BVH which can be raytraced to find "interesting lights", also lights could be made out of voxels (no point/spot lights anymore, only emitting voxels)
- Use physical lights in lumen/lux. Calculate approximate influence radius by letting it fall off faster after a certain threshold.
- When raytracing, also generate a depth image. If needed editor elements or other stuff could be blended into the output.
- Use TAA as anti aliasing and cheap denoiser
- Later, might switch to a dedicated denoiser for the reflections pass. Since we know the primary rays intersection location one could denoise the shaded reflection specially.

#### Rendering performance ideas
- In reflections do not trace until bottom of objects accel. structure (may be given through cone tracing which does not trace till the bottom anyways?)
- Only single shadow rays for reflections
- Compress voxel data (normals)
- Optimize Image transitions (as specially access_flags)
- Beam-optimization (do a depth pre pass in this case only on the object bvh, then use the found point as ray origin)

#### First frame passes idea:
1. (re) generate dynamic octrees / bvh's, this setp later after the main part is working. Have to find out if that can be started while tonemapping etc. is done on the last frame.
2. Primary ray (stores: [Position, Depth Albedo, normal, roughness, metallness]*)
3. Specular lighting based on Gbuffer info via cone tracing (Single cone). Evaluate simple light per intersection for shadowed reflections.
4. Radiance (all contributing light to a given location in the gbuffer)
5. GI/Indicrect light (at first a constant, later maybe VCT-GI with 16 cones?)
6. Take Radiance, Gi/Indirect_light and specular lighting to assemble final image.
8. Do TAA
9. Do ToneMapping and physical-camera adjustments


*Will be three rgba textures like that:

[normal.x | normal.y | normal.z | camera-depth] as rgba_f32

[albedo.r | albedo.g | albedo.b | roughness ] as rgba_unorm

[metallness | unused | unused | unused ] as rgba_sint NOTE: The unused parts can be used for debug data in dev and later be reduced to a single channel image.

I'm not sure about the best format though.

#### Indirect light
I currently don't include indirect light (at least not ray traced). However, this should be easy to add to the reflection pass, or as a seperate
pass.

#### Voxel cone tracing
I decided to use voxel cone tracing for most lighting. Ray-Tracing is still used to generate the gbuffer,
However, cone tracing works better for GI/reflections and shadows.

Since I everage the leaf voxel data for the parents anyways, the cone tracing actually becomes cheaper, the further away the cone is from the point of origin.

Cone tracing Algorithm Notes:
1. Calculate all cones that are needed (several for gi, one wide one for reflections (width based on roughness) and one for the shadows (not sure here how to construct it currently))
2. step along the direction, with each step, widen the intersection primitive (in our case probably a cube or sphere)
3. for each step accumulate the final outcome weighted by the distance from the origin.
4. when ray if compleatly occluded, too far or meets some other criteria, break

##### articles: 
https://prantare.files.wordpress.com/2016/12/frepr183_voxel_cone_tracing.pdf
https://research.nvidia.com/sites/default/files/publications/GIVoxels-pg2011-authors.pdf


### Voxel related ideas
- order the voxels "depth first search" into the buffer, only save a "skip_index" per voxel, if it does not intersect.
  the index should point to the next, parent level voxel that can itersect
- use contours per voxel to not need that many level for a smooth look
- in the last merge (for primary ray + reflections/shadow) do a little smoothing on the result

### Resource management and creation
- Import triangle models, trace them in one direction and find intersections, fill models with intermediate color similar to bresenham algorithm (?)
- Store Voxelized models in custom binary format for fast loading.
- use ECS, probably new one to make BVH creation fast.
- Render UI on top via separate framework (`widkan`)
- make materials object based. Since there are no planes anymore textures will be useless. Instead specify volumes with a certain material. The material is a description of many objects and "fillers" that then populate the volume.
- allow simulation kernels for volumes like water, fog. Let them run at a specifies speed which can be slower then the frames.
- gpu based physics (mostly collision at first), later maybe voxel to voxel interaction (tires going through sand, boat in the water etc.)

### other stuff
- Animate voxels via weights as well which transform a input voxels structure in a pre pass, then generate acceleration structure and trace the outcome.
- animate voxels by bending rays in model space based on bones?
- Try out ray-traced audio
- Try out simultaneous editing in the editor by several parties
