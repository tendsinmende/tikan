/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use ron::de::from_reader;
use ron::ser::{to_string_pretty, PrettyConfig};
use serde::Deserialize;
use serde::Serialize;
use std::fs::{File, OpenOptions};
use std::io::{Error, Write};

use marp::miscellaneous::Version;

use marp::ash::vk;

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub struct TVersion {
    pub major: usize,
    pub minor: usize,
    pub patch: usize,
}

impl TVersion {
    pub fn to_marp_version(&self) -> Version {
        Version::new(self.major, self.minor, self.patch)
    }
}

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq)]
///When in release the engine will be faster since no debug information will be processed and no uneeded
///extensions and layers are loaded.
pub enum EngineMode {
    Release,
    Debug,
}

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq)]
///Describes how the buffering of several frames should be handled.
pub enum BufferingMethode {
    Single,
    Double,
    Triple,
}

impl BufferingMethode {
    pub fn to_count(&self) -> usize {
        match self {
            BufferingMethode::Single => 1,
            BufferingMethode::Double => 2,
            BufferingMethode::Triple => 3,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq)]
///Describes which presenting technique should be used. Consider using mailbox when
/// double or triple buffering is enabled.
pub enum PresentingMethode {
    ///Images get submitted to a queue and at each VSync the next one is taken. This is the only,
    /// always supported methode.
    Vsync,
    ///When a frame is finished, the screen gets updated, sometimes causes screen tearing.
    Immediate,
    ///The screen gets updated with the newest image.
    Mailbox,
}

impl PresentingMethode {
    pub fn to_methode(&self) -> vk::PresentModeKHR {
        match self {
            PresentingMethode::Vsync => vk::PresentModeKHR::FIFO,
            PresentingMethode::Immediate => vk::PresentModeKHR::IMMEDIATE,
            PresentingMethode::Mailbox => vk::PresentModeKHR::MAILBOX,
        }
    }
}

///The Config data
#[derive(Serialize, Deserialize, Clone)]
pub struct Config {
    ///The file this config was read from and will be saved to if wanted.
    pub path: String,

    pub engine_mode: EngineMode,
    pub in_editor: bool,

    ///Is set by the engine while starting.
    ///if set, the engine is running in a debugger and can use the
    /// `DEBUG_MAKRER` extension while recording command buffers.
    pub in_debugger: bool,

    ///If set, no extensions that are not supported by renderdoc are loaded.
    pub support_renderdoc: bool,

    ///The application name provided to the driver
    pub application_name: String,
    ///The application version
    pub app_version: TVersion,
    ///The API version of vulkan that shall be used
    pub vulkan_version: TVersion,

    pub used_physical_device_index: usize,
    pub device_extensions: Vec<String>,
    ///How many swapchain images should be allocated if possible.
    /// This does not influence the double/trible or single buffering.
    pub swapchain_image_count: usize,

    pub buffering_methode: BufferingMethode,
    ///Describes how swapchain updates should be handled
    pub swapchain_update_methode: PresentingMethode,
    ///Describes how many voxels MAX are stored in gpu memory.
    pub voxel_cache_size: usize,

    ///The fov angle of the default camera, might be removed in favor of a camera component.
    pub camera_fov: f32,
    ///The max depth that get's rendered by the camera.
    pub camera_depth: f32,
    ///The Ray-Tracing quality. A lower value traces the scene deeper, but costs more in terms of time/compute-power.
    pub tracing_quality_threshold: f32,
    ///Cone start distance: When too small, self intersection artifacts will appear. A good value
    /// is 2.0
    pub cone_start: f32,
    ///The furthest distance a cone gets traced before it is aboarded. higher values make the
    /// outcome nicer but also cost more if the distance gets too big. A good value is 1000.0
    pub cone_max_length: f32,
    ///The minimal aperture of the cone before it get clamped. Smaller values mean sharper reflections but cost way more. The default value is 1.0.
    ///
    ///WARNING: Never use 0.0, this will lock the gpu in an endless loop
    pub cone_min_aperture: f32,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            path: String::from("engine_config.txt"),
            application_name: String::from("TikanApplication"),
            app_version: TVersion {
                major: 0,
                minor: 1,
                patch: 0,
            },
            vulkan_version: TVersion {
                major: 1,
                minor: 1,
                patch: 0,
            },
            engine_mode: EngineMode::Debug,

            in_editor: true,
            in_debugger: false,

            support_renderdoc: true,

            used_physical_device_index: 0,
            device_extensions: vec![String::from("VK_KHR_swapchain")],
            swapchain_image_count: 2,

            buffering_methode: BufferingMethode::Single,
            swapchain_update_methode: PresentingMethode::Vsync,
            voxel_cache_size: 33554432, // 2^25 voxels for now which is around 2.7Gb on the gpu

            camera_fov: 90.0,
            camera_depth: 500.0,
            tracing_quality_threshold: 0.01,
            cone_start: 0.01,
            cone_max_length: 1000.0,
            cone_min_aperture: 3.0,
        }
    }
}

impl Config {
    //Saves the Config to some path
    pub fn save(&self) -> Result<(), Error> {
        let mut pretty = PrettyConfig::default();
        pretty.depth_limit = 3;
        pretty.separate_tuple_members = true;
        pretty.enumerate_arrays = true;

        let mut self_copy = self.clone();
        //Never save in_debugger info
        self_copy.in_debugger = false;

        let final_string = to_string_pretty(&self_copy, pretty).expect("Failed to encode config!");

        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .open(self.path.clone())?;
        write!(file, "{}", final_string)?;
        Ok(())
    }

    //Loads a config from some file
    pub fn load(path: &str) -> Self {
        let file = match File::open(path) {
            Ok(f) => f,
            Err(e) => {
                println!(
                    "Could not find file at {} with er: {}, loading defaults!",
                    path, e
                );
                let mut default = Config::default();
                default.path = String::from(path);
                return default;
            }
        };
        let config: Config = match from_reader(file) {
            Ok(c) => c,
            Err(e) => {
                println!(
                    "Failed to parse config at {} with err: {}, loading defaults!",
                    path, e
                );
                Config::default()
            }
        };
        config
    }
}
