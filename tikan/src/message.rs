/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::data::{
    components::Transform, data_manager::DataManager, event_state::KeyStates, scene::EntityId, *,
};
use na;
use std::sync::{mpsc::*, Arc, RwLock};
use tikan_ecs::*;

pub enum RenderMessage {
    ///Changes the quality at which is raytraced
    ChangeRenderQuality(f32),
    ///Changes distance from origin at which the cone starts to remove self intersection artefacts.
    ChangeConeStart(f32),
    ///Changes the furthest distance the cone traces
    ChangeConeMaxLength(f32),
    ///Changes the smallest angle the aperture can become.
    ChangeConeMinAperture(f32),
}

pub enum ObjectMessage {
    TransformEntity {
        id: EntityId,
        trans: Transform,
    },
    ///Imports a voxel object in binary format at the given location.
    ImportBin(String),
    ///Converts a model at the given location with the set options
    ConvertGltf {
        location: String,
        resolution: u32,
        sampling_count: usize,
    },
    SetAperture(f32),
    SetIso(f32),
    SetShutterSpeed(f32),
    Job(Box<dyn FnOnce(&DataManager) + Send + Sync>),

    RegisterService(Arc<RwLock<dyn Service + Send + Sync>>),

    Dummy,
}

#[derive(Clone)]
pub struct Messenger {
    to_render: Sender<RenderMessage>,
    to_object_manager: Sender<ObjectMessage>,
}

impl Messenger {
    ///Creates a new messenger system. Returns the receiver for (rendering, object_manager) as well as the system itself.
    ///
    /// # Note
    /// Never create the system by your self, instead use `engine.get_messenger()` to receive a correctly wired version.
    pub fn new() -> (
        (Receiver<RenderMessage>, Receiver<ObjectMessage>),
        Messenger,
    ) {
        let (render_send, render_recv) = channel::<RenderMessage>();
        let (obj_send, obj_recv) = channel::<ObjectMessage>();

        let msg = Messenger {
            to_render: render_send,
            to_object_manager: obj_send,
        };

        ((render_recv, obj_recv), msg)
    }

    pub fn send_obj_message(&self, msg: ObjectMessage) {
        self.to_object_manager
            .send(msg)
            .expect("Failed to send object message!");
    }

    pub fn send_render_message(&self, msg: RenderMessage) {
        self.to_render
            .send(msg)
            .expect("Failed to send message to renderer!");
    }
}

/*
///A reoccurring job that is executed on every data-manager update
pub struct Service{
    job: Box<dyn Fn(&DataManager, std::time::Duration, &KeyStates, &[Event]) + Send + Sync>,
}

impl Service{
    ///Creates a new service which executes the supplied job on each update.
    ///
    /// #Arguments:
    ///
    /// 1. is the data manager which can be used for accessing any engine data.
    ///
    /// 2. is the time that passed since the last update.
    ///
    /// 3. a map of currently pressed keys
    ///
    /// 4. a set of events that happened since the last update
    pub fn new<F: 'static>(job: F) -> Self where F: Fn(&DataManager, std::time::Duration, &KeyStates, &[Event]), F: Send, F: Sync{
        Service{
            job: Box::new(job),
        }
    }

    ///Executes the job via the supplied data manager. This is usually called within the engine
    /// and should not be used outside of it.
    pub fn execute(&self, data_manager: &DataManager, time: std::time::Duration, key_states: &KeyStates, events: &[Event]){
        (self.job)(data_manager, time, key_states, events)
    }
}
*/

pub trait Service {
    ///Is executed when the service is added to the data manager
    fn on_create(&mut self, data_manager: &DataManager) {}
    ///Is executed every time the data manager is updated.
    ///
    /// #Arguments:
    ///
    /// 1. is the data manager which can be used for accessing any engine data.
    ///
    /// 2. is the time that passed since the last update.
    ///
    /// 3. a map of currently pressed keys
    ///
    /// 4. a set of events that happened since the last update
    fn on_update(
        &mut self,
        data_manager: &DataManager,
        time: std::time::Duration,
        key_states: &KeyStates,
        events: &[Event<()>],
    ) {
    }

    ///Can be emitted when this job should not be run again.
    fn should_stop(&self) -> bool {
        false
    }
}
