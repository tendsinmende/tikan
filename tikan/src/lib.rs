/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

///Vulkan wrapper
pub extern crate marp;
use crate::data::data_manager::DataManager;
use marp::ash;
use marp::*;
use std::sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard};
use std::time::Instant;
///Linear algebra library
pub extern crate nalgebra as na;

///The config format we use.
pub extern crate ron;
///Serializing and deserializing the config data
pub extern crate serde;
///Used for interface rendering
pub extern crate widkan;
use widkan::msw::winit;

/// Provides us with paths to shader binaries.
extern crate tikan_shader;

///The (H)ECS used to organize scenes.
pub extern crate tikan_ecs;

///Abstract voxel saving structure
pub extern crate voxer;

///Everything related to loading and saving normal bitmaps.
pub extern crate image;

///The standart random crate used when generating random numbers
pub extern crate rand;

///Contains the engine configuration definition
pub mod config;
use config::*;

///Contains the messaging system with which the engine can be controlled at runtime
pub mod message;
pub use message::*;

///Contains rendering object related sub modules
pub mod render;
use render::*;
///Contains an object manager used for rendering and scene handling as well as sub modules related to renderable objects.
pub mod data;

//Reexports of often used things
pub use data::{
    camera::Camera,
    components::*,
    data_manager::duration_to_float_secs,
    event_state::KeyStates,
    triangle_object::MeshId,
    voxel_object::{VoxelObject, VoxelObjectId},
};

use std::collections::BTreeMap;
///The current engine version. TODO read from Cargo.toml?
pub const ENGINE_VERSION: TVersion = TVersion {
    major: 0,
    minor: 1,
    patch: 0,
};

#[derive(Clone, Copy, Debug)]
///Possible un-handleable engine errors
pub enum EngineError {
    InstanceCreationFailed,
    NoPhysicalDevice,
    NoQueue(device::queue::QueueType),
    DeviceCreation(ash::vk::Result),
}

#[derive(Clone, Copy, PartialEq)]
pub enum EngineState {
    Waiting,
    Running,
}

///An intermediate builder representation of the un-started engine. Can be used to retrieve
/// the engines vulkan-instance and to change the loaded config before systems
/// are started based on it.
pub struct EngineBuilder {
    config: Config,
    instance: Arc<instance::Instance>,
}

impl EngineBuilder {
    pub fn new<'a>(config_path: Option<&'a str>) -> Result<Self, EngineError> {
        let config = if let Some(conf) = config_path {
            Config::load(conf)
        } else {
            Config::default()
        };

        println!("Loaded version: {:#?}", config.vulkan_version);
        let app_info = miscellaneous::AppInfo::new(
            config.application_name.clone(),
            config.app_version.to_marp_version(),
            String::from("Tikan"),
            ENGINE_VERSION.to_marp_version(),
            config.vulkan_version.to_marp_version(),
        );

        let mut extensions = miscellaneous::InstanceExtensions::presentable();
        if config.engine_mode == EngineMode::Release {
            extensions.debug_utils = false;
        }

        //Blacklist all extensions that are not in renderdoc
        if config.support_renderdoc {
            println!("Creating without wayland support");
            extensions.wayland_surface = false;
        }

        if extensions.wayland_surface {
            println!("Starting with wayland support");
        }

        let layers = if config.engine_mode == EngineMode::Debug {
            miscellaneous::InstanceLayer::debug_layers()
        } else {
            vec![]
        };

        let instance = match instance::Instance::new(
            Some(app_info),
            Some(extensions),
            Some(layers), //TODO add layers again, but currently they are segfaulting on my system
            None,
        ) {
            Ok(i) => i,
            Err(e) => {
                println!("TIKAN: ERROR: Instance creation failed with: {:#?}", e);
                return Err(EngineError::InstanceCreationFailed);
            }
        };

        Ok(EngineBuilder { config, instance })
    }

    pub fn get_config(&mut self) -> &mut Config {
        &mut self.config
    }

    pub fn get_instance(&self) -> Arc<instance::Instance> {
        self.instance.clone()
    }

    ///Starts the engine. You need to provide a surface that is used for presenting the rendered image.
    /// If the surface changes call `update()` if you want to update the inner framebuffers to the new extent.
    /// Also provide the framebuffer extent (if you use winit call window.get_inner_size()).
    pub fn build(
        mut self,
        window: Arc<winit::window::Window>,
        surface: Arc<dyn swapchain::surface::Surface + Send + Sync>,
    ) -> Result<Engine, EngineError> {
        //Find a suitable physical device. Currently we use a fixed index that can be changed in the config.
        let physical_devices = match device::physical_device::PhysicalDevice::find_physical_device(
            self.instance.clone(),
        ) {
            Ok(ps) => ps,
            Err(e) => {
                println!("TIKAN: ERROR: Could not find physical device: {:#?}", e);
                return Err(EngineError::NoPhysicalDevice);
            }
        };

        if self.config.engine_mode == EngineMode::Debug {
            println!(
                "TIKAN: DEBUG: Found {} physical devices!",
                physical_devices.len()
            );
        }

        let final_physical_device: Arc<device::PhysicalDevice> = match physical_devices
            .get(self.config.used_physical_device_index)
        {
            Some(pd) => pd.clone(),
            None => {
                //Try to fall back to the first one
                if let Some(apd) = physical_devices.get(0) {
                    println!(
                        "Could not find physical device at index {}, falling back to first one at [0]",
                        self.config.used_physical_device_index
                    );
                    apd.clone()
                } else {
                    println!("Failed to get any physical device!");
                    return Err(EngineError::NoPhysicalDevice);
                }
            }
        };

        // Since we got a physical_device, create our logical device as well as all queue we need.
        let queue_families = final_physical_device.get_queue_families();

        if self.config.engine_mode == EngineMode::Debug {
            println!("Queue families:");
            for q in queue_families.iter() {
                println!(
                    "\t index: {}, type: {:?}",
                    q.get_family_index(),
                    q.get_queue_type()
                );
            }
        }

        let graphics_family = if let Some(final_queue) =
            queue_families.iter().fold(None, |current, q| {
                if q.get_queue_type().graphics
                    && final_physical_device.can_present_on_surface(&surface, q.get_family_index())
                {
                    Some(q)
                } else {
                    current
                }
            }) {
            final_queue
        } else {
            println!("Could not find graphics and present capable queue family!");
            return Err(EngineError::NoQueue(device::queue::QueueType {
                graphics: true,
                compute: false,
                transfer: false,
            }));
        };

        let compute_family = if let Some(final_queue) =
            queue_families.iter().fold(None, |current, q| {
                if q.get_queue_type().compute && !q.get_queue_type().graphics {
                    Some(q)
                } else {
                    current
                }
            }) {
            final_queue
        } else {
            println!("Could not find raw compute capable queue family, searching for any that is capable!");

            if let Some(f) = queue_families.iter().fold(None, |current, q| {
                if q.get_queue_type().compute {
                    Some(q)
                } else {
                    current
                }
            }) {
                f
            } else {
                println!("Could not find any queue that is compute capable!");
                return Err(EngineError::NoQueue(device::queue::QueueType {
                    graphics: false,
                    compute: true,
                    transfer: false,
                }));
            }
        };

        let transfer_family = if let Some(final_queue) =
            queue_families.iter().fold(None, |current, q| {
                if q.get_queue_type().transfer
                    && !q.get_queue_type().compute
                    && !q.get_queue_type().graphics
                {
                    Some(q)
                } else {
                    current
                }
            }) {
            final_queue
        } else {
            println!("Could not find raw transfer capable queue family!");

            if let Some(f) = queue_families.iter().fold(None, |current, q| {
                if q.get_queue_type().transfer {
                    Some(q)
                } else {
                    current
                }
            }) {
                f
            } else {
                println!("Could not find any transfer capable queue family!");
                return Err(EngineError::NoQueue(device::queue::QueueType {
                    graphics: false,
                    compute: false,
                    transfer: true,
                }));
            }
        };

        //Since we can only allocate a certain amount of queues per family
        // we sort them. If there is for instance a family which only supports one queue, but
        // both, compute and transfer queue want to be created from that family, we sort this out here.

        //Fill in all queues based on familie.
        //Format:                    Family             queue_index  family weight
        let mut families: BTreeMap<device::QueueFamily, Vec<(usize, f32)>> = BTreeMap::new();
        for (idx, (fam, fam_weight)) in vec![
            (graphics_family, 0.8),
            (compute_family, 1.0),
            (transfer_family, 0.5),
        ]
        .into_iter()
        .enumerate()
        {
            if families.contains_key(&fam) {
                families.get_mut(&fam).unwrap().push((idx, fam_weight));
            } else {
                families.insert(*fam, vec![(idx, fam_weight)]);
            }
        }

        //now per family, create tubel where the queue_index points to the actual creation index, so we can later retrieve
        // the correct queue from the created queue.
        let mut weights_table: Vec<(device::QueueFamily, f32)> = Vec::new();
        let mut routeing_table: BTreeMap<usize, usize> = BTreeMap::new();
        let mut queue_index = 0;
        for (family, creation_indexes) in families.into_iter() {
            for (i, weight) in creation_indexes {
                if family.get_properties().queue_count <= i as u32 {
                    //Reroute to last index on this family, since it is already incremented
                    //we have to do the -1 here.
                    routeing_table.insert(i, queue_index - 1);
                } else {
                    //Push new weight and update family index
                    weights_table.push((family, weight));
                    routeing_table.insert(i, queue_index); //Ref self
                    queue_index += 1;
                }
            }
        }

        //Prepare the needed device extensions
        let mut device_extensions: Vec<_> = self
            .config
            .device_extensions
            .iter()
            .map(|ext| {
                miscellaneous::DeviceExtension::new(ext.clone(), 1) //TODO specify versions as well?
            })
            .collect();

        //Check if a debug_marker extension is present, if thats the case, the engine is running in a debugger and should add
        // the debug makers to the command buffers.
        for e in final_physical_device.get_extensions() {
            if e.get_name() == "VK_EXT_debug_marker".to_string() {
                println!("TIKAN: INFO: Running in Debugger, adding debug maker extension");
                device_extensions.push(miscellaneous::DeviceExtension::new(
                    "VK_EXT_debug_marker".to_string(),
                    1,
                ));
                self.config.in_debugger = true;
                break;
            }
        }

        //Print debug info when in debug mode
        if self.config.engine_mode == EngineMode::Debug {
            println!("Queue settings: ");
            println!("\tRoutes table: {:#?}", routeing_table);
            println!("\tWeights:");
            for (fam, weight) in weights_table.iter() {
                println!(
                    "\t\tindex: {:?}, weights: {:?}",
                    fam.get_family_index(),
                    weight
                );
            }
            println!("Device extensions: ");
            for ext in final_physical_device.get_extensions() {
                println!("\tName: {}", ext.get_name());
            }
            println!("Is in Debugger: {}", self.config.in_debugger);
        }

        let (device, queues) = match device::Device::new(
            self.instance,
            final_physical_device,
            weights_table,
            Some(&device_extensions),
            None, //TODO load from config as well?
        ) {
            Ok((dev, qs)) => (dev, qs),
            Err(e) => {
                println!("Failed to create logical device!");
                return Err(EngineError::DeviceCreation(e));
            }
        };

        let graphics_queue = queues[*routeing_table
            .get(&0)
            .expect("Could not find graphics queue route")]
        .clone();
        let compute_queue = queues[*routeing_table
            .get(&1)
            .expect("Could not find compute queue route")]
        .clone();
        let transfer_queue = queues[*routeing_table
            .get(&2)
            .expect("Could not find transfer queue route")]
        .clone();

        //Since we are now creating the multithread environment, wrap the config in a pointer
        let config = Arc::new(RwLock::new(self.config));

        //Setup message reciving system
        let ((render_recv, obj_recv), msg) = Messenger::new();

        let data_manager = DataManager::new(config.clone());

        let renderer = render::Render::new(
            config.clone(),
            device.clone(),
            graphics_queue.clone(),
            compute_queue.clone(),
            transfer_queue.clone(),
            window,
            surface,
        );

        Ok(Engine {
            config,
            engine_state: Arc::new(RwLock::new(EngineState::Waiting)),
            device,
            graphics_queue,
            compute_queue,
            transfer_queue,

            engine_thread: None,

            renderer,

            data_manager,
        })
    }
}

///The main engine struct which created all sub systems needed for rendering and scene/object management.
pub struct Engine {
    //The configuration loaded before creating all subsystems
    config: Arc<RwLock<Config>>,

    engine_state: Arc<RwLock<EngineState>>,
    //Common vulkan device containing the vulkan instance and entry point
    device: Arc<device::Device>,
    //The queue needed for graphics, transfer and compute operations.
    graphics_queue: Arc<device::Queue>,
    compute_queue: Arc<device::Queue>,
    transfer_queue: Arc<device::Queue>,

    engine_thread: Option<std::thread::JoinHandle<()>>,

    renderer: Arc<RwLock<render::Render>>,
    data_manager: Arc<DataManager>,
}

impl Engine {
    ///Starts the engine loop. Returns a `Messenger` which can be used to control the engine while running.
    pub fn start_engine(&mut self, interface: Option<InterfaceInfo>) -> Messenger {
        if let Some(inter) = interface {
            self.renderer
                .write()
                .expect("Failed to setup interface for engine")
                .set_interface(inter.clone());
            self.data_manager.set_interface(inter);
        }

        {
            *self
                .engine_state
                .write()
                .expect("Could not set state to running!") = EngineState::Running;
        }

        let state = self.engine_state.clone();
        let renderer = self.renderer.clone();
        let obj_manager = self.data_manager.clone();

        //The messanger system to interact lockless with the engine
        let ((render_recv, obj_recv), messenger) = Messenger::new();

        let thread = std::thread::spawn(move || {
            //The main loop of this engine looks single threaded/iterative.
            //it works by updating the object system
            //and then rendering the result.

            //within the sub systems however, time intensive tasks might be done async
            // (like loading objects) or parallel.

            //Some timing system to calculate avg fps.
            let mut last = Instant::now();
            let mut last_print = Instant::now();
            let mut count = 0;
            let mut comm = 0.0;
            while *state.read().expect("Failed to read engine state") == EngineState::Running {
                //Collect all object_manager commnication
                let obj_messages: Vec<ObjectMessage> = obj_recv.try_iter().collect();
                let render_messages: Vec<RenderMessage> = render_recv.try_iter().collect();

                obj_manager.update(obj_messages);

                renderer
                    .write()
                    .expect("Failed to update renderer")
                    .update(render_messages);

                let (current_camera, voxel_objects, gpu_bvh, mesh_draw_calls) = obj_manager
                    .get_render_data(
                        renderer
                            .read()
                            .expect("Failed to get image_ratio")
                            .get_image_ratio(),
                    );

                let load_calls = obj_manager.get_gpu_load_calls();

                renderer
                    .write()
                    .expect("Failed to schedule new frame!")
                    .next_frame(
                        current_camera,
                        load_calls,
                        voxel_objects,
                        gpu_bvh,
                        mesh_draw_calls,
                    );

                count += 1;
                comm += duration_to_float_secs(last.elapsed());
                last = Instant::now();
                if last_print.elapsed().as_secs() > 1 {
                    println!(
                        "FrameTime: {}ms, which is {}fps",
                        (comm * 1000.0) / count as f64,
                        count as f64 / comm
                    );
                    count = 0;
                    comm = 0.0;
                    last_print = Instant::now();
                }
            }
        });

        self.engine_thread = Some(thread);
        messenger
    }

    ///Ends the inner rendering loop
    pub fn end_engine(&mut self) {
        let thread = match self.engine_thread.take() {
            Some(t) => t,
            None => {
                println!("TIKAN: WARNING: engine thread has already stopped!");
                return;
            }
        };
        *self.engine_state.write().expect("Failed to stop engine!") = EngineState::Waiting;

        thread.join().expect("Failed to engine engine!");
    }

    pub fn set_interface(&self, interface: InterfaceInfo) {
        self.renderer
            .write()
            .expect("Failed to setup interface for engine")
            .set_interface(interface.clone());
        self.data_manager.set_interface(interface);
    }

    pub fn device(&self) -> Arc<device::Device> {
        self.device.clone()
    }

    pub fn graphics_queue(&self) -> Arc<device::Queue> {
        self.graphics_queue.clone()
    }

    ///Adds those events to be processed. There is currently some discussion on the winit-side if the lifetime has to be supplied in here.
    /// Otherwise this could be a borrow. However, thats currently the workaround
    pub fn handle_events(&self, events: Vec<winit::event::Event<'static, ()>>) {
        self.data_manager.add_events(events);
    }

    pub fn get_data_manager<'a>(&'a self) -> Arc<DataManager> {
        self.data_manager.clone()
    }

    pub fn get_config_ptr(&self) -> Arc<RwLock<Config>> {
        self.config.clone()
    }

    pub fn get_config<'a>(&'a self) -> RwLockReadGuard<'a, Config> {
        self.config.read().expect("Failed to get config!")
    }

    pub fn get_config_mut<'a>(&'a self) -> RwLockWriteGuard<'a, Config> {
        self.config.write().expect("Failed to get config!")
    }
}
