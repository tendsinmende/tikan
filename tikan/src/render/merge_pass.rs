/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use marp::descriptor::*;
use marp::image::*;
use marp::memory::MemoryUsage;
use marp::shader::*;
use std::sync::{Arc, RwLock};

use crate::render::buffer_manager::BufferManager;
use crate::render::*;

pub struct MergeData {
    camera_data: Arc<buffer::Buffer>,
    is_transitioned: RwLock<bool>,

    triangle_pass_col: Arc<Image>,
    ray_tracing_pass: Arc<Image>,

    final_image: Arc<Image>,
    descriptor_set: Arc<DescriptorSet>,
}

impl MergeData {
    fn is_transitioned(&self) -> bool {
        *self
            .is_transitioned
            .read()
            .expect("Could not get Assemble Data transition state")
    }

    fn set_transitioned(&self) {
        *self
            .is_transitioned
            .write()
            .expect("Failed to set AssembleData to transitioned state") = true;
    }

    fn transition(&self, graphics_queue: Arc<device::Queue>) -> Vec<vk::ImageMemoryBarrier> {
        vec![self.final_image.new_image_barrier(
            Some(vk::ImageLayout::UNDEFINED),
            Some(vk::ImageLayout::GENERAL),
            Some(graphics_queue.clone()),
            Some(graphics_queue.clone()),
            None,
            Some(
                vk::AccessFlags::SHADER_READ
                    | vk::AccessFlags::SHADER_WRITE
                    | vk::AccessFlags::TRANSFER_READ,
            ),
            None,
        )]
    }
}

pub trait MergePass {
    ///The `images`Should be tubel of (traingle_pass_color, ray_tracing_col).
    fn new(
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        swapchain_image_count: usize,
        swapchain_extent: (u32, u32),

        images: Vec<(Arc<Image>, Arc<Image>)>,
        buffer_manager: &BufferManager,
    ) -> Self;
    fn execute(&self, index: usize, command_buffer: Arc<command_buffer::CommandBuffer>);
    ///Returns the image that's going to contain the final image of this frame.
    fn get_finale_image(&self, index: usize) -> Arc<Image>;
}

impl MergePass for pass::Pass<MergeData> {
    fn new(
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        swapchain_image_count: usize,
        swapchain_extent: (u32, u32),

        images: Vec<(Arc<Image>, Arc<Image>)>,
        buffer_manager: &BufferManager,
    ) -> Self {
        let mut data = Vec::with_capacity(swapchain_image_count);
        //We need just 5 times a Storage image descriptor per set
        let descriptor_pool = descriptor::StdDescriptorPool::new(
            device.clone(),
            vec![
                //Per frame three storage images for triangle_color, ray_color and the final image
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::STORAGE_IMAGE)
                    .descriptor_count(swapchain_image_count as u32 * 3)
                    .build(),
                //Per frame the uniform buffer for the camera
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::UNIFORM_BUFFER)
                    .descriptor_count(swapchain_image_count as u32 * 1)
                    .build(),
            ]
            .as_slice(),
            swapchain_image_count as u32, //one set per frame
        )
        .expect("Failed to create descriptor_pool_size!");

        for (idx, (tri_color, ray_color)) in images.into_iter().enumerate() {
            //Setup the final image and produce the data structs
            let final_image_info = ImageInfo::new(
                ImageType::Image2D {
                    width: swapchain_extent.0,
                    height: swapchain_extent.1,
                    samples: 1,
                },
                vk::Format::R32G32B32A32_SFLOAT,
                None,
                Some(image::MipLevel::Specific(1)),
                image::ImageUsage {
                    storage: true,
                    color_aspect: true,
                    transfer_src: true,
                    ..Default::default()
                },
                MemoryUsage::GpuOnly,
                None,
            );

            let final_image = Image::new(
                device.clone(),
                final_image_info,
                buffer::SharingMode::Exclusive,
            )
            .expect("Failed to create final merge image!");

            //Setup the descriptor set
            let mut descriptor_set_builder = descriptor_pool.next();

            let camera_buffer = buffer_manager.get_compute_camera_data(idx);

            descriptor_set_builder
                .add(DescResource::new_image(
                    0,
                    vec![(tri_color.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add triangle_color image to merge pass descriptor set");

            descriptor_set_builder
                .add(DescResource::new_image(
                    1,
                    vec![(ray_color.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add ray_depth image to merge pass descriptor set");

            descriptor_set_builder
                .add(DescResource::new_image(
                    2,
                    vec![(final_image.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add final pass image to merge pass descriptor set");

            descriptor_set_builder
                .add(DescResource::new_buffer(
                    3,
                    vec![camera_buffer.clone()],
                    vk::DescriptorType::UNIFORM_BUFFER,
                ))
                .expect("Failed to add camera buffer to primary rays descriptor set!");

            let descriptor_set = descriptor_set_builder
                .build()
                .expect("Failed to build assemble pass descriptor set!");

            data.push(MergeData {
                camera_data: camera_buffer,
                is_transitioned: RwLock::new(false),
                triangle_pass_col: tri_color,
                ray_tracing_pass: ray_color,

                final_image,
                descriptor_set,
            });
        }

        //Now build the compute pipeline for this pass
        let pipeline_layout = pipeline::PipelineLayout::new(
            device.clone(),
            vec![*data[0].descriptor_set.layout()],
            vec![], //Currently no push data
        )
        .expect("Failed to create merge pass pipeline layout");

        let shader_module = shader::ShaderModule::new_from_spv(
            device.clone(),
            &tikan_shader::get_shader_path("merge_pass.spv"),
        )
        .expect("Failed to load merge pass shader!");

        let shader = shader_module.to_stage(Stage::Compute, "main");

        let pipeline =
            pipeline::ComputePipeline::new(device.clone(), shader.clone(), pipeline_layout)
                .expect("Failed to create merge pass pipeline!");

        pass::Pass {
            device,
            graphics_queue,
            compute_queue,

            compute_shader: shader,
            pipeline,
            descriptor_pool,
            data,
            extent: swapchain_extent,
        }
    }

    fn execute(&self, index: usize, command_buffer: Arc<command_buffer::CommandBuffer>) {
        if !self.data[index].is_transitioned() {
            //Transition final image
            command_buffer
                .cmd_pipeline_barrier(
                    vk::PipelineStageFlags::ALL_COMMANDS,
                    vk::PipelineStageFlags::ALL_COMMANDS,
                    vk::DependencyFlags::empty(),
                    vec![],
                    vec![],
                    self.data[index].transition(self.graphics_queue.clone()),
                )
                .expect("Failed to transition MergeImage!");
            self.data[index].set_transitioned();
        }

        //Now execute merge pass.
        //Bind pipeline and descriptor set and issue execution
        command_buffer
            .cmd_bind_descriptor_sets(
                vk::PipelineBindPoint::COMPUTE,
                self.pipeline.layout(),
                0,
                vec![self.data[index].descriptor_set.clone()],
                vec![],
            )
            .expect("Failed to bind MergePass descriptor set!");
        //Now execute shader, note that we use a local size of 8x8x1 and want one thread per
        // pixel, therefore we divide the image extent by 8 and ceil it to the next int
        command_buffer
            .cmd_bind_pipeline(vk::PipelineBindPoint::COMPUTE, self.pipeline.clone())
            .expect("Failed to bind merge_pass pipeline");
        command_buffer
            .cmd_dispatch(dispatch_extent(self.extent))
            .expect("Could not execute merge pass!");
    }

    fn get_finale_image(&self, index: usize) -> Arc<Image> {
        self.data[index].final_image.clone()
    }
}
