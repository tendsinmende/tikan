/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use widkan::interface::Interface;
use widkan::rendering::*;
use widkan::widget::*;

use widkan::msw::winit;

use marp::ash::vk;
use std::sync::{Arc, RwLock};
pub struct FrambufferWidget {
    area: RwLock<Area>,
}

impl FrambufferWidget {
    pub fn new() -> Arc<Self> {
        Arc::new(FrambufferWidget {
            area: RwLock::new(Area::empty()),
        })
    }

    pub fn get_area(&self) -> Area {
        *self.area.read().expect("Failed to get frambuffer area")
    }

    ///Returns the Blit operation data needed to blit a custom image to the correct region
    /// of the interfaces final `Arc<Image>`. Just fill in your src data and blit it.
    pub fn get_framebuffer_area(&self, _interface: Arc<Interface>) -> vk::ImageBlit {
        let area = self.get_area();
        vk::ImageBlit::builder()
            .dst_subresource(vk::ImageSubresourceLayers {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                mip_level: 0,
                base_array_layer: 0,
                layer_count: 1,
            })
            .dst_offsets([
                vk::Offset3D {
                    x: area.pos.0.ceil() as i32,
                    y: area.pos.1.ceil() as i32,
                    z: 0,
                }, //location
                vk::Offset3D {
                    x: (area.pos.0 + area.extent.0).floor() as i32,
                    y: (area.pos.1 + area.extent.1).floor() as i32,
                    z: 1,
                }, //extent
            ])
            .build() //src has to be filled in by the rendering engine
    }

    pub fn get_extent(&self) -> (u32, u32) {
        let area = self.get_area();
        (area.extent.0 as u32, area.extent.1 as u32)
    }
}

impl Widget for FrambufferWidget {
    fn render(&self, _renderer: &mut Renderer, _level: u32) -> Vec<primitive::PrimitiveCall> {
        vec![]
    }

    fn update(
        &self,
        new_area: Area,
        events: &Vec<events::Event>,
        _renderer: &Renderer,
    ) -> UpdateRetState {
        *self
            .area
            .write()
            .expect("Could not update framebuffer widget area") = new_area;

        //TODO pass inputs to engine if they are within the window
        UpdateRetState::default()
    }
}
