/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use marp::ash::vk;
use marp::descriptor::*;
use marp::image::*;
use marp::memory::MemoryUsage;
use marp::shader::*;
use marp::*;
use std::sync::{Arc, RwLock};

use crate::render;
use crate::render::buffer_manager::BufferManager;
use crate::render::pass::Pass;

///Represents the data pushed to the primary command buffer through a

///Holds the per frame data for the primary pass
pub struct PrimaryData {
    ///Holds the camera data for this frame.
    pub camera_data: Arc<buffer::Buffer>,
    ///[RGB: Normal, A: depth] f32,
    pub normal_depth: Arc<image::Image>,
    ///[RGB: Albedo, A: roughness] 8bit unorm,
    pub albedo_roughness: Arc<image::Image>,
    ///[R: Metal, GBA: unused] f32,
    pub metal_other: Arc<image::Image>,
    ///Holds the depth data from this frame after executing the barrier.
    pub graphics_depth: Arc<image::Image>,
    ///standard descriptor set bound to the primary ray. sub buffers are updated on a per frame basis.
    pub descriptor_set: Arc<descriptor::DescriptorSet>,
    ///Tracks if the resources have been transitioned, if not we have to shedule that before usage.
    pub is_transitioned: RwLock<bool>,
}

impl PrimaryData {
    fn is_transitioned(&self) -> bool {
        *self
            .is_transitioned
            .read()
            .expect("Could not read primary data transitioned state")
    }

    fn set_transitioned(&self) {
        *self
            .is_transitioned
            .write()
            .expect("Could not set primary data transitioned state") = true;
    }
}

///Executes the primary ray-tracing pass, saves the gbuffer into several
///Attachments.
///
/// After raytracing the primary ray the pos_depth image will copy the depth information to an depth image
/// which can be used on the graphics queue to blend in debug objects later.
pub trait PrimaryPass {
    fn new(
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        swapchain_image_count: usize,
        swapchain_extent: (u32, u32),
        buffer_manager: &BufferManager,
    ) -> Self;

    ///Updates the inner descriptor set to use the new voxel-object-buffer
    fn update_descriptor_sets(&self, index: usize, buffer_manager: &BufferManager);

    fn transition_to_start_layout(&self, idx: usize) -> Vec<vk::ImageMemoryBarrier>;
    fn execute(&self, index: usize, command_buffer: Arc<command_buffer::CommandBuffer>);

    ///Returns the normal_depth image for the frame
    fn get_normal_depth_image(&self, index: usize) -> Arc<Image>;
    fn get_albedo_roughness_image(&self, index: usize) -> Arc<Image>;
    ///Returns the depth only image for the triangle pass.
    fn get_graphics_image(&self, index: usize) -> Arc<image::Image>;
    ///Returns the whole gbuffer ordered by the index. So you get a Vec of tuples of images, the index 0 is the gbuffer for the frame slot 0.
    ///The tuple contains (normal_depth, albedo_roughness, metal_other)
    fn get_gbuffer(&self) -> Vec<(Arc<Image>, Arc<Image>, Arc<Image>)>;
}

impl PrimaryPass for Pass<PrimaryData> {
    fn new(
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        swapchain_image_count: usize,
        swapchain_extent: (u32, u32),
        buffer_manager: &BufferManager,
    ) -> Self {
        let mut frame_data: Vec<PrimaryData> = Vec::with_capacity(swapchain_image_count);

        let descriptor_pool = descriptor::StdDescriptorPool::new(
            device.clone(),
            vec![
                //Per frame three storage images and one uniform buffer
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::STORAGE_IMAGE)
                    //For each frame/set a descriptor for normal_depth, albedo_roughness and metal_other
                    .descriptor_count(swapchain_image_count as u32 * 3)
                    .build(),
                //Per frame the uniform buffer for the camera
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::UNIFORM_BUFFER)
                    .descriptor_count(swapchain_image_count as u32 * 1)
                    .build(),
                //For each frame the gpu voxel buffer as well as the dynamic voxel_object buffer and the toplevel bvh buffer
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(swapchain_image_count as u32 * 3)
                    .build(),
            ]
            .as_slice(),
            swapchain_image_count as u32, //one set per frame
        )
        .expect("Failed to create descriptor_pool_size!");

        for idx in 0..swapchain_image_count {
            // Float position and depth info
            let normal_depth_info = image::ImageInfo::new(
                image::ImageType::Image2D {
                    width: swapchain_extent.0,
                    height: swapchain_extent.1,
                    samples: 1,
                },
                vk::Format::R32G32B32A32_SFLOAT,
                None,
                Some(image::MipLevel::Specific(1)),
                image::ImageUsage {
                    transfer_src: true,
                    storage: true,
                    color_aspect: true,
                    ..Default::default()
                },
                MemoryUsage::GpuOnly,
                None,
            );

            let normal_depth = image::Image::new(
                device.clone(),
                normal_depth_info,
                buffer::SharingMode::Exclusive,
            )
            .expect("Failed to allocate normal_depth image");

            //uint, normed albedo and roughness info
            let albedo_roughness_info = image::ImageInfo::new(
                image::ImageType::Image2D {
                    width: swapchain_extent.0,
                    height: swapchain_extent.1,
                    samples: 1,
                },
                vk::Format::R8G8B8A8_UNORM,
                None,
                Some(image::MipLevel::Specific(1)),
                image::ImageUsage {
                    transfer_src: true,
                    storage: true,
                    color_aspect: true,
                    ..Default::default()
                },
                MemoryUsage::GpuOnly,
                None,
            );

            let albedo_roughness = image::Image::new(
                device.clone(),
                albedo_roughness_info,
                buffer::SharingMode::Exclusive,
            )
            .expect("Failed to allocate albedo roughness image");

            //8bit int (probably more later if needed) metallic and other information
            let metal_other_info = image::ImageInfo::new(
                image::ImageType::Image2D {
                    width: swapchain_extent.0,
                    height: swapchain_extent.1,
                    samples: 1,
                },
                vk::Format::R32G32B32A32_SFLOAT,
                None,
                Some(image::MipLevel::Specific(1)),
                image::ImageUsage {
                    transfer_src: true,
                    storage: true,
                    color_aspect: true,
                    ..Default::default()
                },
                MemoryUsage::GpuOnly,
                None,
            );

            let metal_other = image::Image::new(
                device.clone(),
                metal_other_info,
                buffer::SharingMode::Exclusive,
            )
            .expect("Failed to allocate metallic_other image");

            //the depth image that will be supplied to the graphics pass
            let graphics_depth_info = image::ImageInfo::new(
                image::ImageType::Image2D {
                    width: swapchain_extent.0,
                    height: swapchain_extent.1,
                    samples: 1,
                },
                vk::Format::R32G32B32A32_SFLOAT, //TODO find a nice way to just use one channel
                None,
                Some(image::MipLevel::Specific(1)),
                image::ImageUsage {
                    transfer_dst: true,
                    storage: true,
                    color_attachment: true,
                    sampled: true,
                    ..Default::default()
                },
                MemoryUsage::GpuOnly,
                None,
            );

            let graphics_depth = image::Image::new(
                device.clone(),
                graphics_depth_info,
                buffer::SharingMode::Exclusive,
            )
            .expect("Failed to allocate graphics depth image");
            //The graphics depth is not bound, only copied to.

            let camera_buffer = buffer_manager.get_compute_camera_data(idx);

            //Allocate the descriptor set for this frame.
            let mut descriptor_set = descriptor_pool.next();

            descriptor_set
                .add(DescResource::new_image(
                    0,
                    vec![(normal_depth.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to bind pos_depth_image!");

            descriptor_set
                .add(DescResource::new_image(
                    1,
                    vec![(albedo_roughness.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to bind albedo roughness image!");

            descriptor_set
                .add(DescResource::new_image(
                    2,
                    vec![(metal_other.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to bind normal_metal image!");

            descriptor_set
                .add(DescResource::new_buffer(
                    3,
                    vec![camera_buffer.clone()],
                    vk::DescriptorType::UNIFORM_BUFFER,
                ))
                .expect("Failed to add camera buffer to primary rays descriptor set!");

            //Add the static gpu voxel buffer
            descriptor_set
                .add(DescResource::new_buffer(
                    4,
                    vec![buffer_manager.get_voxel_buffer()],
                    vk::DescriptorType::STORAGE_BUFFER,
                ))
                .expect("Failed to add voxel buffer to primary rays descriptor set!");

            //Add the current voxel_object buffer, but it's going to be updated every frame
            descriptor_set
                .add(DescResource::new_buffer(
                    5,
                    vec![buffer_manager.get_voxel_object_buffer(idx)],
                    vk::DescriptorType::STORAGE_BUFFER,
                ))
                .expect("Failed to add voxel object buffer to primary rays descriptor set!");

            descriptor_set
                .add(DescResource::new_buffer(
                    6,
                    vec![buffer_manager.get_toplevel_bvh_buffer(idx)],
                    vk::DescriptorType::STORAGE_BUFFER,
                ))
                .expect("Failed to add toplevel bvh buffer to primary rays descriptor set!");

            let descriptor_set = descriptor_set
                .build()
                .expect("Failed to allocate descriptor set!");

            frame_data.push(PrimaryData {
                camera_data: camera_buffer,
                normal_depth,
                albedo_roughness,
                metal_other,
                graphics_depth,
                descriptor_set,
                is_transitioned: RwLock::new(false),
            });
        }

        let pipeline_layout = pipeline::PipelineLayout::new(
            device.clone(),
            vec![*frame_data[0].descriptor_set.layout()],
            vec![],
        )
        .expect("Failed to crate primary rays pipeline layout");

        //Load the compute shader we use to trace the primary rays.
        let shader_mod = shader::ShaderModule::new_from_spv(
            device.clone(),
            &tikan_shader::get_shader_path("primary_pass.spv"),
        )
        .expect("Failed to load primary pass shader!");

        let shader = shader_mod.to_stage(Stage::Compute, "main");

        let pipeline =
            pipeline::ComputePipeline::new(device.clone(), shader.clone(), pipeline_layout)
                .expect("Failed to create primary rays pipeline");

        Pass {
            device,
            graphics_queue,
            compute_queue,
            compute_shader: shader,
            pipeline,
            descriptor_pool,
            data: frame_data,
            extent: swapchain_extent,
        }
    }

    fn update_descriptor_sets(&self, index: usize, buffer_manager: &BufferManager) {
        self.data[index]
            .descriptor_set
            .update(DescResource::new_buffer(
                5,
                vec![buffer_manager.get_voxel_object_buffer(index)],
                vk::DescriptorType::STORAGE_BUFFER,
            ))
            .expect("Failed to update primary rays descriptor set with new voxel objects!");

        self.data[index]
            .descriptor_set
            .update(DescResource::new_buffer(
                6,
                vec![buffer_manager.get_toplevel_bvh_buffer(index)],
                vk::DescriptorType::STORAGE_BUFFER,
            ))
            .expect("Failed to update primary rays descriptor set with new bvh buffer!");
    }

    ///Transitions all inner resource to the initial layouts and access flags
    fn transition_to_start_layout(&self, idx: usize) -> Vec<vk::ImageMemoryBarrier> {
        vec![
            self.data[idx].graphics_depth.new_image_barrier(
                //Since we assume in the execute that the image is still from the graphics queue in read mode
                Some(vk::ImageLayout::UNDEFINED),
                Some(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                None,
                None,
                None,
                Some(vk::AccessFlags::SHADER_READ),
                None, //all of the image
            ),
            self.data[idx].normal_depth.new_image_barrier(
                Some(vk::ImageLayout::UNDEFINED),
                Some(vk::ImageLayout::GENERAL),
                Some(self.compute_queue.clone()),
                Some(self.compute_queue.clone()),
                None,
                Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE),
                None, //all of the image
            ),
            self.data[idx].albedo_roughness.new_image_barrier(
                Some(vk::ImageLayout::UNDEFINED),
                Some(vk::ImageLayout::GENERAL),
                Some(self.compute_queue.clone()),
                Some(self.compute_queue.clone()),
                None,
                Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE),
                None, //all of the image
            ),
            self.data[idx].metal_other.new_image_barrier(
                Some(vk::ImageLayout::UNDEFINED),
                Some(vk::ImageLayout::GENERAL),
                Some(self.compute_queue.clone()),
                Some(self.compute_queue.clone()),
                None,
                Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE),
                None, //all of the image
            ),
        ]
    }

    ///Executes the raytracing pass, save the primary rays gbuffer, then copies depth info to the graphics depth
    /// info and transitions the image to the graphics queue. Returns the semaphore that should be signaled when
    /// executing this pass so that the graphics command buffer can start working.
    fn execute(&self, index: usize, command_buffer: Arc<command_buffer::CommandBuffer>) {
        //Assuming that we are the first on the command buffer and can therefore just start writing.
        let mut is_first_frame = false;
        if !self.data[index].is_transitioned() {
            command_buffer
                .cmd_pipeline_barrier(
                    vk::PipelineStageFlags::ALL_COMMANDS,
                    vk::PipelineStageFlags::ALL_COMMANDS,
                    vk::DependencyFlags::empty(),
                    vec![],
                    vec![],
                    self.transition_to_start_layout(index),
                )
                .expect("Could not transition to start layout!");
            //Reset flag
            self.data[index].set_transitioned();
            is_first_frame = true;
        }

        //Bind this frame descriptor set and execute compute shader
        command_buffer
            .cmd_bind_descriptor_sets(
                vk::PipelineBindPoint::COMPUTE,
                self.pipeline.layout(),
                0,
                vec![self.data[index].descriptor_set.clone()],
                vec![],
            )
            .expect("Failed to bind primary pass descriptor set!");
        //Now execute shader, note that we use a local size of 8x8x1 and want one thread per
        // pixel, therefore we divide the image extent by 8 and ceil it to the next int
        command_buffer
            .cmd_bind_pipeline(vk::PipelineBindPoint::COMPUTE, self.pipeline.clone())
            .expect("Failed to bind primary ray's pipeline");
        //Execute compute shader
        command_buffer
            .cmd_dispatch(render::dispatch_extent(self.extent))
            .expect("Could not execute primary ray pass!");

        //If we are the first frame only transition depth image,
        //else also acquire graphics depth image
        let mut image_barriers = vec![self.data[index].normal_depth.new_image_barrier(
            Some(vk::ImageLayout::GENERAL),
            Some(vk::ImageLayout::TRANSFER_SRC_OPTIMAL),
            None,
            None,
            Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE),
            Some(vk::AccessFlags::TRANSFER_READ),
            None, //all of the image
        )];
        //Transition from "Something" to initial state, no queue acquire
        if is_first_frame {
            image_barriers.push(
                //Acquire image from graphics queue
                self.data[index].graphics_depth.new_image_barrier(
                    Some(vk::ImageLayout::UNDEFINED),
                    Some(vk::ImageLayout::TRANSFER_DST_OPTIMAL),
                    None,
                    None,
                    None,
                    Some(vk::AccessFlags::TRANSFER_WRITE),
                    None, //all of the image
                ),
            );
        } else {
            image_barriers.push(
                //Acquire image from graphics queue
                self.data[index].graphics_depth.new_image_barrier(
                    Some(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                    Some(vk::ImageLayout::TRANSFER_DST_OPTIMAL),
                    Some(self.graphics_queue.clone()),
                    Some(self.compute_queue.clone()),
                    Some(vk::AccessFlags::SHADER_READ),
                    Some(vk::AccessFlags::TRANSFER_WRITE),
                    None, //all of the image
                ),
            );
        }

        //transfer the depth image to the compute queue and transfer layout
        //transition the pos_depth image to to the copy-src layout.
        command_buffer
            .cmd_pipeline_barrier(
                vk::PipelineStageFlags::COMPUTE_SHADER,
                vk::PipelineStageFlags::TRANSFER,
                vk::DependencyFlags::empty(),
                vec![],
                vec![],
                image_barriers,
            )
            .expect("Could not execute primary pass entry barriers");

        //copy depth content from the primary pass to the image
        command_buffer
            .cmd_copy_image(
                self.data[index].normal_depth.clone(),
                vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                self.data[index].graphics_depth.clone(),
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                vec![vk::ImageCopy {
                    src_subresource: vk::ImageSubresourceLayers {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        mip_level: 0,
                        base_array_layer: 0,
                        layer_count: 1,
                    },
                    src_offset: vk::Offset3D::builder().build(),
                    dst_subresource: vk::ImageSubresourceLayers {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        mip_level: 0,
                        base_array_layer: 0,
                        layer_count: 1,
                    },
                    dst_offset: vk::Offset3D::builder().build(),
                    extent: self.data[index].normal_depth.extent_3d(),
                }],
            )
            .expect("Could not copy depth info to graphics accessible image");

        //Transition graphics depth first to SHADER READ and pos back to SHADER READ
        command_buffer
            .cmd_pipeline_barrier(
                vk::PipelineStageFlags::TRANSFER,
                vk::PipelineStageFlags::ALL_COMMANDS,
                vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![
                    //release to graphics queue
                    self.data[index].graphics_depth.new_image_barrier(
                        Some(vk::ImageLayout::TRANSFER_DST_OPTIMAL),
                        Some(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                        None,
                        None,
                        Some(vk::AccessFlags::TRANSFER_WRITE),
                        Some(vk::AccessFlags::SHADER_READ),
                        None, //all of the image
                    ),
                    self.data[index].normal_depth.new_image_barrier(
                        Some(vk::ImageLayout::TRANSFER_SRC_OPTIMAL),
                        Some(vk::ImageLayout::GENERAL),
                        None,
                        None,
                        Some(vk::AccessFlags::TRANSFER_READ),
                        Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE),
                        None, //all of the image
                    ),
                ],
            )
            .expect("Could not execute primary pass end transitions!");
        //Now Release the graphics image
        command_buffer
            .cmd_pipeline_barrier(
                vk::PipelineStageFlags::TRANSFER,
                vk::PipelineStageFlags::ALL_COMMANDS,
                vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![
                    //release to graphics queue
                    self.data[index].graphics_depth.new_image_barrier(
                        Some(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                        Some(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                        Some(self.compute_queue.clone()),
                        Some(self.graphics_queue.clone()),
                        None,
                        None,
                        None, //all of the image
                    ),
                ],
            )
            .expect("Could not execute primary pass end transitions!");
    }

    fn get_normal_depth_image(&self, index: usize) -> Arc<Image> {
        self.data[index].normal_depth.clone()
    }

    fn get_albedo_roughness_image(&self, index: usize) -> Arc<Image> {
        self.data[index].albedo_roughness.clone()
    }

    fn get_graphics_image(&self, index: usize) -> Arc<image::Image> {
        self.data[index].graphics_depth.clone()
    }

    fn get_gbuffer(&self) -> Vec<(Arc<Image>, Arc<Image>, Arc<Image>)> {
        self.data
            .iter()
            .map(|d| {
                (
                    d.normal_depth.clone(),
                    d.albedo_roughness.clone(),
                    d.metal_other.clone(),
                )
            })
            .collect()
    }
}
