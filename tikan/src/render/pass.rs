/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use marp::descriptor::*;
use marp::*;
use std::sync::Arc;

///The prototype for an renderpass which is implemented for some `data` payload.
pub struct Pass<D> {
    pub device: Arc<device::Device>,
    pub graphics_queue: Arc<device::Queue>,
    pub compute_queue: Arc<device::Queue>,

    pub compute_shader: Arc<shader::ShaderStage>,
    pub pipeline: Arc<pipeline::ComputePipeline>,

    ///The pool from which our descriptors get allocated
    pub descriptor_pool: Arc<StdDescriptorPool>,

    ///Holds the per frame resources needed for this pass.
    pub data: Vec<D>,
    ///The extent this data vec has been created for,
    pub extent: (u32, u32),
}

impl<D> Pass<D> {
    pub fn get_data(&self, index: usize) -> Option<&D> {
        self.data.get(index)
    }
}
