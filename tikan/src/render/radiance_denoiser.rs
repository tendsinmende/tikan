/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Radiance Denoiser
//! The denoiser takes the noisy radiance image produced before this pass and tries to clear it up via some
//! context information from the primary pass.

use marp::descriptor::*;
use marp::image::*;
use marp::memory::MemoryUsage;
use marp::shader::*;
use std::sync::{Arc, RwLock};

use crate::render::buffer_manager::BufferManager;
use crate::render::*;

#[repr(C)]
struct DenoiserPushData {
    radius: i32,
    radius_dist: i32,
    pad1: [i32; 2],

    gi_phi: f32,
    normal_phi: f32,
    depth_phi: f32,
    albedo_phi: f32,

    roughness_phi: f32,
    pad3: [f32; 3],
}

pub struct RadianceDenoiserData {
    //Input data
    g_normal_depth: Arc<Image>,
    g_albedo_roughness: Arc<Image>,
    //Radiance image from the radiance pass
    radiance_image: Arc<Image>,

    descriptor_set: Arc<DescriptorSet>,
}

impl RadianceDenoiserData {
    ///Transitions from write only mode to r+w
    fn transition_to_rw(&self, compute_queue: Arc<device::Queue>) -> Vec<vk::ImageMemoryBarrier> {
        vec![self.radiance_image.new_image_barrier(
            Some(vk::ImageLayout::UNDEFINED),
            Some(vk::ImageLayout::GENERAL),
            Some(compute_queue.clone()),
            Some(compute_queue.clone()),
            None,
            Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE), //Currently transition into a read/write state, my change to dedicated write/read per frame state.
            None,
        )]
    }

    ///Transitions from r+w mode to write only
    fn transition_to_w_only(
        &self,
        compute_queue: Arc<device::Queue>,
    ) -> Vec<vk::ImageMemoryBarrier> {
        vec![self.radiance_image.new_image_barrier(
            Some(vk::ImageLayout::UNDEFINED),
            Some(vk::ImageLayout::GENERAL),
            Some(compute_queue.clone()),
            Some(compute_queue.clone()),
            None,
            Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE), //Currently transition into a read/write state, my change to dedicated write/read per frame state.
            None,
        )]
    }
}

pub trait RadianceDenoiser {
    ///The `images` should be a vector ordered by the frame id (which is usually the case). The tuple consists of: (normal_depth, albedo_roughness, metal_other)
    fn new(
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        swapchain_image_count: usize,
        swapchain_extent: (u32, u32),

        normal_depth_radiance_alebdo_images: Vec<(Arc<Image>, Arc<Image>, Arc<Image>)>,
    ) -> Self;
    fn execute(&self, index: usize, command_buffer: Arc<command_buffer::CommandBuffer>);
    ///Returns the image that's going to contain the final image of this frame.
    fn get_final_image(&self, index: usize) -> Arc<Image>;
}

impl RadianceDenoiser for pass::Pass<RadianceDenoiserData> {
    fn new(
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        swapchain_image_count: usize,
        swapchain_extent: (u32, u32),

        normal_depth_radiance_albedo_images: Vec<(Arc<Image>, Arc<Image>, Arc<Image>)>,
    ) -> Self {
        let mut data = Vec::with_capacity(swapchain_image_count);
        let descriptor_pool = descriptor::StdDescriptorPool::new(
            device.clone(),
            vec![
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::STORAGE_IMAGE)
                    //For each frame/set a descriptor for normal_depth, radiance, alebdo_roughness
                    .descriptor_count(swapchain_image_count as u32 * 3)
                    .build(),
                /* TODO add uniform buffer for denoising controll data
                       vk::DescriptorPoolSize::builder()
                           .ty(vk::DescriptorType::UNIFORM_BUFFER)
                           //For each frame/set a descriptor for camera uniform buffer
                           .descriptor_count(swapchain_image_count as u32 * 1)
                           .build()
                */
            ]
            .as_slice(),
            swapchain_image_count as u32, //one set per frame
        )
        .expect("Failed to create descriptor_pool_size!");

        for (idx, (normal_depth, radiance_image, albedo_roughness)) in
            normal_depth_radiance_albedo_images.into_iter().enumerate()
        {
            //Setup the descriptor set
            let mut descriptor_set_builder = descriptor_pool.next();

            descriptor_set_builder
                .add(DescResource::new_image(
                    0,
                    vec![(normal_depth.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add normal_depth to radiance_denoiser descriptor set");

            descriptor_set_builder
                .add(DescResource::new_image(
                    1,
                    vec![(radiance_image.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add in_radiance to radiance_denoiser descriptor set");

            descriptor_set_builder
                .add(DescResource::new_image(
                    2,
                    vec![(albedo_roughness.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add albedo_roughness to radiance_denoiser descriptor set");

            let descriptor_set = descriptor_set_builder
                .build()
                .expect("Failed to build radiance pass descriptor set!");

            data.push(RadianceDenoiserData {
                g_normal_depth: normal_depth,
                g_albedo_roughness: albedo_roughness,
                //Radiance image from the radiance pass
                radiance_image,

                descriptor_set,
            });
        }

        let dummy_push = PushConstant::new(
            DenoiserPushData {
                radius: 2,
                radius_dist: 1,
                pad1: [0; 2],

                gi_phi: 1.0,
                normal_phi: 0.5,
                depth_phi: 1.0,
                albedo_phi: 0.0001,

                roughness_phi: 0.1,
                pad3: [0.0; 3],
            },
            vk::ShaderStageFlags::COMPUTE,
        );

        //Now build the compute pipeline for this pass
        let pipeline_layout = pipeline::PipelineLayout::new(
            device.clone(),
            vec![*data[0].descriptor_set.layout()],
            vec![*dummy_push.range()], //Currently no push data
        )
        .expect("Failed to create radiance pass pipeline layout");

        let shader_module = shader::ShaderModule::new_from_spv(
            device.clone(),
            &tikan_shader::get_shader_path("radiance_denoiser.spv"),
        )
        .expect("Failed to load radiance denoiser shader!");

        let shader = shader_module.to_stage(Stage::Compute, "main");

        let pipeline =
            pipeline::ComputePipeline::new(device.clone(), shader.clone(), pipeline_layout)
                .expect("Failed to create radiance denoiser pipeline!");

        pass::Pass {
            device,
            graphics_queue,
            compute_queue,

            compute_shader: shader,
            pipeline,
            descriptor_pool,
            data,
            extent: swapchain_extent,
        }
    }

    fn execute(&self, index: usize, command_buffer: Arc<command_buffer::CommandBuffer>) {
        //Always trnsition gi image to read/write access and after this pass back to write only
        /* TODO Currently don't have to transition since all images are in r+w mode
            command_buffer.cmd_pipeline_barrier(
                    vk::PipelineStageFlags::ALL_COMMANDS,
                    vk::PipelineStageFlags::ALL_COMMANDS,
                    vk::DependencyFlags::empty(),
                    vec![],
                    vec![],
                    self.data[index].transition(self.compute_queue.clone())
                ).expect("Failed to transition radiance pass final image!!");
        */

        //Now bind appropriate descriptor set and execute
        command_buffer
            .cmd_bind_descriptor_sets(
                vk::PipelineBindPoint::COMPUTE,
                self.pipeline.layout(),
                0,
                vec![self.data[index].descriptor_set.clone()],
                vec![],
            )
            .expect("Failed to bind radiance descriptor set!");
        //Now execute shader, note that we use a local size of 8x8x1 and want one thread per
        // pixel, therefore we divide the image extent by 8 and ceil it to the next int
        command_buffer
            .cmd_bind_pipeline(vk::PipelineBindPoint::COMPUTE, self.pipeline.clone())
            .expect("Failed to bind radiance pass pipeline");

        //Several executions of the filter
        for n in 0..3 {
            command_buffer
                .cmd_push_constants(
                    self.pipeline.layout(),
                    &PushConstant::new(
                        DenoiserPushData {
                            radius: 2,
                            radius_dist: 1,
                            pad1: [0; 2],

                            gi_phi: 10000.0,
                            normal_phi: 0.5,
                            depth_phi: 1.0,
                            albedo_phi: 0.0001,

                            roughness_phi: 0.1,
                            pad3: [0.0; 3],
                        },
                        vk::ShaderStageFlags::COMPUTE,
                    ),
                )
                .expect("Failed to push data to denoiser");

            command_buffer
                .cmd_dispatch(dispatch_extent(self.extent))
                .expect("Could not execute radiance pass!");

            command_buffer
                .cmd_pipeline_barrier(
                    vk::PipelineStageFlags::COMPUTE_SHADER,
                    vk::PipelineStageFlags::COMPUTE_SHADER,
                    vk::DependencyFlags::empty(),
                    vec![],
                    vec![],
                    vec![],
                )
                .expect("Failed to transition radiance pass final image!!");
        }
    }

    ///Returns the image that's going to contain the final image of this frame. In this case that's a copy of the normal radiance image.
    fn get_final_image(&self, index: usize) -> Arc<Image> {
        self.get_data(index)
            .expect("Could not get radiance data for index!")
            .radiance_image
            .clone()
    }
}
