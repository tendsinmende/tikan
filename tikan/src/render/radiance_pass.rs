/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Radiance Pass
//! The shadow pass traced the scene to every registered and influencing light for a given pixel / gbuffer location.
//! The resulting value is not just a binary "is_in_shadow" but the whole luminance/radiance at that location for every light.
//! The values are in lumen (at least will be).

use marp::descriptor::*;
use marp::image::*;
use marp::memory::MemoryUsage;
use marp::sampler::Sampler;
use marp::shader::*;
use std::sync::{Arc, RwLock};

use crate::render::buffer_manager::BufferManager;
use crate::render::*;

use rand;
use rand::Rng;

#[repr(C)]
struct RadiancePushData {
    iter_index: u32,
    rand: [u32; 3],
    hemi_rad: f32,
    gi_depth: f32,
    pad2: [f32; 2],
}

pub struct RadianceData {
    is_transitioned: RwLock<bool>,

    iter_index: Arc<RwLock<u32>>,

    //Input data
    g_normal_depth: Arc<Image>,
    g_albedo_roughness: Arc<Image>,
    g_metal_other: Arc<Image>,

    bluenoise_image: (Arc<Image>, Arc<Sampler>),

    //Output data
    out_radiance: Arc<Image>,

    descriptor_set: Arc<DescriptorSet>,
}

impl RadianceData {
    fn is_transitioned(&self) -> bool {
        *self
            .is_transitioned
            .read()
            .expect("Could not get Assemble Data transition state")
    }

    fn set_transitioned(&self) {
        *self
            .is_transitioned
            .write()
            .expect("Failed to set AssembleData to transitioned state") = true;
    }

    fn transition(&self, compute_queue: Arc<device::Queue>) -> Vec<vk::ImageMemoryBarrier> {
        vec![self.out_radiance.new_image_barrier(
            Some(vk::ImageLayout::UNDEFINED),
            Some(vk::ImageLayout::GENERAL),
            Some(compute_queue.clone()),
            Some(compute_queue.clone()),
            None,
            Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE), //Currently transition into a read/write state, my change to dedicated write/read per frame state.
            None,
        )]
    }
}

pub trait RadiancePass {
    ///The `images` should be a vector ordered by the frame id (which is usually the case). The tuple consists of: (normal_depth, albedo_roughness, metal_other)
    fn new(
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        swapchain_image_count: usize,
        swapchain_extent: (u32, u32),

        blue_noise_image: (Arc<Image>, Arc<Sampler>),
        g_buffer_images: Vec<(Arc<Image>, Arc<Image>, Arc<Image>)>,
        buffer_manager: &BufferManager,
    ) -> Self;
    ///Updates the inner descriptor set to use the new voxel-object-buffer
    fn update_descriptor_sets(&self, index: usize, buffer_manager: &BufferManager);
    fn execute(&self, index: usize, command_buffer: Arc<command_buffer::CommandBuffer>);
    ///Returns the image that's going to contain the final image of this frame.
    fn get_final_image(&self, index: usize) -> Arc<Image>;
}

impl RadiancePass for pass::Pass<RadianceData> {
    fn new(
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        swapchain_image_count: usize,
        swapchain_extent: (u32, u32),

        blue_noise_image: (Arc<Image>, Arc<Sampler>),
        g_buffer_images: Vec<(Arc<Image>, Arc<Image>, Arc<Image>)>,
        buffer_manager: &BufferManager,
    ) -> Self {
        let mut data = Vec::with_capacity(swapchain_image_count);
        let descriptor_pool = descriptor::StdDescriptorPool::new(
            device.clone(),
            vec![
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::STORAGE_IMAGE)
                    //For each frame/set a descriptor for pos_depth, albedo_roughness and normal_metal and the final out_radiance
                    .descriptor_count(swapchain_image_count as u32 * 4)
                    .build(),
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::STORAGE_BUFFER)
                    //For each frame/set a descriptor for voxel buffer, voxel object buffer, toplevel_bvh
                    .descriptor_count(swapchain_image_count as u32 * 4)
                    .build(),
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::UNIFORM_BUFFER)
                    //For each frame/set a descriptor for camera uniform buffer
                    .descriptor_count(swapchain_image_count as u32 * 2)
                    .build(),
                /*
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    //One image sampler per swapchain image for the bluenoise
                    .descriptor_count(swapchain_image_count as u32 * 1)
                    .build()
                 */
            ]
            .as_slice(),
            swapchain_image_count as u32, //one set per frame
        )
        .expect("Failed to create descriptor_pool_size!");

        for (idx, (normal_depth, albedo_roughness, metal_other)) in
            g_buffer_images.into_iter().enumerate()
        {
            //Setup the final image and produce the data structs
            let out_radiance_info = ImageInfo::new(
                ImageType::Image2D {
                    width: swapchain_extent.0,
                    height: swapchain_extent.1,
                    samples: 1,
                },
                vk::Format::R32G32B32A32_SFLOAT, //TODO Might have to optimize the format here...
                None,
                Some(image::MipLevel::Specific(1)),
                image::ImageUsage {
                    storage: true,
                    color_aspect: true,
                    ..Default::default()
                },
                MemoryUsage::GpuOnly,
                None,
            );

            let out_radiance_image = Image::new(
                device.clone(),
                out_radiance_info,
                buffer::SharingMode::Exclusive,
            )
            .expect("Failed to create radiance image!");

            let camera_buffer = buffer_manager.get_compute_camera_data(idx);

            //Setup the descriptor set
            let mut descriptor_set_builder = descriptor_pool.next();

            descriptor_set_builder
                .add(DescResource::new_image(
                    0,
                    vec![(normal_depth.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add normal_depth to radiance_pass descriptor set");

            descriptor_set_builder
                .add(DescResource::new_image(
                    1,
                    vec![(albedo_roughness.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add albedo_roughness to radiance_pass descriptor set");

            descriptor_set_builder
                .add(DescResource::new_image(
                    2,
                    vec![(metal_other.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add metal_other to radiance_pass descriptor set");

            descriptor_set_builder
                .add(DescResource::new_image(
                    3,
                    vec![(out_radiance_image.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add out_radiance to radiance_pass descriptor set");

            //The camera for this frame
            descriptor_set_builder
                .add(DescResource::new_buffer(
                    4,
                    vec![camera_buffer],
                    vk::DescriptorType::UNIFORM_BUFFER,
                ))
                .expect("Failed to add camera buffer to radiance pass descriptor set!");

            //the voxels for this frame
            descriptor_set_builder
                .add(DescResource::new_buffer(
                    5,
                    vec![buffer_manager.get_voxel_buffer()],
                    vk::DescriptorType::STORAGE_BUFFER,
                ))
                .expect("Failed to add voxel buffer to radiance pass descriptor set!");

            //the voxel object list
            descriptor_set_builder
                .add(DescResource::new_buffer(
                    6,
                    vec![buffer_manager.get_voxel_object_buffer(idx)],
                    vk::DescriptorType::STORAGE_BUFFER,
                ))
                .expect("Failed to add voxel object buffer to radiance pass descriptor set!");

            descriptor_set_builder
                .add(DescResource::new_buffer(
                    7,
                    vec![buffer_manager.get_toplevel_bvh_buffer(idx)],
                    vk::DescriptorType::STORAGE_BUFFER,
                ))
                .expect("Failed to add toplevel bvh buffer to radiance pass descriptor set!");

            descriptor_set_builder
                .add(DescResource::new_image(
                    8,
                    vec![(blue_noise_image.0.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add bluenoise texture radiance pass descriptor set!");

            let descriptor_set = descriptor_set_builder
                .build()
                .expect("Failed to build radiance pass descriptor set!");

            data.push(RadianceData {
                is_transitioned: RwLock::new(false),
                iter_index: Arc::new(RwLock::new(0)),
                g_normal_depth: normal_depth,
                g_albedo_roughness: albedo_roughness,
                g_metal_other: metal_other,
                bluenoise_image: blue_noise_image.clone(),
                out_radiance: out_radiance_image,
                descriptor_set,
            });
        }

        //Dummy Pushconstant struct
        let dummy_push = PushConstant::new(
            RadiancePushData {
                iter_index: 0,
                rand: [0; 3],
                hemi_rad: 10.0,
                gi_depth: 0.5,
                pad2: [0.0; 2],
            },
            vk::ShaderStageFlags::COMPUTE,
        );

        //Now build the compute pipeline for this pass
        let pipeline_layout = pipeline::PipelineLayout::new(
            device.clone(),
            vec![*data[0].descriptor_set.layout()],
            vec![*dummy_push.range()], //Currently no push data
        )
        .expect("Failed to create radiance pass pipeline layout");

        let shader_module = shader::ShaderModule::new_from_spv(
            device.clone(),
            &tikan_shader::get_shader_path("radiance_pass.spv"),
        )
        .expect("Failed to load radiance pass shader!");

        let shader = shader_module.to_stage(Stage::Compute, "main");

        let pipeline =
            pipeline::ComputePipeline::new(device.clone(), shader.clone(), pipeline_layout)
                .expect("Failed to create radiance pass pipeline!");

        pass::Pass {
            device,
            graphics_queue,
            compute_queue,

            compute_shader: shader,
            pipeline,
            descriptor_pool,
            data,
            extent: swapchain_extent,
        }
    }

    ///Updates the inner descriptor set to use the new voxel-object-buffer
    fn update_descriptor_sets(&self, index: usize, buffer_manager: &BufferManager) {
        self.data[index]
            .descriptor_set
            .update(DescResource::new_buffer(
                6,
                vec![buffer_manager.get_voxel_object_buffer(index)],
                vk::DescriptorType::STORAGE_BUFFER,
            ))
            .expect("Failed to update primary rays descriptor set with new voxel objects!");

        self.data[index]
            .descriptor_set
            .update(DescResource::new_buffer(
                7,
                vec![buffer_manager.get_toplevel_bvh_buffer(index)],
                vk::DescriptorType::STORAGE_BUFFER,
            ))
            .expect("Failed to update primary rays descriptor set with new bvh!");
    }

    fn execute(&self, index: usize, command_buffer: Arc<command_buffer::CommandBuffer>) {
        let iter_idx = *self.data[index]
            .iter_index
            .read()
            .expect("Could not get iter_index!");
        let clone = self.data[index].iter_index.clone();
        *clone.write().expect("Could not get iter_index!") = (iter_idx + 1) % (std::u32::MAX - 1);

        if !self.data[index].is_transitioned() {
            //Transition final image
            command_buffer
                .cmd_pipeline_barrier(
                    vk::PipelineStageFlags::ALL_COMMANDS,
                    vk::PipelineStageFlags::ALL_COMMANDS,
                    vk::DependencyFlags::empty(),
                    vec![],
                    vec![],
                    self.data[index].transition(self.compute_queue.clone()),
                )
                .expect("Failed to transition radiance pass final image!!");
            self.data[index].set_transitioned();
        }

        //Now bind appropriate descriptor set and execute
        command_buffer
            .cmd_bind_descriptor_sets(
                vk::PipelineBindPoint::COMPUTE,
                self.pipeline.layout(),
                0,
                vec![self.data[index].descriptor_set.clone()],
                vec![],
            )
            .expect("Failed to bind radiance descriptor set!");
        //Now execute shader, note that we use a local size of 8x8x1 and want one thread per
        // pixel, therefore we divide the image extent by 8 and ceil it to the next int
        command_buffer
            .cmd_bind_pipeline(vk::PipelineBindPoint::COMPUTE, self.pipeline.clone())
            .expect("Failed to bind radiance pass pipeline");

        //let mut rand_gen = rand::thread_rng();
        command_buffer
            .cmd_push_constants(
                self.pipeline.layout(),
                &PushConstant::new(
                    RadiancePushData {
                        iter_index: iter_idx,
                        rand: [0; 3], //[rand_gen.gen(), rand_gen.gen(), rand_gen.gen()],
                        hemi_rad: 10.0,
                        gi_depth: 100.0,
                        pad2: [0.0; 2],
                    },
                    vk::ShaderStageFlags::COMPUTE,
                ),
            )
            .expect("Failed to push iter index to radiance pass");

        command_buffer
            .cmd_dispatch(dispatch_extent(self.extent))
            .expect("Could not execute radiance pass!");
    }

    ///Returns the image that's going to contain the final image of this frame.
    fn get_final_image(&self, index: usize) -> Arc<Image> {
        self.get_data(index)
            .expect("Could not get radiance data for index!")
            .out_radiance
            .clone()
    }
}
