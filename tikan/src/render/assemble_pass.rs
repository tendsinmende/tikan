/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use marp::ash::vk;
use marp::descriptor::*;
use marp::image::*;
use marp::memory::MemoryUsage;
use marp::shader::*;
use marp::*;
use std::sync::{Arc, RwLock};

use crate::render::buffer_manager::BufferManager;
use crate::render::*;

///Is a able to assemble a set of raytracing inputs (reflection, luminance) into a
/// final shaded buffer.
pub struct AssembleData {
    is_transitioned: RwLock<bool>,
    pub final_image: Arc<Image>,
    pub src_specular: Arc<Image>,
    pub src_radiance: Arc<Image>,

    ///Stores the attachment information which is pretty static.
    descriptor_set: Arc<DescriptorSet>,
}

impl AssembleData {
    fn is_transitioned(&self) -> bool {
        *self
            .is_transitioned
            .read()
            .expect("Could not get Assemble Data transition state")
    }

    fn set_transitioned(&self) {
        *self
            .is_transitioned
            .write()
            .expect("Failed to set AssembleData to transitioned state") = true;
    }

    fn transition(&self, compute_queue: Arc<device::Queue>) -> Vec<vk::ImageMemoryBarrier> {
        vec![self.final_image.new_image_barrier(
            Some(vk::ImageLayout::UNDEFINED),
            Some(vk::ImageLayout::GENERAL),
            Some(compute_queue.clone()),
            Some(compute_queue.clone()),
            None,
            Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE),
            None,
        )]
    }
}

pub trait AssemblePass {
    ///Creates a new Assemble pass from the supplied array of images.
    /// The images vec should full fill the following:
    /// 1. has the length of `swapchain_image_count`
    /// 2. each item is an tuple of (final_reflection_image, luminance_image)
    fn new(
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        swapchain_image_count: usize,
        swapchain_extent: (u32, u32),

        g_buffer_images: Vec<(Arc<Image>, Arc<Image>, Arc<Image>)>,
        images: Vec<(Arc<Image>, Arc<Image>)>,
        buffer_manager: &BufferManager,
    ) -> Self;

    ///Adds an execution of this assemble pass to the command buffer. If `queue_change` is set to true,
    /// the final image is acquired from the graphics queue before the execution, and released again to the graphics queue
    /// after execution.
    fn execute(
        &self,
        index: usize,
        command_buffer: Arc<command_buffer::CommandBuffer>,
        queue_change: bool,
    );

    fn get_final_image(&self, index: usize) -> Arc<Image>;
}

impl AssemblePass for pass::Pass<AssembleData> {
    fn new(
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        swapchain_image_count: usize,
        swapchain_extent: (u32, u32),

        g_buffer_images: Vec<(Arc<Image>, Arc<Image>, Arc<Image>)>,
        images: Vec<(Arc<Image>, Arc<Image>)>,
        buffer_manager: &BufferManager,
    ) -> Self {
        let mut data = Vec::with_capacity(swapchain_image_count);
        //We need just 3 times a Storage image descriptor per set
        let descriptor_pool = descriptor::StdDescriptorPool::new(
            device.clone(),
            vec![
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::STORAGE_IMAGE)
                    // Each set needs a descriptor for
                    //specular, radiance, final, normal_depth, albedo_roughness, metal_other
                    .descriptor_count(swapchain_image_count as u32 * 6)
                    .build(),
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::UNIFORM_BUFFER)
                    //For each frame/set a descriptor for camera uniform buffer
                    .descriptor_count(swapchain_image_count as u32 * 1)
                    .build(),
            ]
            .as_slice(),
            swapchain_image_count as u32, //one set per frame
        )
        .expect("Failed to create descriptor_pool_size!");

        for (idx, ((specular, radiance), (normal_depth, albedo_roughness, metal_other))) in images
            .into_iter()
            .zip(g_buffer_images.into_iter())
            .enumerate()
        {
            //Setup the final image and produce the data structs
            let final_image_info = ImageInfo::new(
                ImageType::Image2D {
                    width: swapchain_extent.0,
                    height: swapchain_extent.1,
                    samples: 1,
                },
                vk::Format::R32G32B32A32_SFLOAT,
                None,
                Some(image::MipLevel::Specific(1)),
                image::ImageUsage {
                    storage: true,
                    color_aspect: true,
                    ..Default::default()
                },
                MemoryUsage::GpuOnly,
                None,
            );

            let final_image = Image::new(
                device.clone(),
                final_image_info,
                buffer::SharingMode::Exclusive,
            )
            .expect("Failed to create final assemble image!");

            //Setup the descriptor set
            let mut descriptor_set_builder = descriptor_pool.next();

            descriptor_set_builder
                .add(DescResource::new_image(
                    0,
                    vec![(specular.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add reflection pass image to assemble pass descriptor set");

            descriptor_set_builder
                .add(DescResource::new_image(
                    1,
                    vec![(radiance.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add luminance pass image to assemble pass descriptor set");

            descriptor_set_builder
                .add(DescResource::new_image(
                    2,
                    vec![(final_image.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add final pass image to assemble pass descriptor set");

            descriptor_set_builder
                .add(DescResource::new_image(
                    3,
                    vec![(normal_depth.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add normal_depth to assemble pass descriptor set");

            descriptor_set_builder
                .add(DescResource::new_image(
                    4,
                    vec![(albedo_roughness.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add albedo_roughness to assemble pass descriptor set");

            descriptor_set_builder
                .add(DescResource::new_image(
                    5,
                    vec![(metal_other.clone(), None, vk::ImageLayout::GENERAL)],
                    vk::DescriptorType::STORAGE_IMAGE,
                ))
                .expect("Failed to add metal_other to assemble pass descriptor set");

            descriptor_set_builder
                .add(DescResource::new_buffer(
                    6,
                    vec![buffer_manager.get_compute_camera_data(idx)],
                    vk::DescriptorType::UNIFORM_BUFFER,
                ))
                .expect("Failed to add camera to assemble pass!");

            let descriptor_set = descriptor_set_builder
                .build()
                .expect("Failed to build assemble pass descriptor set!");

            data.push(AssembleData {
                is_transitioned: RwLock::new(false),
                final_image,
                src_specular: specular,
                src_radiance: radiance,
                descriptor_set,
            });
        }

        //Now build the compute pipeline for this pass
        let pipeline_layout = pipeline::PipelineLayout::new(
            device.clone(),
            vec![*data[0].descriptor_set.layout()],
            vec![], //Currently no push data
        )
        .expect("Failed to create assemble pass pipeline layout");

        let shader_module = shader::ShaderModule::new_from_spv(
            device.clone(),
            &tikan_shader::get_shader_path("ray_assemble_pass.spv"),
        )
        .expect("Failed to load assemble pass shader!");

        let shader = shader_module.to_stage(Stage::Compute, "main");

        let pipeline =
            pipeline::ComputePipeline::new(device.clone(), shader.clone(), pipeline_layout)
                .expect("Failed to create assemble pass pipeline!");

        pass::Pass {
            device,
            graphics_queue,
            compute_queue,

            compute_shader: shader,
            pipeline,
            descriptor_pool,
            data,
            extent: swapchain_extent,
        }
    }

    fn execute(
        &self,
        index: usize,
        command_buffer: Arc<command_buffer::CommandBuffer>,
        queue_change: bool,
    ) {
        //First of all, check if we need to init the data
        if !self.data[index].is_transitioned() {
            command_buffer
                .cmd_pipeline_barrier(
                    vk::PipelineStageFlags::ALL_COMMANDS,
                    vk::PipelineStageFlags::ALL_COMMANDS,
                    vk::DependencyFlags::empty(),
                    vec![],
                    vec![],
                    self.data[index].transition(self.compute_queue.clone()),
                )
                .expect("Failed to add transition command for assemble pass!");
            //Reset flag for this data
            self.data[index].set_transitioned();
        }

        if queue_change {
            //Acquire the image from the graphicsQueue
            command_buffer
                .cmd_pipeline_barrier(
                    vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                    vk::PipelineStageFlags::COMPUTE_SHADER,
                    vk::DependencyFlags::empty(),
                    vec![],
                    vec![],
                    vec![self.data[index].final_image.new_image_barrier(
                        Some(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                        Some(vk::ImageLayout::GENERAL),
                        Some(self.graphics_queue.clone()),
                        Some(self.compute_queue.clone()),
                        Some(vk::AccessFlags::SHADER_READ),
                        Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE),
                        None,
                    )],
                )
                .expect("Failed to add image acquire barrier to assemble pass");
        }

        //Bind pipeline and descriptor set and issue execution
        command_buffer
            .cmd_bind_descriptor_sets(
                vk::PipelineBindPoint::COMPUTE,
                self.pipeline.layout(),
                0,
                vec![self.data[index].descriptor_set.clone()],
                vec![],
            )
            .expect("Failed to bind AssemblePass descriptor set!");
        //Now execute shader, note that we use a local size of 8x8x1 and want one thread per
        // pixel, therefore we divide the image extent by 8 and ceil it to the next int
        command_buffer
            .cmd_bind_pipeline(vk::PipelineBindPoint::COMPUTE, self.pipeline.clone())
            .expect("Failed to bind assemble_pass pipeline");
        command_buffer
            .cmd_dispatch(dispatch_extent(self.extent))
            .expect("Could not execute assemble pass!");

        //Now barrier if needed to release the image to the graphics queue
        if queue_change {
            command_buffer
                .cmd_pipeline_barrier(
                    vk::PipelineStageFlags::COMPUTE_SHADER,
                    vk::PipelineStageFlags::COMPUTE_SHADER, //TODO Check if thats correct since we change the queue
                    vk::DependencyFlags::empty(),
                    vec![],
                    vec![],
                    vec![self.data[index].final_image.new_image_barrier(
                        Some(vk::ImageLayout::GENERAL),
                        Some(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                        Some(self.compute_queue.clone()),
                        Some(self.graphics_queue.clone()),
                        Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE),
                        Some(vk::AccessFlags::SHADER_READ),
                        None,
                    )],
                )
                .expect("Failed to add image release barrier to assemble pass");
        }
    }

    fn get_final_image(&self, index: usize) -> Arc<Image> {
        self.data[index].final_image.clone()
    }
}
