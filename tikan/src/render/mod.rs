/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::data::{
    bvh::GpuBvhNode,
    camera::ShaderCamera,
    components::transform::Transform,
    data_manager::DataManager,
    triangle_object::MeshId,
    voxel_data_manager::VoxelDataId,
    voxel_object::{GpuVoxel, GpuVoxelObject},
};

use marp::ash::vk;
use marp::{ash::vk::Extent2D, *};

use widkan::interface;
use widkan::msw::winit;

use crate::config::*;
use crate::message::RenderMessage;

///Contains the command  pool and handles command buffer creation
pub mod command_builder;

///The prototype for specific render/compute-pass implementations.
pub mod pass;

///Defines the rasterized debug pass
pub mod debug_pass;

///Defines the primary ray generation pass
pub mod primary_pass;

///Calculates the scenes specular component which is usually seen as "reflection"
pub mod specular_pass;

///Denoising pass that takes the current frames raw (noisy) GI image and some meta data and tries to improve it.
pub mod radiance_denoiser;

///Calculates the radiance from the incoming light for each intersection in the gbuffer.
pub mod radiance_pass;

///A pass that assembles raytracing inputs based on some settings.
pub mod assemble_pass;

///The final pass that combines the raytraced and rasterized images into one final image.
pub mod merge_pass;

///Provides a widgets which can give us the current size and location we have to blit the final image to
/// when using an interface.
pub mod framebuffer_widget;

///Handles all gpu buffers
pub mod buffer_manager;
use buffer_manager::BufferManager;

use std::sync::{Arc, RwLock};
use std::u64;

const SIZEX: u32 = 8;
const SIZEY: u32 = 8;

///Converts the extent of an image to the dispatch size needed to start one thread
/// by an local thread size of `SIZEX x SIZEY`
fn dispatch_extent(image_extent: (u32, u32)) -> [u32; 3] {
    [
        (image_extent.0 as f32 / SIZEX as f32).ceil() as u32,
        (image_extent.1 as f32 / SIZEY as f32).ceil() as u32,
        1,
    ]
}

///Defines how and interface from widkan will be rendered.
/// If you want to change the interface at runtime, just change sub children of the root node.
#[derive(Clone)]
pub struct InterfaceInfo {
    pub interface: Arc<interface::Interface>,
    pub target_widget: Arc<framebuffer_widget::FrambufferWidget>,
}

#[derive(PartialEq, Debug)]
pub enum RenderState {
    Start,
    Working,
    Ending,
}

///Holds all dependencies of a frame as well as its synchronization primitives.
pub struct FrameInfo {
    ///The index we can use whenever a resource has to be created per frame.
    index: usize,
    ///All fences that need to be signaled to be sure that this frame has finished rendering.
    present_fences: RwLock<Vec<sync::QueueFence>>,

    ///Gets signaled after this frames primary pass has ended and the depth value has been blit to the
    /// graphics depth target. There is a Semaphore for the compute and the graphics queue
    primary_pass_finished_cmp: Arc<sync::Semaphore>,
    primary_pass_finished_gra: Arc<sync::Semaphore>,
    ///Gets signaled when the debug and the possible UI pass have finished
    debug_ui_finished: Arc<sync::Semaphore>,
    ///Gets signaled when the heavy raytracing has finished.
    raytracing_finished: Arc<sync::Semaphore>,

    ///Gets signaled by the swapchain when the swapchain is ready to recive a new image.
    swapchain_ready: Arc<sync::Semaphore>,
    ///Gets signaled by the merge pass when the swapchain image is ready to be presented
    present_ready: Arc<sync::Semaphore>,
}

impl FrameInfo {
    pub fn new(device: Arc<device::Device>, index: usize) -> Self {
        FrameInfo {
            index,
            present_fences: RwLock::new(vec![]),

            primary_pass_finished_cmp: sync::Semaphore::new(device.clone())
                .expect("Could not create priamry_pass sem."),
            primary_pass_finished_gra: sync::Semaphore::new(device.clone())
                .expect("Could not create priamry_pass sem."),
            raytracing_finished: sync::Semaphore::new(device.clone())
                .expect("Could not create raytracing_pass sem."),
            debug_ui_finished: sync::Semaphore::new(device.clone())
                .expect("Could not create debug_pass sem."),
            swapchain_ready: sync::Semaphore::new(device.clone())
                .expect("Could not create swapchain_ready sem."),
            present_ready: sync::Semaphore::new(device.clone())
                .expect("Could not create present_ready sem."),
        }
    }

    ///Replaces the current fence of this frame with the new one.
    pub fn set_present_fences(&self, fences: Vec<sync::QueueFence>) {
        *self
            .present_fences
            .write()
            .expect("Could not set present fence!") = fences;
    }

    ///Returns the current present fence. You should always wait uppon its activation before using this frame info
    pub fn get_present_fences(&self) -> Vec<sync::QueueFence> {
        self.present_fences
            .read()
            .expect("Could not get present fence!")
            .clone()
    }
}

///Handles the rendering complex by using the command builder to build new command buffers, then
/// appends synchronization between frames and passes as well as swapchain handling.
pub struct Render {
    //Reference to the current config
    config: Arc<RwLock<Config>>,

    frame_index: usize,

    //Common vulkan device containing the vulkan instance and entry point
    device: Arc<device::Device>,
    //The queue needed for graphics, transfer and compute operations.
    graphics_queue: Arc<device::Queue>,
    compute_queue: Arc<device::Queue>,
    transfer_queue: Arc<device::Queue>,

    //the swapchain we use to present something
    swapchain: Arc<swapchain::Swapchain>,
    swapchain_images: Vec<Arc<image::SwapchainImage>>,
    //The window on which out swapchain is based.
    window: Arc<winit::window::Window>,

    //The surface we use for presentation
    surface: Arc<dyn swapchain::surface::Surface + Send + Sync + 'static>,

    buffering: BufferingMethode,

    ///Holds all inflight info on a per swapchain image base.
    /// The index of the acquire function of the swapchain indexes into this vec.
    inflight_infos: Vec<FrameInfo>,

    ///Handles command buffer generation
    command_builder: command_builder::CommandBuilder,
    ///Handles the big buffers of the engine, like the voxel cache.
    buffer_manager: BufferManager,
}

impl Render {
    pub fn new(
        config: Arc<RwLock<Config>>,
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        transfer_queue: Arc<device::Queue>,
        window: Arc<winit::window::Window>,
        surface: Arc<dyn swapchain::surface::Surface + Send + Sync + 'static>,
    ) -> Arc<RwLock<Self>> {
        //Check for an appropriate swapchain format. Currently we always try to get the 8-bit unorm format.
        let swapchain_format = surface
            .get_supported_formats(device.get_physical_device())
            .expect("TIKAN: ERROR: Could not get any surface formats!")
            .into_iter()
            .fold(None, |current, format| {
                if let None = current {
                    if format.format == vk::Format::B8G8R8A8_UNORM {
                        Some(format)
                    } else {
                        None
                    }
                } else {
                    current
                }
            })
            .expect("Could not find the 8-bit unorm present format!");

        //Depending on the surfaces creator, read the swapchain dimensions.
        let swapchain_extent = match surface.get_platform() {
            swapchain::surface::WindowPlatform::Wayland => {
                let new_dimensions = window.inner_size();
                let dpi = window.scale_factor();
                //Thats a small hack since some (at least wayland) implementations always return a not okay surface we
                // skip if the image is the same size.
                (
                    (new_dimensions.width as f64 * dpi) as u32,
                    (new_dimensions.height as f64 * dpi) as u32,
                )
            }
            _ => {
                //Get the new dimensions from the swapchain
                let area = surface
                    .get_surface_capabilities(device.get_physical_device())
                    .expect("Failed to get surface capabilities!")
                    .current_extent;
                (area.width, area.height)
            }
        };

        let swapchain = swapchain::Swapchain::new(
            device.clone(),
            surface.clone(),
            vk::Extent2D::builder()
                .width(swapchain_extent.0)
                .height(swapchain_extent.1)
                .build(),
            Some(swapchain_format),
            Some(
                config
                    .read()
                    .expect("Failed to get config for Swapchain")
                    .swapchain_image_count,
            ),
            Some(
                config
                    .read()
                    .expect("Failed to get config for Swapchain")
                    .swapchain_update_methode
                    .to_methode(),
            ),
            Some(image::ImageUsage {
                transfer_dst: true, //we only want to copy to the swapcahin image. Not render to it or anything else
                color_attachment: true,
                ..Default::default()
            }),
        )
        .expect("TIKAN: ERROR: Failed to create swapchain!");

        //Fast transition into present layout
        swapchain.images_to_present_layout(graphics_queue.clone());

        let swapchain_images = swapchain.get_images();

        //create the needed amount of inflight fences which are already signaled.
        let mut inflight_queue = Vec::new();
        for idx in 0..swapchain_images.len() {
            inflight_queue.push(FrameInfo::new(device.clone(), idx));
        }

        let buffer_manager = BufferManager::new(
            &config
                .read()
                .expect("Failed to get config for buffer manager!"),
            device.clone(),
            graphics_queue.clone(),
            compute_queue.clone(),
            transfer_queue.clone(),
            swapchain_images.len(),
        );

        let command_builder = command_builder::CommandBuilder::new(
            config.clone(),
            device.clone(),
            graphics_queue.clone(),
            compute_queue.clone(),
            &buffer_manager,
            inflight_queue.len(),
            swapchain_extent,
        );

        Arc::new(RwLock::new(Render {
            config: config.clone(),
            frame_index: 0,

            device,
            graphics_queue,
            compute_queue,
            transfer_queue,

            swapchain,
            swapchain_images,
            window,
            surface,
            buffering: config
                .read()
                .expect("Failed to get config for buffering methode")
                .buffering_methode,
            inflight_infos: inflight_queue,

            command_builder,
            buffer_manager,
        }))
    }

    pub fn get_image_ratio(&self) -> f32 {
        self.command_builder.get_render_image_ratio()
    }

    ///Renders a new frame. Takes the frame slot-index which decides which subsets of resources are used.
    /// Should always be the longest unused index since unnecessary wait could occur otherwise.
    pub fn next_frame(
        &mut self,
        camera: ShaderCamera,
        load_calls: Vec<(VoxelDataId, Vec<GpuVoxel>)>,
        active_objects: Vec<(VoxelDataId, GpuVoxelObject)>,
        toplevel_bvh: Vec<GpuBvhNode>,
        mesh_draw_calls: Vec<(MeshId, Transform)>,
    ) {
        let index = self.frame_index;
        //Update index
        self.frame_index = (index + 1) % self.inflight_infos.len();

        //Schedule load calls before waiting for last frame
        for (id, data) in load_calls.into_iter() {
            self.buffer_manager.upload_voxel_object(&id, data);
        }

        //wait for last frames fences on this frame slot since we should not override fences that are still in use on this slot
        for fence in self.inflight_infos[index].get_present_fences() {
            if let Err(er) = fence.wait(1_000_000_000) {
                panic!("TIKAN: Error while waiting for last frames fences: {}", er);
            }
        }

        //Acquire the image index from the swapchain,
        let swapchain_ready_semaphore = self.inflight_infos[index].swapchain_ready.clone();

        //Since there could be an resize event since we blocked, check state
        self.check_swapchain_state();

        let swapchain_image_idx = match self
            .swapchain
            .acquire_next_image(u64::MAX, swapchain_ready_semaphore.clone())
        {
            Ok(idx) => idx,
            Err(_) => {
                println!("Could not acquire new image idx!");
                return;
            }
        };

        //Before starting the frame, schedule buffer updates, this can be done parallel to the last frame
        self.buffer_manager
            .update_manager(self.inflight_infos[index].index);

        //generate command buffers. This will internally already start most work
        // and only returns the last, swapchain dependend command buffer, since we need
        // to sync its exec with the swapchain.
        let frame_fences = self.command_builder.build_new_frame(
            &self.inflight_infos[index],
            self.swapchain_images[swapchain_image_idx as usize].clone(),
            &mut self.buffer_manager,
            camera,
            active_objects,
            toplevel_bvh,
            mesh_draw_calls,
        );

        //Update the frame info to the new fences
        self.inflight_infos[index].set_present_fences(frame_fences);
        //Submit new image to swapchain
        match self.swapchain.queue_present(
            self.graphics_queue.clone(),
            vec![self.inflight_infos[index].present_ready.clone()],
            swapchain_image_idx,
        ) {
            Ok(_) => {}
            Err(er) => {
                println!("Failed to present to swapchain: {:#?}", er);
            }
        }
    }

    fn check_swapchain_state(&mut self) {
        if self.swapchain.check_image_state() != swapchain::SwapchainImageState::Ok {
            //Recreate swapchain for current window dimensions.
            //Currenty the wayland surface is reporting wrong information, therefore check if we are on wayland, if so
            //Use the window provided dimension, else use the one from the current swapchain extent.
            let new_dim = match self.surface.get_platform() {
                swapchain::surface::WindowPlatform::Wayland => {
                    let new_dimensions = self.window.inner_size();
                    let dpi = self.window.scale_factor();
                    //Thats a small hack since some (at least wayland) implementations always return a not okay surface we
                    // skip if the image is the same size.

                    (
                        (new_dimensions.width as f64 * dpi) as u32,
                        (new_dimensions.height as f64 * dpi) as u32,
                    )
                }
                _ => {
                    //Get the new dimensions from the swapchain
                    let area = self
                        .swapchain
                        .get_current_surface_extend()
                        .unwrap_or(Extent2D {
                            width: self.window.inner_size().width,
                            height: self.window.inner_size().height,
                        });
                    (area.width, area.height)
                }
            };

            let fb_dim = self.current_framebuffer_extent();
            if fb_dim.0 == new_dim.0 && fb_dim.1 == new_dim.1 {
                return;
            } else {
                println!("Resizing with new dim: {:?}", new_dim);
                //resize sub systems
                self.update_framebuffer(new_dim);
            }
        }
    }

    pub fn get_device(&self) -> Arc<device::Device> {
        self.device.clone()
    }

    pub fn frame_count(&self) -> usize {
        self.inflight_infos.len()
    }

    pub fn get_graphics_queue(&self) -> Arc<device::Queue> {
        self.graphics_queue.clone()
    }

    fn current_framebuffer_extent(&self) -> (u32, u32) {
        use marp::image::AbstractImage;
        let img_ext = self.swapchain_images[0].extent();
        (img_ext.width, img_ext.height)
    }

    fn update_framebuffer(&mut self, new_extent: (u32, u32)) {
        //Recreate swapchain
        let swapchain_extent = vk::Extent2D::builder()
            .width(new_extent.0)
            .height(new_extent.1)
            .build();
        self.swapchain.recreate(swapchain_extent);
        let submit_fence = self
            .swapchain
            .images_to_present_layout(self.graphics_queue.clone());
        submit_fence
            .wait(u64::MAX)
            .expect("Failed to transition swapchain image to present layout!");
        self.swapchain_images = self.swapchain.get_images();

        //Recreate the sub systems of the command builder
        self.command_builder.update_framebuffer(
            self.device.clone(),
            new_extent,
            self.swapchain.get_images().len(),
            &self.buffer_manager,
        );
    }
    ///Executes all messages received from outside
    pub fn update(&mut self, messages: Vec<RenderMessage>) {
        for m in messages {
            match m {
                RenderMessage::ChangeRenderQuality(new_quality) => {
                    self.config
                        .write()
                        .expect("Failed to set new render quality")
                        .tracing_quality_threshold = new_quality
                }
                RenderMessage::ChangeConeStart(new_start) => {
                    self.config
                        .write()
                        .expect("Failed to set new cone start")
                        .cone_start = new_start
                }
                RenderMessage::ChangeConeMaxLength(new_length) => {
                    self.config
                        .write()
                        .expect("Failed to set new cone length")
                        .cone_max_length = new_length
                }
                RenderMessage::ChangeConeMinAperture(new_angle) => {
                    self.config
                        .write()
                        .expect("Failed to set new cone angle")
                        .cone_min_aperture = new_angle
                }
            }
        }
    }

    pub fn set_interface(&mut self, interface: InterfaceInfo) {
        self.command_builder.set_interface(interface);
    }
}
