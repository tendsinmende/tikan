/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Buffer Manager
//! Manages several special buffers.
//!
//!
//! The manager currently contains three major sub structures:
//! - Camera Buffer updates
//! - VoxelScene handling
//! - TriangleScene handling
//!
//! The manager has one camera buffer per frame, which gets updated whenever a frame starts. Fairly simple.
//! The Voxel scene handling is a bit more complicated.
//!
//! The Voxel scene data is stored on the gpu within several junks of voxel data (one per object atm., later those might be compined).
//! When an object is needed, it gets uploaded via an async upload command on the transfer-queue.
//! When that is finished, it gets flagged usable on the cpu.
//!
//! When a voxel object is usable, it will be respected when building the scene BVH on runtime and therefore will be accessed based on its index on the gpu.
//! When an object is not used anymore, the buffer object will be dropped and therefore deleted.
//!
//! **Later the buffer objects might be combined into big buffers where each object occupies a region. That should have better performance but is
//! more difficult to implement.**
//!
//!
//! The triangle data used in the debug pass is currently stored in separate buffers which are connected to their
//! Object via the objects ID

use crate::data::camera::ShaderCamera;
use marp::ash::vk;
use marp::command_buffer::*;
use marp::memory::MemoryUsage;
use marp::sync::{AbstractFence, Fence, Semaphore};
use marp::{buffer::*, device::Device, device::Queue, *};
use std::collections::HashMap;
use std::convert::TryInto;
use std::sync::Arc;

use crate::config::Config;
use crate::data::{
    bvh::GpuBvhNode,
    triangle_object::*,
    voxel_data_manager::VoxelDataId,
    voxel_object::{GpuVoxel, GpuVoxelObject, VoxelObject, VoxelObjectId},
};

#[derive(Clone)]
pub struct Range {
    ///True if this buffer range is used
    used: bool,
    ///The index of the first VOXEL in this buffer range. That is the same index used within the shaders to index the first voxel of an object.
    start: usize,
    ///Number of voxels that fit in here
    size: usize,
}

impl Range {
    pub fn size(&self) -> usize {
        self.size
    }
}

///Handles the voxel buffer on the gpu
struct VoxelBuffer {
    gpu_mem: Arc<Buffer>,
    ranges: Vec<Range>,
}

impl VoxelBuffer {
    pub fn new(config: &Config, device: Arc<Device>, compute_queue: Arc<Queue>) -> Self {
        VoxelBuffer {
            gpu_mem: Buffer::new(
                device,
                (config.voxel_cache_size * std::mem::size_of::<GpuVoxel>()) as u64,
                BufferUsage {
                    storage_buffer: true,
                    transfer_dst: true,
                    ..Default::default()
                },
                SharingMode::Exclusive,
                MemoryUsage::GpuOnly,
            )
            .expect("Failed to init GPU Voxel cache"),
            ranges: vec![Range {
                used: false,
                start: 0,
                size: config.voxel_cache_size, //is in voxels, not byte
            }],
        }
    }

    pub fn alloc(&mut self, num_voxels: usize) -> Result<Range, ()> {
        let res = self.inner_alloc(num_voxels);
        if res.is_ok() {
            res
        } else {
            self.defrag();
            self.inner_alloc(num_voxels)
        }
    }

    //Tries to find a continues allocation in this buffer for `num_voxels` voxels.
    fn inner_alloc(&mut self, num_voxels: usize) -> Result<Range, ()> {
        for idx in 0..self.ranges.len() {
            if !self.ranges[idx].used && self.ranges[idx].size() >= num_voxels {
                //take the found range, mark some voxels used then insert both back in.
                let range = self.ranges[idx].clone();
                let new_range = Range {
                    used: true,
                    start: range.start,
                    size: num_voxels,
                };

                let rest_range = Range {
                    used: false,
                    start: range.start + num_voxels + 1,
                    size: range.size() - num_voxels - 1,
                };

                self.ranges[idx] = new_range.clone();
                self.ranges.insert(idx + 1, rest_range);

                return Ok(new_range);
            }
        }

        Err(())
    }

    fn defrag(&mut self) {
        //Build the new allocator layout
        let mut new_buffer_layout = Vec::new();
        let mut new_free_range: Option<Range> = None;
        for r in self.ranges.iter() {
            if !r.used {
                //Check if we got a free range at. if so, append size
                if let Some(ref mut nfr) = new_free_range {
                    nfr.size += r.size;
                } else {
                    new_free_range = Some(r.clone());
                }
            } else {
                //Check if there has been a free range till here and push
                if let Some(fr) = new_free_range.take() {
                    new_buffer_layout.push(fr);
                }

                //And push the used one
                new_buffer_layout.push(r.clone());
            }
        }

        //If we ended with a free range we have to push it as well
        if let Some(fr) = new_free_range.take() {
            new_buffer_layout.push(fr);
        }

        //Finally override the buffers
        self.ranges = new_buffer_layout;
    }

    pub fn remove(&mut self, range: Range) {
        for r in self.ranges.iter_mut() {
            if r.start == range.start {
                r.used = false;
            }
        }
    }

    pub fn print(&self) {
        print!("-");
        let mut last_biggest_start = 0;
        for range in self.ranges.iter() {
            if range.start < last_biggest_start {
                println!("ALLOC ERROR!");
                return;
            } else {
                last_biggest_start = range.start;
            }
            let print = if range.used { "|x" } else { "| " };

            for _ in 0..range.size() {
                print!("{}", print);
            }
        }
        println!("|-");
    }
}

///A small abstraction over the data that is needed to draw debug triangles.
pub struct TriangleData {
    pub mesh: TriangleMesh,

    ///Is some if the Mesh is loaded to gpu memory
    pub vertex_buffer: Option<Arc<Buffer>>,
    /// Same as vertex buffer, is some if mesh is loaded.
    pub index_buffer: Option<Arc<Buffer>>,
}

pub struct VoxelObjectData {
    ///Describes where on the gpu the data is layed out. If `None`, then the object is not loaded on the gpu.
    pub data_range: Option<Range>,
}

enum LoadCall {
    Mesh(MeshId),
    VoxelObject {
        data_id: VoxelDataId,
        data: Vec<GpuVoxel>,
    },
}

enum UnloadCall {
    Mesh(MeshId),
    VoxelObject(VoxelDataId),
}

///Holds semaphores and other data needed to synchronise buffer uploads to the gpu.
/// The "finish" semaphore is used to not start primary raytracing with buffer ranges that are still being uploaded.
struct UploadData {
    last_release_finished: Option<Arc<dyn AbstractFence + Send + Sync + 'static>>,
    release_has_finished: Arc<Semaphore>,
    last_upload_finished: Option<Arc<dyn AbstractFence + Send + Sync + 'static>>,
    upload_has_finished: Arc<Semaphore>,
    last_acquire_finished: Option<Arc<dyn AbstractFence + Send + Sync + 'static>>,
    acquire_has_finished: Arc<Semaphore>,

    //Is true if the acquire semaphore has to be waited uppon when using the gpu-voxels next
    requires_wait: bool,
}

impl UploadData {
    pub fn new(device: Arc<Device>) -> Self {
        UploadData {
            last_release_finished: None,
            release_has_finished: Semaphore::new(device.clone())
                .expect("Could not create upload semaphore!"),
            last_upload_finished: None,
            upload_has_finished: Semaphore::new(device.clone())
                .expect("Could not create upload semaphore!"),
            last_acquire_finished: None,
            acquire_has_finished: Semaphore::new(device.clone())
                .expect("Could not create upload semaphore!"),
            requires_wait: false,
        }
    }
}

///Collects all per-frame data within the buffer manager. This includes
/// - Voxel-/Triangle-Buffer update semaphores
/// - Camera data for compute and graphics queue
/// - VoxelObjectBuffer for the last frame
pub struct BufferFrameData {
    ///Holds the semaphores required to mark upload events and sync them
    upload_data: UploadData,

    graphics_camera_buffer: Arc<Buffer>,
    compute_camera_buffer: Arc<Buffer>,
    ///Holds the list of voxel object information for this frame.
    voxel_object_buffer: Arc<Buffer>,
    ///Holds the Bvh for the voxel_object_buffer used for acceleration.
    toplevel_bvh_buffer: Arc<Buffer>,
}

impl BufferFrameData {
    pub fn new(device: Arc<device::Device>) -> Self {
        BufferFrameData {
            upload_data: UploadData::new(device.clone()),
            graphics_camera_buffer: Buffer::new(
                device.clone(),
                std::mem::size_of::<ShaderCamera>() as u64,
                BufferUsage {
                    uniform_buffer: true,
                    transfer_dst: true, //needed for the cmd_update_buffer
                    ..Default::default()
                },
                SharingMode::Exclusive,
                MemoryUsage::GpuOnly,
            )
            .expect("Failed to create camera buffer!"),
            compute_camera_buffer: Buffer::new(
                device.clone(),
                std::mem::size_of::<ShaderCamera>() as u64,
                BufferUsage {
                    uniform_buffer: true,
                    transfer_dst: true, //needed for the cmd_update_buffer
                    ..Default::default()
                },
                SharingMode::Exclusive,
                MemoryUsage::GpuOnly,
            )
            .expect("Failed to create camera buffer!"),

            voxel_object_buffer: Buffer::new(
                device.clone(),
                std::mem::size_of::<GpuVoxelObject>() as u64, //Create for just one but won't be used yet
                BufferUsage {
                    storage_buffer: true,
                    transfer_dst: true,
                    ..Default::default()
                },
                SharingMode::Exclusive,
                MemoryUsage::GpuOnly,
            )
            .expect("Failed to create voxel_object_buffer!"),

            toplevel_bvh_buffer: Buffer::new(
                device.clone(),
                std::mem::size_of::<GpuBvhNode>() as u64, //Create for just one but won't be used yet
                BufferUsage {
                    storage_buffer: true,
                    transfer_dst: true,
                    ..Default::default()
                },
                SharingMode::Exclusive,
                MemoryUsage::GpuOnly,
            )
            .expect("Failed to create voxel_object_buffer!"),
        }
    }
}

pub struct BufferManager {
    device: Arc<device::Device>,
    graphics_queue: Arc<device::Queue>,
    compute_queue: Arc<device::Queue>,
    transfer_queue: Arc<device::Queue>,

    ///Manages the transfer command buffer pools
    transfer_command_pool: Arc<CommandBufferPool>,
    ///Used to release and reaquire the voxel-cache ranges
    compute_command_pool: Arc<CommandBufferPool>,

    frame_data: Vec<BufferFrameData>,

    triangle_data: Vec<TriangleData>,
    free_triangle_data_slots: Vec<usize>,

    voxel_data: HashMap<VoxelDataId, VoxelObjectData>,
    free_voxel_object_slots: Vec<usize>,

    ///Handles the on gpu memory and is used to schedule the alloc and free of new
    ///voxel object on the gpu
    voxel_buffer: VoxelBuffer,

    load_calls: Vec<LoadCall>,
    unload_calls: Vec<UnloadCall>,
}

impl BufferManager {
    pub fn new(
        config: &Config,
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        transfer_queue: Arc<device::Queue>,

        swapchain_image_count: usize,
    ) -> Self {
        let mut frame_data = Vec::with_capacity(swapchain_image_count);

        for _ in 0..swapchain_image_count {
            frame_data.push(BufferFrameData::new(device.clone()))
        }

        let transfer_pool = CommandBufferPool::new(
            device.clone(),
            transfer_queue.clone(),
            ash::vk::CommandPoolCreateFlags::empty(),
        )
        .expect("Could not create transfer command pool!");

        let compute_pool = CommandBufferPool::new(
            device.clone(),
            transfer_queue.clone(),
            ash::vk::CommandPoolCreateFlags::empty(),
        )
        .expect("Could not create compute acquire/release command pool!");

        BufferManager {
            device: device.clone(),
            graphics_queue,
            compute_queue: compute_queue.clone(),
            transfer_queue,

            transfer_command_pool: transfer_pool,
            compute_command_pool: compute_pool,

            frame_data,

            triangle_data: Vec::new(),
            free_triangle_data_slots: Vec::new(),

            voxel_data: HashMap::new(),
            free_voxel_object_slots: Vec::new(),

            voxel_buffer: VoxelBuffer::new(config, device, compute_queue),

            load_calls: Vec::new(),
            unload_calls: Vec::new(),
        }
    }

    ///Get the current camera buffer which can be used on the graphics queue.
    pub fn get_graphics_camera_data(&self, index: usize) -> Arc<Buffer> {
        self.frame_data[index].graphics_camera_buffer.clone()
    }

    ///Get the current camera buffer which can be used on the compute queue.
    pub fn get_compute_camera_data(&self, index: usize) -> Arc<Buffer> {
        self.frame_data[index].compute_camera_buffer.clone()
    }

    ///Returns a semaphore that has to be waited uppon submitting a raytracing pass, if voxel data was updated before on that frame-index
    pub fn get_upload_semaphore(&mut self, frame_index: usize) -> Option<Arc<Semaphore>> {
        if self.frame_data[frame_index].upload_data.requires_wait {
            if self.transfer_queue.get_family().get_family_index()
                == self.compute_queue.get_family().get_family_index()
            {
                //Uses the shortcut function

                //Assume that sem will be used, flag as unused
                self.frame_data[frame_index].upload_data.requires_wait = false;
                Some(
                    self.frame_data[frame_index]
                        .upload_data
                        .upload_has_finished
                        .clone(),
                )
            } else {
                //Uses the whole pass.

                //Assume that sem will be used, flag as unused
                self.frame_data[frame_index].upload_data.requires_wait = false;
                Some(
                    self.frame_data[frame_index]
                        .upload_data
                        .acquire_has_finished
                        .clone(),
                )
            }
        } else {
            None
        }
    }

    ///Returns the current toplevel bvh buffer to the index
    pub fn get_toplevel_bvh_buffer(&self, index: usize) -> Arc<Buffer> {
        self.frame_data[index].toplevel_bvh_buffer.clone()
    }

    ///Returns the voxel object buffer for the frame `index`
    pub fn get_voxel_object_buffer(&self, index: usize) -> Arc<Buffer> {
        self.frame_data[index].voxel_object_buffer.clone()
    }

    ///Returns the big gpu local voxel buffer which is managed by this manager.
    pub fn get_voxel_buffer(&self) -> Arc<Buffer> {
        self.voxel_buffer.gpu_mem.clone()
    }

    ///Sorts all voxel load operations into a tupel per load of (star, size, data_buffer)
    fn sort_voxel_load_op(
        &mut self,
        load_ops: Vec<(VoxelDataId, Vec<GpuVoxel>)>,
    ) -> Vec<(u64, u64, Arc<Buffer>)> {
        load_ops.into_iter().filter_map(|(vox_id, data)|{
            //Check if we can get a range for the object
            if let Some(ref mut vo) = self.voxel_data.get_mut(&vox_id){
                //Load the data, but only if the object has no range assigned yet
                if vo.data_range.is_none(){
                    //Try to get a range in our gpu memory
                    if let Ok(range) = self.voxel_buffer.alloc(data.len()){
                        //Since there is a range, override in the object
                        vo.data_range = Some(range.clone());   

                        //Transfer the target region of the voxel buffer to the transfer queue.
                        let range_start = (range.start * std::mem::size_of::<GpuVoxel>()) as u64;
                        let range_size = (range.size * std::mem::size_of::<GpuVoxel>()) as u64;
                        
                        let (transfer_upload, transfer_buffer) = Buffer::from_data(
                            self.device.clone(),
                            self.transfer_queue.clone(),
                            data,
                            BufferUsage{
                                transfer_src: true,
                                ..Default::default()
                            },
                            SharingMode::Exclusive,
                            MemoryUsage::CpuToGpu
                        ).expect("Failed to create new voxel transfer buffer!");
                        transfer_upload.wait(std::u64::MAX).expect("Failed to wait for voxel transfer buffer memory mapping!");

                        Some((range_start, range_size, transfer_buffer))
                    }else{
                        println!("TIKAN: WARNING: Could not alloc voxel object room on the gpu, there might not be enough room on the gpu!");
                        None
                    }
                }else{
                    None
                }
            }else{
                //There is already such a 
                None
            }
        }).collect()
    }

    ///Builds release barriers for a given set of ranges from the compute to the transfer queue. Those are the same used to acquire the ranges on the
    /// transfer queue.
    fn compute_release_barriers(
        &self,
        ranges: &[(u64, u64, Arc<Buffer>)],
    ) -> Vec<ash::vk::BufferMemoryBarrier> {
        ranges
            .iter()
            .map(|(start, size, _buf)| {
                self.voxel_buffer.gpu_mem.buffer_barrier(
                    Some(self.compute_queue.clone()),
                    Some(self.transfer_queue.clone()),
                    Some(vk::AccessFlags::SHADER_READ),
                    Some(vk::AccessFlags::TRANSFER_WRITE),
                    Some(*start),
                    Some(*size),
                )
            })
            .collect::<Vec<vk::BufferMemoryBarrier>>()
    }

    ///Releases the given ranges on the compute queue, returns the semaphore that signals that releasing on this queue is done.
    fn release_ranges_on_compute(
        &mut self,
        frame_index: usize,
        ranges: &[(u64, u64, Arc<Buffer>)],
    ) -> Arc<Semaphore> {
        let compute_release_barriers = self.compute_release_barriers(ranges);

        //Now create a small command buffer which calls the release barriers for each range.
        //Again, if there are non we just don't do anything and don't wait for the barrier in the acquire/copy command
        let release_cb = self
            .compute_command_pool
            .alloc(1, false)
            .expect("Failed to get release command buffer!")
            .into_iter()
            .nth(0)
            .expect("Could not get release command buffer from iter");

        release_cb
            .begin_recording(true, false, false, None)
            .expect("Failed to start release command buffer!");
        //release all buffer ranges
        release_cb
            .cmd_pipeline_barrier(
                vk::PipelineStageFlags::COMPUTE_SHADER,
                vk::PipelineStageFlags::TRANSFER,
                vk::DependencyFlags::empty(),
                vec![],
                compute_release_barriers.clone(),
                vec![],
            )
            .expect("Failed to release buffer ranges!");

        release_cb
            .end_recording()
            .expect("failed to end release cb!");
        //Submit to queue
        let release_has_finished = self
            .compute_queue
            .queue_submit(vec![device::SubmitInfo::new(
                vec![],
                vec![release_cb],
                vec![self.frame_data[frame_index]
                    .upload_data
                    .release_has_finished
                    .clone()],
            )])
            .expect("Failed to run release cb!");

        //Update the fence info for this frame
        if let Some(wait) = self.frame_data[frame_index]
            .upload_data
            .last_release_finished
            .take()
        {
            wait.wait(std::u64::MAX)
                .expect("Failed to wait for last release!");
        }
        self.frame_data[frame_index]
            .upload_data
            .last_release_finished = Some(release_has_finished);
        //Return semaphore
        self.frame_data[frame_index]
            .upload_data
            .release_has_finished
            .clone()
    }

    /// Acquires the given ranges on the transfer queue. Copies the buffers to the ranges and releases them again to the compute queue.
    fn copy_ranges(
        &mut self,
        frame_index: usize,
        ranges: &[(u64, u64, Arc<Buffer>)],
        compute_release: Arc<Semaphore>,
    ) -> Arc<Semaphore> {
        //Since the release is scheduled, we now acquire all the ranges on the
        //transfer command buffer, copy all the stuff over
        // and release the ranges again.
        let transfer_cb = self
            .transfer_command_pool
            .alloc(1, false)
            .expect("Failed to allocate transfer command buffer!")
            .into_iter()
            .nth(0)
            .expect("Failed to get first transfer command buffer!");

        transfer_cb
            .begin_recording(true, false, false, None)
            .expect("Failed to begin transfer of new data!");

        let acquire_barriers = self.compute_release_barriers(ranges);
        transfer_cb
            .cmd_pipeline_barrier(
                vk::PipelineStageFlags::COMPUTE_SHADER,
                vk::PipelineStageFlags::TRANSFER,
                vk::DependencyFlags::empty(),
                vec![],
                acquire_barriers, //Have to be the same
                vec![],
            )
            .expect("Failed to acquire buffer ranges on transfer queue!");

        for (start, size, data) in ranges.iter() {
            transfer_cb
                .cmd_copy_buffer(
                    data.clone(),
                    self.voxel_buffer.gpu_mem.clone(),
                    vec![vk::BufferCopy {
                        src_offset: 0, //The buffer range in the child always starts at 0 since its a deticated buffer
                        dst_offset: *start, //Start on the gpu voxel data
                        size: *size,   //size of the buffer
                    }],
                )
                .expect("Failed to copy buffer!");
        }

        //Now release the ranges again and shedule this cb after the release cb
        let transfer_release_barriers = self.transfer_release_barriers(ranges);

        transfer_cb
            .cmd_pipeline_barrier(
                vk::PipelineStageFlags::TRANSFER, //TODO CHECK
                vk::PipelineStageFlags::COMPUTE_SHADER,
                vk::DependencyFlags::empty(),
                vec![],
                transfer_release_barriers,
                vec![],
            )
            .expect("Failed to release buffer ranges on transfer queue!");

        transfer_cb
            .end_recording()
            .expect("Failed to end transfer command buffer recording!");
        //Now submit to transfer queue synced the the last given compute queue semaphore

        //Submit to queue
        let copy_has_finished = self
            .transfer_queue
            .queue_submit(vec![device::SubmitInfo::new(
                vec![(compute_release, vk::PipelineStageFlags::TRANSFER)], //Wait for the release
                vec![transfer_cb],
                vec![self.frame_data[frame_index]
                    .upload_data
                    .upload_has_finished
                    .clone()],
            )])
            .expect("Failed to run copy cb!");

        //Update the fence info for this frame
        if let Some(wait) = self.frame_data[frame_index]
            .upload_data
            .last_upload_finished
            .take()
        {
            wait.wait(std::u64::MAX)
                .expect("Failed to wait for last upload!");
        }
        self.frame_data[frame_index]
            .upload_data
            .last_upload_finished = Some(copy_has_finished);

        self.frame_data[frame_index]
            .upload_data
            .upload_has_finished
            .clone()
    }

    ///Builds release barriers for a given set of ranges from the transfer to the compute queue. Those are the same used to acquire the ranges on the
    /// compute queue.
    fn transfer_release_barriers(
        &self,
        ranges: &[(u64, u64, Arc<Buffer>)],
    ) -> Vec<ash::vk::BufferMemoryBarrier> {
        ranges
            .iter()
            .map(|(start, size, _buf)| {
                self.voxel_buffer.gpu_mem.buffer_barrier(
                    Some(self.transfer_queue.clone()),
                    Some(self.compute_queue.clone()),
                    Some(vk::AccessFlags::TRANSFER_WRITE),
                    Some(vk::AccessFlags::SHADER_READ),
                    Some(*start),
                    Some(*size),
                )
            })
            .collect::<Vec<vk::BufferMemoryBarrier>>()
    }
    ///Acquires the given ranges on the compute queue. Also take the semaphore that signals that copying on the transfer queue has finished.
    ///When done flags the frame index as "transfering" for the command builder which will wait for the buffer manager to finish uploading and releasing
    ///the ranges back to the compute queue.
    fn acquire_ranges_on_compute(
        &mut self,
        frame_index: usize,
        ranges: &[(u64, u64, Arc<Buffer>)],
        copy_semaphore: Arc<Semaphore>,
    ) {
        //Finally acquire the ranges back on the compute queue
        let acquire_cb = self
            .compute_command_pool
            .alloc(1, false)
            .expect("Failed to get acquire command buffer!")
            .into_iter()
            .nth(0)
            .expect("Could not get acquire command buffer from iter");

        acquire_cb
            .begin_recording(true, false, false, None)
            .expect("Failed to start acquire command buffer!");
        //release all buffer ranges
        acquire_cb
            .cmd_pipeline_barrier(
                vk::PipelineStageFlags::TRANSFER,
                vk::PipelineStageFlags::COMPUTE_SHADER,
                vk::DependencyFlags::empty(),
                vec![],
                self.transfer_release_barriers(ranges),
                vec![],
            )
            .expect("Failed to acquire buffer ranges!");

        acquire_cb
            .end_recording()
            .expect("failed to end release cb!");
        //Submit to queue
        let acquire_has_finished = self
            .compute_queue
            .queue_submit(vec![device::SubmitInfo::new(
                vec![(copy_semaphore, vk::PipelineStageFlags::ALL_COMMANDS)],
                vec![acquire_cb],
                vec![self.frame_data[frame_index]
                    .upload_data
                    .acquire_has_finished
                    .clone()],
            )])
            .expect("Failed to run re-acquire cb!");

        //Update the fence info for this frame
        if let Some(wait) = self.frame_data[frame_index]
            .upload_data
            .last_acquire_finished
            .take()
        {
            wait.wait(std::u64::MAX)
                .expect("Failed to wait for last acquire!");
        }
        //Update inner fence
        self.frame_data[frame_index]
            .upload_data
            .last_acquire_finished = Some(acquire_has_finished);
    }

    ///When the transfer queue is the same family as the compute queue we can just schedule a copy command buffer and execute it.
    ///This is a shortcut function for this behavior.
    fn copy_on_same_queue(&mut self, frame_index: usize, ranges: &[(u64, u64, Arc<Buffer>)]) {
        let transfer_cb = self
            .transfer_command_pool
            .alloc(1, false)
            .expect("Failed to allocate transfer command buffer!")
            .into_iter()
            .nth(0)
            .expect("Failed to get first transfer command buffer!");

        transfer_cb
            .begin_recording(true, false, false, None)
            .expect("Failed to begin transfer of new data!");

        for (start, size, data) in ranges.iter() {
            transfer_cb
                .cmd_copy_buffer(
                    data.clone(),
                    self.voxel_buffer.gpu_mem.clone(),
                    vec![vk::BufferCopy {
                        src_offset: 0, //The buffer range in the child always starts at 0 since its a deticated buffer
                        dst_offset: *start, //Start on the gpu voxel data
                        size: *size,   //size of the buffer
                    }],
                )
                .expect("Failed to copy buffer!");
        }

        transfer_cb
            .end_recording()
            .expect("Failed to end transfer command buffer recording!");
        //Now submit to transfer queue synced the the last given compute queue semaphore

        //Submit to queue
        let copy_has_finished = self
            .transfer_queue
            .queue_submit(vec![device::SubmitInfo::new(
                vec![], //Do not have to wait for anything
                vec![transfer_cb],
                vec![self.frame_data[frame_index]
                    .upload_data
                    .upload_has_finished
                    .clone()],
            )])
            .expect("Failed to run copy cb!");

        //Update the fence info for this frame
        if let Some(wait) = self.frame_data[frame_index]
            .upload_data
            .last_upload_finished
            .take()
        {
            wait.wait(std::u64::MAX)
                .expect("Failed to wait for last upload!");
        }
        self.frame_data[frame_index]
            .upload_data
            .last_upload_finished = Some(copy_has_finished);
    }

    ///Runs maintenance work like uploading new meshes, recycling buffers and taking care of the
    ///Voxel cache on the GPU
    pub fn update_manager(&mut self, frame_index: usize) {
        //Quick check if there are any updates to do, otherwise we can already return
        if self.load_calls.len() < 1 && self.unload_calls.len() < 1 {
            return;
        }

        //Take all load calls into local array.
        let mut to_load_objects = Vec::with_capacity(self.load_calls.len());
        std::mem::swap(&mut to_load_objects, &mut self.load_calls);

        //First of all sort out all voxels and triangles to load
        let (voxel_load, triangle_load) = to_load_objects.into_iter().fold(
            (Vec::new(), Vec::new()),
            |(mut voxl, mut tril), loadc| {
                match loadc {
                    LoadCall::Mesh(mesh_id) => tril.push(mesh_id),
                    LoadCall::VoxelObject { data_id, data } => voxl.push((data_id, data)),
                }

                (voxl, tril)
            },
        );

        //Find ranges for the voxel loads and assign them a cpu_local buffer.
        //Format is          start  size   data_to_copy
        let ranges: Vec<(u64, u64, Arc<Buffer>)> = if voxel_load.len() > 0 {
            self.sort_voxel_load_op(voxel_load)
        } else {
            vec![]
        };

        //Only process the release/acquire routine if there are ranges:
        if ranges.len() > 0 {
            //When transfer != compute queue_family we'll release the ranges, acquire them on the transfer
            // queue, copy, then release them back to the compute queue and acquire again.

            //However when transfer == compute queue family, we only shedule the copy command.

            if self.transfer_queue.get_family().get_family_index()
                == self.compute_queue.get_family().get_family_index()
            {
                self.copy_on_same_queue(frame_index, &ranges);
            } else {
                //RELEASE ----------------------------------------------------------
                let release_semaphore = self.release_ranges_on_compute(frame_index, &ranges);
                //ACQUIRE/UPLOAD/RELEASE ----------------------------------------------------------
                let copy_semaphore = self.copy_ranges(frame_index, &ranges, release_semaphore);
                //ACQUIRE ----------------------------------------------------------
                self.acquire_ranges_on_compute(frame_index, &ranges, copy_semaphore);
            }

            //Now flag the index for waiting when starting a new frame
            self.frame_data[frame_index].upload_data.requires_wait = true;
        }

        //TODO
        //For the vertex/index buffers, use copy_sync and acquire them on a separate
        //graphics acquire command buffer.
        for mesh_id in triangle_load {
            self.load_mesh(mesh_id);
        }
    }

    ///Adds a mesh from data returns the Id under which it is referenced.
    pub fn add_triangle_mesh(&mut self, object: VertexMeshBuilder) -> MeshId {
        //Find a mesh id and add the data to the manager, the mesh gets loaded when needed.
        if self.free_triangle_data_slots.len() > 0 {
            let id = self
                .free_triangle_data_slots
                .pop()
                .expect("Failed to pop free triangle idx");

            let mesh_id = MeshId { id };
            //Build the actual triangle mesh
            let triangle_mesh = object.to_mesh(mesh_id);
            let data = TriangleData {
                mesh: triangle_mesh,

                vertex_buffer: None,
                index_buffer: None,
            };
            //Override
            self.triangle_data[id] = data;

            mesh_id
        } else {
            let id = self.triangle_data.len();
            let mesh_id = MeshId { id };
            //Build the actual triangle mesh
            let triangle_mesh = object.to_mesh(mesh_id);
            let data = TriangleData {
                mesh: triangle_mesh,

                vertex_buffer: None,
                index_buffer: None,
            };
            //Push new data
            self.triangle_data.push(data);

            mesh_id
        }
    }

    ///Adds a request to load this mesh into GPU memory. Does nothing if there is no mesh under that Id
    fn load_mesh(&mut self, mesh_id: MeshId) {
        let (vertices, indices) = if let Some(ref data) = self.triangle_data.get(mesh_id.id) {
            (data.mesh.vertices.clone(), data.mesh.indices.clone())
        } else {
            return;
        };

        let (vertex_upload, vertex_buffer) = Buffer::from_data(
            self.device.clone(),
            self.graphics_queue.clone(),
            vertices,
            BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly,
        )
        .expect("Failed to load vertex buffer");
        vertex_upload
            .wait(std::u64::MAX)
            .expect("Failed to upload!");

        let (index_upload, index_buffer) = Buffer::from_data(
            self.device.clone(),
            self.graphics_queue.clone(),
            indices,
            BufferUsage {
                index_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly,
        )
        .expect("Failed to load index buffer");
        index_upload.wait(std::u64::MAX).expect("Failed to upload!");

        //We can "unwrap" since we got the data immutable already
        let mut data = self
            .triangle_data
            .get_mut(mesh_id.id)
            .expect("Could not get data");

        data.vertex_buffer = Some(vertex_buffer);
        data.index_buffer = Some(index_buffer);
    }

    ///Adds a notification that this mesh id can be unloaded from gpu memory
    fn unload_mesh(&mut self, mesh_id: MeshId) {
        if let Some(ref mut data) = self.triangle_data.get_mut(mesh_id.id) {
            let _v_buf = data.vertex_buffer.take();
            let _i_buf = data.index_buffer.take();
            //now go out of scopre which will delte the buffers
        }
    }

    pub fn get_mesh(&self, id: MeshId) -> Option<&TriangleMesh> {
        if let Some(data) = self.triangle_data.get(id.id) {
            Some(&data.mesh)
        } else {
            None
        }
    }

    pub fn get_mesh_vertex_buffer(&self, mesh_id: &MeshId) -> Option<Arc<Buffer>> {
        if let Some(mesh) = self.triangle_data.get(mesh_id.id) {
            mesh.vertex_buffer.clone()
        } else {
            None
        }
    }

    pub fn get_mesh_index_buffer(&self, mesh_id: &MeshId) -> Option<Arc<Buffer>> {
        if let Some(mesh) = self.triangle_data.get(mesh_id.id) {
            mesh.index_buffer.clone()
        } else {
            None
        }
    }

    ///Notifies the buffer manager that the mesh under this id was needed
    pub fn notify_mesh_needed(&mut self, mesh_id: &MeshId) {
        self.load_calls.push(LoadCall::Mesh(*mesh_id));
    }

    ///Notifies the buffer manager that this mesh is not needed anymore
    pub fn notify_mesh_not_needed(&mut self, mesh_id: &MeshId) {
        self.unload_calls.push(UnloadCall::Mesh(*mesh_id));
    }

    ///Adds a voxel mesh to the buffer_manager. This voxel-object however does not have to be loaded.
    ///If the voxel object already existed, it is overwritten.
    pub fn upload_voxel_object(&mut self, voxel_id: &VoxelDataId, voxel_data: Vec<GpuVoxel>) {
        let data_range = VoxelObjectData {
            data_range: None, //TODO Actually set
        };

        let old_entry = self.voxel_data.insert(*voxel_id, data_range);

        if let Some(olde) = old_entry {
            if let Some(r) = olde.data_range {
                //Notify manager that range is unused
                self.voxel_buffer.remove(r);
            }
        };

        self.load_calls.push(LoadCall::VoxelObject {
            data_id: *voxel_id,
            data: voxel_data,
        })
    }

    ///Removes the voxel object from memory on next update
    pub fn unload_voxel_object(&mut self, voxel_id: &VoxelDataId) {
        let data_range = if let Some(r) = self.voxel_data.remove(voxel_id) {
            r
        } else {
            return;
        };

        //notify that the manager can release this range.
        if let Some(r) = data_range.data_range {
            self.voxel_buffer.remove(r);
        }
    }

    ///Queues buffer updates into the command buffer.
    ///Assumes that the command buffer is not within a renderpass.
    /// Updates contain:
    /// - Camera buffer update,
    /// - Dynamic Voxel Data
    /// - Camera settings update (Iso, aperture, etc.)
    pub fn update_frame_data(
        &mut self,
        index: usize,
        command_buffer: Arc<command_buffer::CommandBuffer>,
        camera: ShaderCamera,
        active_voxel_objects: Vec<(VoxelDataId, GpuVoxelObject)>,
        mut toplevel_bvh: Vec<GpuBvhNode>,
    ) {
        command_buffer
            .cmd_update_buffer(
                self.frame_data[index].graphics_camera_buffer.clone(),
                0,
                &[camera.clone()],
            )
            .expect("Failed to update frames camera buffer!");

        command_buffer
            .cmd_update_buffer(
                self.frame_data[index].compute_camera_buffer.clone(),
                0,
                &[camera],
            )
            .expect("Failed to update frames camera buffer!");

        //Now schedule a update of this frame voxel objects.
        let mut filtered_voxel_objects = active_voxel_objects
            .into_iter()
            .map(|(id, mut gpu_obj)| {
                //Check if there is a memory range allocated for this object on the gpu.
                //If thats the case, update the GpuVoxelobject to use the correct range.

                //otherwise mark the voxel as un-allocated.
                //We cannot just remove the object, since the toplevel bvh is build for this set of objects.
                if let Some(voxel_range_data) = self.voxel_data.get(&id) {
                    if let Some(ref range) = voxel_range_data.data_range {
                        //update gpu data range
                        let gpu_offset = range.start.try_into().expect("range start exceeds u32");
                        let gpu_size = range.size.try_into().expect("range size exceeds u32");
                        //Now setup rest of gpu voxel object representation. Model and invModel matrix where set in the scene.rs
                        gpu_obj.setup_buffer_range(gpu_offset, gpu_size);
                        gpu_obj.is_not_allocated = 0;
                        gpu_obj
                    } else {
                        gpu_obj.is_not_allocated = 1;
                        gpu_obj
                    }
                } else {
                    gpu_obj.is_not_allocated = 1;
                    gpu_obj
                }
            })
            .collect::<Vec<GpuVoxelObject>>();

        //Create an empty buffer if there are no voxel-objects
        if filtered_voxel_objects.len() <= 0 {
            filtered_voxel_objects =
                vec![GpuVoxelObject::empty_with_transform(na::Matrix4::identity())];

            toplevel_bvh = vec![GpuBvhNode {
                min: [0.0; 4],
                max: [0.0; 4],
                skip_idx: std::u32::MAX,
                is_leaf: 0,
                object_index: 0,
                pad: 0,
            }];
        }

        //Now update the voxel-object list on the gpu for this frame as well as the bvh buffer.
        self.frame_data[index].voxel_object_buffer = buffer::Buffer::new(
            self.device.clone(),
            (std::mem::size_of::<GpuVoxelObject>() * filtered_voxel_objects.len()) as u64,
            BufferUsage {
                transfer_dst: true,
                storage_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly,
        )
        .expect("Failed to create voxel object list for frame!");
        //Staging buffer from the cpu for the voxel object data
        let staging_buffer_voxel_object = buffer::Buffer::new(
            self.device.clone(),
            (std::mem::size_of::<GpuVoxelObject>() * filtered_voxel_objects.len()) as u64,
            BufferUsage {
                transfer_src: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::CpuToGpu,
        )
        .expect("Failed to create staging buffer for upload!");

        //Map our usage data to the staging buffer
        staging_buffer_voxel_object
            .map_data(filtered_voxel_objects)
            .expect("Failed to map data to staging voxel-object-buffer!");

        self.frame_data[index].toplevel_bvh_buffer = buffer::Buffer::new(
            self.device.clone(),
            (std::mem::size_of::<GpuBvhNode>() * toplevel_bvh.len()) as u64,
            BufferUsage {
                transfer_dst: true,
                storage_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly,
        )
        .expect("Failed to create new gpu local bvh buffer");

        let staging_buffer_toplevel_bvh = buffer::Buffer::new(
            self.device.clone(),
            (std::mem::size_of::<GpuBvhNode>() * toplevel_bvh.len()) as u64,
            BufferUsage {
                transfer_src: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::CpuToGpu,
        )
        .expect("Failed to create staging buffer for upload!");

        staging_buffer_toplevel_bvh
            .map_data(toplevel_bvh)
            .expect("Failed to map data to staging toplevel-bvh-buffer!");

        command_buffer
            .cmd_pipeline_barrier(
                vk::PipelineStageFlags::ALL_COMMANDS,
                vk::PipelineStageFlags::TRANSFER,
                vk::DependencyFlags::empty(),
                vec![],
                vec![
                    self.frame_data[index].voxel_object_buffer.buffer_barrier(
                        None,
                        None,
                        None,
                        Some(vk::AccessFlags::TRANSFER_WRITE),
                        None,
                        None,
                    ),
                    staging_buffer_voxel_object.buffer_barrier(
                        None,
                        None,
                        None,
                        Some(vk::AccessFlags::TRANSFER_READ),
                        None,
                        None,
                    ),
                    self.frame_data[index].toplevel_bvh_buffer.buffer_barrier(
                        None,
                        None,
                        None,
                        Some(vk::AccessFlags::TRANSFER_WRITE),
                        None,
                        None,
                    ),
                    staging_buffer_toplevel_bvh.buffer_barrier(
                        None,
                        None,
                        None,
                        Some(vk::AccessFlags::TRANSFER_READ),
                        None,
                        None,
                    ),
                ],
                vec![],
            )
            .expect("Failed to schedule voxel-object-buffer initial layout transfer!");

        command_buffer
            .cmd_copy_buffer(
                staging_buffer_voxel_object,
                self.frame_data[index].voxel_object_buffer.clone(),
                vec![vk::BufferCopy {
                    src_offset: 0,
                    dst_offset: 0,
                    size: self.frame_data[index].voxel_object_buffer.size(),
                }],
            )
            .expect("Failed to copy voxel-object-buffer data to gpu buffer!");

        command_buffer
            .cmd_copy_buffer(
                staging_buffer_toplevel_bvh,
                self.frame_data[index].toplevel_bvh_buffer.clone(),
                vec![vk::BufferCopy {
                    src_offset: 0,
                    dst_offset: 0,
                    size: self.frame_data[index].toplevel_bvh_buffer.size(),
                }],
            )
            .expect("Failed to schedule toplevel bvh buffer copy CPU->GPU!");

        //Now transition the gpu buffer back to the shader_read stage for later
        command_buffer
            .cmd_pipeline_barrier(
                vk::PipelineStageFlags::TRANSFER,
                vk::PipelineStageFlags::ALL_COMMANDS,
                vk::DependencyFlags::empty(),
                vec![],
                vec![
                    self.frame_data[index].voxel_object_buffer.buffer_barrier(
                        None,
                        None,
                        Some(vk::AccessFlags::TRANSFER_WRITE),
                        Some(vk::AccessFlags::SHADER_READ),
                        None,
                        None,
                    ),
                    self.frame_data[index].toplevel_bvh_buffer.buffer_barrier(
                        None,
                        None,
                        Some(vk::AccessFlags::TRANSFER_WRITE),
                        Some(vk::AccessFlags::SHADER_READ),
                        None,
                        None,
                    ),
                ],
                vec![],
            )
            .expect("Failed to transition voxel-object-buffer to shader-read state!");
    }
}
