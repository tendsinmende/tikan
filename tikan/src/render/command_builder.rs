/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::data::{
    bvh::GpuBvhNode,
    camera::ShaderCamera,
    components::transform::Transform,
    triangle_object::*,
    voxel_data_manager::VoxelDataId,
    voxel_object::{GpuVoxelObject, VoxelObject, VoxelObjectId},
};

use crate::render::{
    assemble_pass::*,
    buffer_manager::BufferManager,
    debug_pass::*,
    merge_pass::*,
    pass::*,
    primary_pass::*,
    radiance_denoiser::*,
    radiance_pass::{RadianceData, RadiancePass},
    specular_pass::{SpecularData, SpecularPass},
    FrameInfo, InterfaceInfo,
};

use marp::ash::vk;
use marp::buffer::{SharingMode, UploadStrategy};
use marp::image::AbstractImage;
use marp::memory::MemoryUsage;
use marp::*;

use marp::command_buffer::*;
use std::sync::{Arc, RwLock};

use crate::config::Config;

///Shared resources between all renderpasses
pub struct SharedResources {
    blue_noise: (Arc<image::Image>, Arc<sampler::Sampler>),
}

impl SharedResources {
    pub fn new(device: Arc<device::Device>, compute_queue: Arc<device::Queue>) -> Self {
        let bluenoise_bitmap = crate::image::open("tikan/resources/textures/LDR_RGBA_0.png")
            .expect("Failed to load bluenoise image")
            .to_rgba8();

        let dim = bluenoise_bitmap.dimensions();
        let data: Vec<_> = bluenoise_bitmap.into_raw();

        let blue_noise_image = image::Image::new(
            device.clone(),
            image::ImageInfo::new(
                image::ImageType::Image2D {
                    width: dim.0,
                    height: dim.1,
                    samples: 1,
                },
                vk::Format::R8G8B8A8_UNORM,
                None, //No extra ordinary component mapping
                Some(image::MipLevel::Specific(1)),
                image::ImageUsage {
                    sampled: true,
                    transfer_dst: true,
                    color_attachment: true,
                    storage: true,
                    ..Default::default()
                },
                MemoryUsage::GpuOnly,
                None, //Optimal tiling by default
            ),
            SharingMode::Exclusive,
        )
        .expect("Failed to create bluenoise image");

        //Upload to gpu
        let f = blue_noise_image
            .copy(compute_queue.clone(), data)
            .expect("Failed to upload bluenoise image to gpu");

        f.wait(std::u64::MAX).expect("Failed to wait for fence");

        //Transition to general layout
        let cb_pool = CommandBufferPool::new(
            device.clone(),
            compute_queue.clone(),
            vk::CommandPoolCreateFlags::empty(),
        )
        .expect("Failed to create transition command pool for bluenoise image");

        let transition_cb = cb_pool
            .alloc(1, false)
            .expect("Could not get new command buffer!")
            .into_iter()
            .nth(0)
            .expect("Could not get compute command buffer!");

        transition_cb
            .begin_recording(true, false, false, None)
            .expect("Failed to start transition command buffer for bluenoise image");

        transition_cb
            .cmd_pipeline_barrier(
                vk::PipelineStageFlags::ALL_COMMANDS,
                vk::PipelineStageFlags::ALL_COMMANDS,
                vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![blue_noise_image.new_image_barrier(
                    Some(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                    Some(vk::ImageLayout::GENERAL),
                    None,
                    None,
                    None,
                    None,
                    None,
                )],
            )
            .expect("Failed to schedule bluenoise transition to general layout!");

        transition_cb
            .end_recording()
            .expect("Failed to end bluenoise transition cb");

        let transition_finished = compute_queue
            .queue_submit(vec![device::SubmitInfo::new(
                vec![],
                vec![transition_cb],
                vec![],
            )])
            .expect("Failed to execute bluenoise transition!");
        transition_finished
            .wait(std::u64::MAX)
            .expect("Failed to wait for bluenoise transition!");
        println!("Ping!");

        //Now create the sampler and store both
        let mut sampler_builder = sampler::SamplerBuilder::default();
        /*
        sampler_builder.mag_filter = ash::vk::Filter::NEAREST;
        sampler_builder.min_filter = ash::vk::Filter::NEAREST;
        sampler_builder.mipmap_mode = ash::vk::SamplerMipmapMode::NEAREST;
        */
        let sampler = sampler_builder
            .build(device.clone())
            .expect("Failed to build sampler!");

        SharedResources {
            blue_noise: (blue_noise_image, sampler),
        }
    }

    pub fn get_bluenoise(&self) -> (Arc<image::Image>, Arc<sampler::Sampler>) {
        self.blue_noise.clone()
    }
}

///Handles the generation of several command buffers and arranging/ syncing them.
pub struct CommandBuilder {
    ///Engine config pointer. Reads render related data like voxel tracing quality etc.
    config: Arc<RwLock<Config>>,

    device: Arc<device::Device>,

    ///holds a possible interface that can be used as a host for the final rendered image
    interface: Option<InterfaceInfo>,
    ///Create new command buffers when calling build_new_frame for the graphics commands
    graphics_command_pool: Arc<command_buffer::CommandBufferPool>,
    ///Creates the command buffers for the compute queue.
    compute_command_pool: Arc<command_buffer::CommandBufferPool>,

    graphics_queue: Arc<device::Queue>,
    compute_queue: Arc<device::Queue>,

    ///Saves all immutable shared data
    shared_resources: SharedResources,

    ///Executes the primary ray raytracing step and provides the gbuffer to other
    /// passes.
    primary_pass: Pass<PrimaryData>,

    ///Assembles the inputs needed to finish the primary pass. Currently that is the the
    ///final image of the primary reflections as well as the primary passes luminance.
    primary_assemble: Pass<AssembleData>,

    ///Holds all objects needed to execute the debug pass.
    debug_pass: Arc<DebugPass>,

    ///Holds the pass thats going to combine raytraced and rasterized results into one image. Later the merge
    /// pass might add some additional post progress.
    merge_pass: Pass<MergeData>,

    ///Takes the Radiance image of an frame and tries to denoise it via some context information from the primary pass.
    radiance_denoiser: Pass<RadianceDenoiserData>,

    ///The radiance pass accumulates the incoming radiance of each influencing light for a given pixel.
    radiance_pass: Pass<RadianceData>,

    ///The specular pass cone traces the scene and outputs the final specular component based on the given input.
    specular_pass: Pass<SpecularData>,

    ///Tracks the current swapchain extent
    swapchain_extent: (u32, u32),
    /// The last swapchain image count. This could change between frames.
    swapchain_image_count: usize,
    ///Tracks the extent of the scene framebuffer. This is different to the swapchain extent, when
    /// there is an interface. There is no area if no interface is set.
    render_area: Option<(u32, u32)>,
}

impl CommandBuilder {
    ///Creates a new CommandBuilds with all sub systems
    pub fn new(
        config: Arc<RwLock<Config>>,
        device: Arc<device::Device>,
        graphics_queue: Arc<device::Queue>,
        compute_queue: Arc<device::Queue>,
        buffer_manager: &BufferManager,
        swapchain_image_count: usize,
        swapchain_extent: (u32, u32),
    ) -> CommandBuilder {
        let graphics_command_pool = command_buffer::CommandBufferPool::new(
            device.clone(),
            graphics_queue.clone(),
            ash::vk::CommandPoolCreateFlags::empty(),
        )
        .expect("Could not create graphics command pool!");

        let compute_command_pool = command_buffer::CommandBufferPool::new(
            device.clone(),
            compute_queue.clone(),
            ash::vk::CommandPoolCreateFlags::empty(),
        )
        .expect("Could not create compute command pool!");

        let shared_res = SharedResources::new(device.clone(), compute_queue.clone());

        let debug_pass = DebugPass::new(
            device.clone(),
            swapchain_image_count,
            swapchain_extent,
            graphics_queue.clone(),
            buffer_manager,
        );

        let primary_pass: Pass<PrimaryData> = PrimaryPass::new(
            device.clone(),
            graphics_queue.clone(),
            compute_queue.clone(),
            swapchain_image_count,
            swapchain_extent,
            buffer_manager,
        );

        let radiance_pass: Pass<RadianceData> = RadiancePass::new(
            device.clone(),
            graphics_queue.clone(),
            compute_queue.clone(),
            swapchain_image_count,
            swapchain_extent,
            shared_res.get_bluenoise(),
            primary_pass.get_gbuffer(),
            buffer_manager,
        );

        let mut denoiser_images = Vec::with_capacity(swapchain_image_count);
        for i in 0..swapchain_image_count {
            denoiser_images.push((
                primary_pass.get_normal_depth_image(i),
                radiance_pass.get_final_image(i),
                primary_pass.get_albedo_roughness_image(i),
            ));
        }

        let radiance_denoiser: Pass<RadianceDenoiserData> = RadianceDenoiser::new(
            device.clone(),
            graphics_queue.clone(),
            compute_queue.clone(),
            swapchain_image_count,
            swapchain_extent,
            denoiser_images,
        );

        let specular_pass: Pass<SpecularData> = SpecularPass::new(
            device.clone(),
            graphics_queue.clone(),
            compute_queue.clone(),
            swapchain_image_count,
            swapchain_extent,
            primary_pass.get_gbuffer(),
            buffer_manager,
        );

        //Assembles the specular part and the scenes incoming radiance into the final image
        let mut primary_assemble_images = Vec::with_capacity(swapchain_image_count);
        for i in 0..swapchain_image_count {
            primary_assemble_images.push((
                specular_pass.get_final_image(i),
                radiance_pass.get_final_image(i),
            ));
        }

        let primary_assemble: Pass<AssembleData> = AssemblePass::new(
            device.clone(),
            graphics_queue.clone(),
            compute_queue.clone(),
            swapchain_image_count,
            swapchain_extent,
            primary_pass.get_gbuffer(),
            primary_assemble_images,
            buffer_manager,
        );

        let mut merge_images = Vec::with_capacity(swapchain_image_count);
        for i in 0..swapchain_image_count {
            merge_images.push((
                debug_pass.get_color_image(i),
                primary_assemble
                    .get_data(i)
                    .expect("Could not get primary passes final image!")
                    .final_image
                    .clone(),
            ));
        }

        let merge_pass: Pass<MergeData> = MergePass::new(
            device.clone(),
            graphics_queue.clone(),
            compute_queue.clone(),
            swapchain_image_count,
            swapchain_extent,
            merge_images,
            buffer_manager,
        );

        CommandBuilder {
            config,

            device,

            interface: None,
            graphics_command_pool,
            compute_command_pool,

            graphics_queue,
            compute_queue,

            shared_resources: shared_res,

            primary_pass,
            primary_assemble,
            debug_pass,
            merge_pass,
            radiance_denoiser,
            radiance_pass,
            specular_pass,

            swapchain_extent: swapchain_extent,
            swapchain_image_count,
            render_area: None,
        }
    }

    ///Updates all dynamic descriptors for this frame
    fn update_descriptor_sets(&self, frame_index: usize, buffer_manager: &BufferManager) {
        //The primary pass voxel_object_buffer gets updated
        self.primary_pass
            .update_descriptor_sets(frame_index, buffer_manager);
        self.radiance_pass
            .update_descriptor_sets(frame_index, buffer_manager);
        self.specular_pass
            .update_descriptor_sets(frame_index, buffer_manager);
    }

    ///Creates a command buffer that updates buffers and executes the primary ray pass.
    ///Returns a fence representing the moment this pass has executed
    fn prepare_and_primary_rays(
        &self,
        frame_info: &FrameInfo,
        buffer_manager: &mut BufferManager,
        camera: ShaderCamera,
        active_voxel_objects: Vec<(VoxelDataId, GpuVoxelObject)>,
        toplevel_bvh: Vec<GpuBvhNode>,
    ) -> sync::QueueFence {
        //--------PREPARING FRAME DATA--------
        //Before we can start our work, the systems have to update all underling data. This
        //contains mostly dynamic buffers for the objects and the camera as well as
        //updating descriptor sets to contain the new data.
        let compute_command_buffer = self
            .compute_command_pool
            .alloc(1, false)
            .expect("Could not get new command buffer!")
            .into_iter()
            .nth(0)
            .expect("Could not get compute command buffer!");

        compute_command_buffer
            .begin_recording(true, false, false, None)
            .expect("Could not begin debug command buffer!");

        //Mark region if in debug mode
        if self.in_debugger() {
            //compute_command_buffer.debug_marker_begin_region("Updating Buffermanager", [0.2, 0.2, 1.0, 1.0]);
        }

        //Update frame dependent data
        buffer_manager.update_frame_data(
            frame_info.index,
            compute_command_buffer.clone(),
            camera,
            active_voxel_objects,
            toplevel_bvh,
        );

        //Now update all descriptor sets
        self.update_descriptor_sets(frame_info.index, buffer_manager);

        if self.in_debugger() {
            //compute_command_buffer.debug_marker_end_region();
        }

        //--------PRIMARY START--------
        //1. Generate the primary rays and therefore the Gbuffer neeeded for all other raytracing passes
        // and the debug triangle based pass.

        if self.in_debugger() {
            //compute_command_buffer.debug_marker_begin_region("Tracing Primary Rays", [0.1, 0.1, 1.0, 1.0]);
        }

        //Add primary call to cb
        self.primary_pass
            .execute(frame_info.index, compute_command_buffer.clone());

        if self.in_debugger() {
            //compute_command_buffer.debug_marker_end_region();
        }

        compute_command_buffer
            .end_recording()
            .expect("Failed to end primary command buffer!");

        //When submitting we might have to wait for upload operations that occured, therefore check
        let wait_semaphores =
            if let Some(sem) = buffer_manager.get_upload_semaphore(frame_info.index) {
                vec![(sem, vk::PipelineStageFlags::ALL_COMMANDS)]
            } else {
                vec![]
            };

        //Already start this command buffer and let it signal the primary_finished semaphore.
        let primary_pass_finished = self
            .compute_queue
            .queue_submit(vec![device::SubmitInfo::new(
                wait_semaphores, //Wait for nothing since we are the first cb on this frame
                vec![compute_command_buffer],
                //Signal the two primary pass semaphore for the compute and graphics queue
                vec![
                    frame_info.primary_pass_finished_cmp.clone(),
                    frame_info.primary_pass_finished_gra.clone(),
                ],
            )])
            .expect("Failed to submit command buffer!");
        //--------PRIMARY FINISHED--------

        primary_pass_finished
    }

    ///Renders the debug pass as well as the interface (if there is any) on the graphics queue.
    ///Gets synchronized to the primary pass and signals its own fences.
    ///Runs parallel to the other raytracing passes
    pub fn render_debug_and_ui(
        &self,
        frame_info: &FrameInfo,
        buffer_manager: &mut BufferManager,
        mesh_draw_calls: Vec<(MeshId, Transform)>,
    ) -> sync::QueueFence {
        //--------UI AND DEBUG PASS--------
        //1. Build the actual draw calls from the meshIds
        let (mut line_meshes, mut triangle_meshes) = (Vec::new(), Vec::new());
        for (mesh_id, transform) in mesh_draw_calls {
            if let Some(mesh_data) = buffer_manager.get_mesh(mesh_id) {
                //Now try to get the buffers, if there is one missing, notify the buffer manager that the
                // mesh has to be loaded. This will trigger an upload
                //next time it is possible
                let vertex_buffer =
                    if let Some(vb) = buffer_manager.get_mesh_vertex_buffer(&mesh_id) {
                        vb
                    } else {
                        //Is not loaded, add notification then skip
                        buffer_manager.notify_mesh_needed(&mesh_id);
                        continue;
                    };

                let index_buffer = if let Some(ib) = buffer_manager.get_mesh_index_buffer(&mesh_id)
                {
                    ib
                } else {
                    //Is not loaded, add notification then skip
                    buffer_manager.notify_mesh_needed(&mesh_id);
                    continue;
                };

                //Now check which pipeline we have to use and add the drawcall
                match mesh_data.draw_type() {
                    DebugDrawType::Filled(col) => {
                        //Add to color meshes
                        triangle_meshes.push(MeshDrawCall {
                            index_count: mesh_data.indice_count(),
                            vertex_buffer: vertex_buffer,
                            index_buffer: index_buffer,
                            transform: transform.to_matrix4().into(),
                            color: col,
                        });
                    }
                    DebugDrawType::Lines(col) => {
                        //Add to lines
                        line_meshes.push(MeshDrawCall {
                            index_count: mesh_data.indice_count(),
                            vertex_buffer: vertex_buffer,
                            index_buffer: index_buffer,
                            transform: transform.to_matrix4().into(),
                            color: col,
                        })
                    }
                }
            } else {
                println!("No data under id!");
                //Currently always loading all meshes, later we can only load those who are needed in some way
                buffer_manager.notify_mesh_needed(&mesh_id);
            }
        }

        //2. GraphicsQueue: In part 2 we render the traingle based debug pass and a possible
        // ui on the graphics queue.
        let graphics_command_buffer = self
            .graphics_command_pool
            .alloc(1, false)
            .expect("Could not get new command buffer!")
            .into_iter()
            .nth(0)
            .expect("Could not get graphics command buffer!");

        graphics_command_buffer
            .begin_recording(true, false, false, None)
            .expect("Could not begin debug command buffer!");

        if self.in_debugger() {
            //graphics_command_buffer.debug_marker_begin_region("UI and Debug draw calls", [1.0, 0.2, 0.2, 1.0]);
        }

        //Acquire depth image from compute queue. Was released in the last part of the primary_pass.
        graphics_command_buffer
            .cmd_pipeline_barrier(
                ash::vk::PipelineStageFlags::TRANSFER,
                ash::vk::PipelineStageFlags::FRAGMENT_SHADER,
                ash::vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![
                    //Acquire image on graphics queue.
                    self.primary_pass
                        .get_graphics_image(frame_info.index)
                        .new_image_barrier(
                            Some(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                            Some(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                            Some(self.compute_queue.clone()),
                            Some(self.graphics_queue.clone()),
                            None,
                            None,
                            None,
                        ),
                ],
            )
            .expect("Could not submit graphics queue acquire barrier");

        if self.in_debugger() {
            //graphics_command_buffer.debug_marker_insert("Execute Debug draw calls", [1.0, 0.1, 0.1, 1.0]);
        }
        //Add debug pass calls
        self.debug_pass.execute_pass(
            frame_info,
            graphics_command_buffer.clone(),
            (triangle_meshes, line_meshes),
        );

        //Add UI calls if there is a UI.
        if let Some(ref interface) = self.interface {
            //TODO actually handle the return state in the window.

            if self.in_debugger() {
                //graphics_command_buffer.debug_marker_insert("UI Rendering", [1.0, 0.1, 0.1, 1.0]);
            }
            let _ret_state = interface.interface.render(graphics_command_buffer.clone());
        }

        if self.in_debugger() {
            //graphics_command_buffer.debug_marker_end_region();
        }
        //end the graphics cb and submit to the graphics queue
        graphics_command_buffer
            .end_recording()
            .expect("Failed to end debug pass command buffer!");

        let debug_pass_finshed = self
            .graphics_queue
            .queue_submit(vec![device::SubmitInfo::new(
                //Wait for the primary pass
                vec![(
                    frame_info.primary_pass_finished_gra.clone(),
                    vk::PipelineStageFlags::COMPUTE_SHADER,
                )],
                vec![graphics_command_buffer],
                vec![frame_info.debug_ui_finished.clone()],
            )])
            .expect("Failed to execute UI and Debug pass!");

        //--------UI AND DEBUG PASS FINISHED--------
        debug_pass_finshed
    }

    ///Executes all raytracing passes based on the gbuffer.
    ///This currently includes radiance, gi and specular component in parallel, when they
    ///finished it executes a assemble pass that take the outcome of those images and assembles a final,
    ///raytraced image.
    fn execute_ray_tracing(&self, frame_info: &FrameInfo) -> sync::QueueFence {
        //--------RAY-TRACING--------
        //2. ComputeQueue: While the graphics queue does other stuff, we raytrace the whole voxels scene in
        // several passes and synchronize them.
        let rt_command_buffer = self
            .compute_command_pool
            .alloc(1, false)
            .expect("Failed to allocate the raytracing command buffer")
            .into_iter()
            .nth(0)
            .expect("Failed to get raytracing command buffer!");

        rt_command_buffer
            .begin_recording(true, false, false, None)
            .expect("Failed to begin raytracing command buffer!");

        if self.in_debugger() {
            //rt_command_buffer.debug_marker_begin_region("Second stage raytracing", [0.2, 1.0, 0.2, 1.0]);
            //rt_command_buffer.debug_marker_insert("Radiance Pass", [0.1, 1.0, 0.1, 1.0]);
        }
        //execute radiance pass...
        self.radiance_pass
            .execute(frame_info.index, rt_command_buffer.clone());

        if self.in_debugger() {
            //rt_command_buffer.debug_marker_insert("Specular Pass", [0.1, 1.0, 0.1, 1.0]);
        }
        //... And specular pass in parallel
        self.specular_pass
            .execute(frame_info.index, rt_command_buffer.clone());

        //Now wait for both parts to finish
        rt_command_buffer
            .cmd_pipeline_barrier(
                vk::PipelineStageFlags::COMPUTE_SHADER,
                vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![],
            )
            .expect("Failed to wait for radiance and specular raytracing passes to finish");

        if self.in_debugger() {
            //rt_command_buffer.debug_marker_insert("Radiance Denoiser", [0.1, 1.0, 0.1, 1.0]);
        }
        //Now execute radiance and (maybe later specular) pass denoisers.
        self.radiance_denoiser
            .execute(frame_info.index, rt_command_buffer.clone());

        //Now wait for the denoisers to finsh to assemble the frame
        rt_command_buffer
            .cmd_pipeline_barrier(
                vk::PipelineStageFlags::COMPUTE_SHADER,
                vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![],
            )
            .expect("Failed to wait for denoisers to finish");

        if self.in_debugger() {
            //rt_command_buffer.debug_marker_insert("Assemble radiance, specular and primary ray into final", [0.1, 1.0, 0.1, 1.0]);
        }
        //Now assemble both tracing passes into a final raytraced image based on the gbuffer, incoming diffuse radiance and specular component.
        self.primary_assemble.execute(
            frame_info.index,
            rt_command_buffer.clone(),
            false, //We need the queue change since the image is later used in the merge pass
        );

        if self.in_debugger() {
            //rt_command_buffer.debug_marker_end_region();
        }

        rt_command_buffer
            .end_recording()
            .expect("Failed to end raytracing command buffer recording!");

        //Submit the raytracing and signal semaphore
        let rt_pass_finshed = self
            .compute_queue
            .queue_submit(vec![device::SubmitInfo::new(
                //Wait for the primary pass to execute
                vec![(
                    frame_info.primary_pass_finished_cmp.clone(),
                    vk::PipelineStageFlags::COMPUTE_SHADER,
                )],
                vec![rt_command_buffer],
                vec![frame_info.raytracing_finished.clone()],
            )])
            .expect("Failed to execute UI and Debug pass!");
        //--------RAY-TRACING FINISHED--------
        rt_pass_finshed
    }

    ///Merges the compute shaders raytraced image into the ui/debug pass image and copies it to the swapchain image.
    fn merge_and_copy_to_swapchain(
        &self,
        frame_info: &FrameInfo,
        swapchain_image: Arc<image::SwapchainImage>,
    ) -> sync::QueueFence {
        //--------ASSEMBLE DEBUG AND RAY-TRACING--------
        //3. The last part of the frame acquires the final raytracing image from the compute queue,
        // mixes it with the debug output, performs some post progress and finally, if needed mixes it with the
        // UI.

        let merge_command_buffer = self
            .graphics_command_pool
            .alloc(1, false)
            .expect("Failed to allocate the merge command buffer")
            .into_iter()
            .nth(0)
            .expect("Failed to get merge cb");

        merge_command_buffer
            .begin_recording(true, false, false, None)
            .expect("Failed to start merge pass recording!");

        if self.in_debugger() {
            //merge_command_buffer.debug_marker_begin_region("Merging pass", [1.0, 1.0, 0.2, 1.0]);
            //merge_command_buffer.debug_marker_insert("Execute merging pass for ray-tracing and rasterizer pass", [1.0, 1.0, 0.1, 1.0]);
        }
        //Merge the two images. Execution is synced via two semaphores.
        self.merge_pass
            .execute(frame_info.index, merge_command_buffer.clone());

        //Finally handle the swapchain presentation, this is going to wait for the compute shader anyways.
        self.handle_swapchain_image(swapchain_image, merge_command_buffer.clone(), frame_info);

        //release depth image to compute queue for the next frame
        merge_command_buffer
            .cmd_pipeline_barrier(
                ash::vk::PipelineStageFlags::TRANSFER,
                ash::vk::PipelineStageFlags::ALL_COMMANDS,
                ash::vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![
                    //release image to compute queue.
                    self.primary_pass
                        .get_graphics_image(frame_info.index)
                        .new_image_barrier(
                            Some(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                            Some(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                            Some(self.graphics_queue.clone()),
                            Some(self.compute_queue.clone()),
                            None,
                            None,
                            None,
                        ),
                ],
            )
            .expect("Could not submit graphics queue acquire barrier");

        if self.in_debugger() {
            //merge_command_buffer.debug_marker_end_region();
        }
        //Finish last command buffer and execute.
        merge_command_buffer
            .end_recording()
            .expect("Failed to end present_buffer recording");

        let render_fence = self
            .graphics_queue
            .queue_submit(vec![device::queue::SubmitInfo::new(
                vec![
                    //Wait for the swapchain to be ready
                    (
                        frame_info.swapchain_ready.clone(),
                        vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                    ),
                    //wait for the raytracing pass to finish before we start
                    (
                        frame_info.raytracing_finished.clone(),
                        vk::PipelineStageFlags::COMPUTE_SHADER,
                    ),
                    //Wait for the debug/ui pass to finish
                    (
                        frame_info.debug_ui_finished.clone(),
                        vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                    ),
                ],
                vec![merge_command_buffer],
                //Signal when the swapchain image has recived the final image to be presented.
                vec![frame_info.present_ready.clone()],
            )])
            .expect("Failed to submit work to queue!");
        //--------ASSEMBLE DEBUG AND RAY-TRACING FINISHED--------
        render_fence
    }

    /// Creates a new command_buffer queue for the current state of the sub
    /// systems.
    /// Returns the collection of fences representing the execution of the variouse command buffers on compute
    /// and graphics queue.
    pub fn build_new_frame(
        &mut self,
        frame_info: &FrameInfo,
        swapchain_image: Arc<image::SwapchainImage>,
        buffer_manager: &mut BufferManager,
        camera: ShaderCamera,
        active_voxel_objects: Vec<(VoxelDataId, GpuVoxelObject)>,
        toplevel_bvh: Vec<GpuBvhNode>,
        active_triangle_objects: Vec<(MeshId, Transform)>,
    ) -> Vec<sync::QueueFence> {
        //The commands a recorded more or less in the order they should be executed. However, The UI/Debug pass are
        //parallel to the main ray tracing (except for the primary rays).

        //Before doing anything, check if we got a interface, and if so, if our render_area is up to date.
        match (self.interface.as_ref(), self.render_area) {
            (Some(interface), Some(render_area)) => {
                //Update interface

                let widget_extent = interface.target_widget.get_extent();
                if render_area.0 != widget_extent.0 || render_area.1 != widget_extent.1 {
                    println!("Render area missmatch!");

                    //Update render area
                    self.render_area = Some(widget_extent);

                    self.update_framebuffer(
                        self.device.clone(),
                        self.swapchain_extent,
                        self.swapchain_image_count,
                        buffer_manager,
                    );
                }
            }
            (Some(interface), None) => {
                println!("Got interface but no render area!");

                //Got interface but no render area, Add render area and resize
                let widget_extent = interface.target_widget.get_extent();

                self.render_area = Some(widget_extent);
                self.update_framebuffer(
                    self.device.clone(),
                    self.swapchain_extent,
                    self.swapchain_image_count,
                    buffer_manager,
                );
            }
            (None, Some(_render_area)) => {
                println!("Render area but no interface!");
                //No interface but render area, remove render area and resize everything to swapchain extent
                self.render_area = None;
                self.update_framebuffer(
                    self.device.clone(),
                    self.swapchain_extent,
                    self.swapchain_image_count,
                    buffer_manager,
                );
            }
            (None, None) => {
                //Got no interface and no render area, this is correct anyways
            }
        }
        //since all passes frame_buffer haven been updated if needed, execute primary ray pass.
        //This is always done since we otherwise would have no data at all.
        let primary_pass_finished = self.prepare_and_primary_rays(
            frame_info,
            buffer_manager,
            camera,
            active_voxel_objects,
            toplevel_bvh,
        );
        //Synchronize the debug pass with the primary ray execution, then render ui and debug triangles on the graphics queue in parallel to the next
        // ray-tracing passes.
        //When there is no UI and the client is running the engine this pass could be ignored. However, currently it is always executed.
        let debug_pass_finished =
            self.render_debug_and_ui(frame_info, buffer_manager, active_triangle_objects);
        //Now execute all the other raytracing passes.
        let rt_pass_finished = self.execute_ray_tracing(frame_info);

        //Now assemble the graphics_queue image and the computed raytracing image into the final one.
        let merge_and_present = self.merge_and_copy_to_swapchain(frame_info, swapchain_image);

        //Finally return the last command buffer and the semaphore it has to wait for before it can be submitted.
        vec![
            primary_pass_finished,
            debug_pass_finished,
            rt_pass_finished,
            merge_and_present,
        ]
    }

    ///Handles, based on the interface state, the copy/blit of of interface and
    /// final engine image onto the swapchain image
    fn handle_swapchain_image(
        &self,
        swapchain_image: Arc<image::SwapchainImage>,
        command_buffer: Arc<command_buffer::CommandBuffer>,
        frame_info: &FrameInfo,
    ) {
        // Find final image we want to blit either into the interface, or directly
        // onto the swapchain image.
        let final_image = self.merge_pass.get_finale_image(frame_info.index);

        //Wait for the merge pass to end, then transition the images into the transfer layouts, blit and transfer them back
        command_buffer
            .cmd_pipeline_barrier(
                ash::vk::PipelineStageFlags::COMPUTE_SHADER,
                ash::vk::PipelineStageFlags::ALL_COMMANDS,
                ash::vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![
                    swapchain_image.new_image_barrier(
                        Some(ash::vk::ImageLayout::PRESENT_SRC_KHR),
                        Some(ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL),
                        None,
                        None, //No queue transfer
                        None, //No old access mask
                        None, //We want to write to it only
                        None, //All of the image should be transfered
                    ),
                    final_image.new_image_barrier(
                        Some(vk::ImageLayout::GENERAL),
                        Some(vk::ImageLayout::TRANSFER_SRC_OPTIMAL),
                        None,
                        None,
                        Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE),
                        Some(vk::AccessFlags::TRANSFER_READ),
                        None,
                    ),
                ],
            )
            .expect("Failed to add pipeline barrier while copying");

        //Handle interface and final blit /copy to the swapchain
        if let Some(ref interface) = self.interface {
            //First copy the interface image to the swapchain, the
            // blit the engine image into the region
            let interface_image = interface.interface.get_image();
            //NOTE we do NOT have to transition the interface image, since this is done by the renderpass.

            //Now first blit the interface image to the swapchain
            let interface_extent = interface_image.extent_3d();
            let swapchain_extent = swapchain_image.extent_3d();

            command_buffer
                .cmd_blit_image(
                    interface_image.clone(),
                    vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                    swapchain_image.clone(),
                    vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                    vec![vk::ImageBlit::builder()
                        .src_subresource(vk::ImageSubresourceLayers {
                            aspect_mask: vk::ImageAspectFlags::COLOR,
                            mip_level: 0,
                            base_array_layer: 0,
                            layer_count: 1,
                        })
                        .src_offsets(
                            //We blit the whole image, therefor from 0/0 till extent
                            [
                                vk::Offset3D { x: 0, y: 0, z: 0 },
                                vk::Offset3D {
                                    x: interface_extent.width as i32,
                                    y: interface_extent.height as i32,
                                    z: interface_extent.depth as i32,
                                },
                            ],
                        )
                        .dst_subresource(vk::ImageSubresourceLayers {
                            aspect_mask: vk::ImageAspectFlags::COLOR,
                            mip_level: 0,
                            base_array_layer: 0,
                            layer_count: 1,
                        })
                        .dst_offsets(
                            //We blit the whole image, therefor from 0/0 till extent
                            [
                                vk::Offset3D { x: 0, y: 0, z: 0 },
                                vk::Offset3D {
                                    x: swapchain_extent.width as i32,
                                    y: swapchain_extent.height as i32,
                                    z: 1 as i32,
                                },
                            ],
                        )
                        .build()],
                    vk::Filter::LINEAR, //Simple linear filtering.
                )
                .expect("Could not blit interface image to swapchain!");

            //Now do the same to the engine picture, but this time use the interface widgets
            //location as target.
            let final_extent = final_image.extent_3d();
            //build the blit operation destination in the widget, only fill in the source
            let mut target_blit = interface
                .target_widget
                .get_framebuffer_area(interface.interface.clone());
            target_blit.src_subresource = vk::ImageSubresourceLayers {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                mip_level: 0,
                base_array_layer: 0,
                layer_count: 1,
            };

            target_blit.src_offsets = [
                vk::Offset3D { x: 0, y: 0, z: 0 },
                vk::Offset3D {
                    x: final_extent.width as i32,
                    y: final_extent.height as i32,
                    z: 1 as i32,
                },
            ];

            //Only actually blit if we dont exeed our region
            if !(target_blit.dst_offsets[0].y > swapchain_extent.height as i32
                || target_blit.dst_offsets[0].x > swapchain_extent.width as i32
                || target_blit.dst_offsets[1].y > swapchain_extent.height as i32
                || target_blit.dst_offsets[1].x > swapchain_extent.width as i32)
            {
                command_buffer
                    .cmd_blit_image(
                        final_image.clone(),
                        vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                        swapchain_image.clone(),
                        vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                        vec![target_blit],
                        vk::Filter::LINEAR, //Simple linear filtering.
                    )
                    .expect("Could not blit final engine image onto interface");
            }

            //transition the interface image back into the color attachment layout
            command_buffer
                .cmd_pipeline_barrier(
                    ash::vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                    ash::vk::PipelineStageFlags::TOP_OF_PIPE,
                    ash::vk::DependencyFlags::empty(),
                    vec![],
                    vec![],
                    vec![interface_image.clone().new_image_barrier(
                        Some(ash::vk::ImageLayout::TRANSFER_SRC_OPTIMAL),
                        Some(ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL),
                        None,
                        None, //No queue transfer
                        None, //No old access mask
                        None, //We want to write to it only
                        None, //All of the image should be transfered
                    )],
                )
                .expect("Failed to add pipeline barrier while copying");
        } else {
            //Just blit the engine image to the swapchain.
            //We use blit anyways since we might have a virtual difference between our
            //framebuffer size and the actual swapchain image size.

            let final_extent = final_image.extent_3d();
            let swapchain_extent = swapchain_image.extent_3d();

            command_buffer
                .cmd_blit_image(
                    final_image.clone(),
                    vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                    swapchain_image.clone(),
                    vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                    vec![vk::ImageBlit::builder()
                        .src_subresource(vk::ImageSubresourceLayers {
                            aspect_mask: vk::ImageAspectFlags::COLOR,
                            mip_level: 0,
                            base_array_layer: 0,
                            layer_count: 1,
                        })
                        .src_offsets(
                            //We blit the whole image, therefor from 0/0 till extent
                            [
                                vk::Offset3D { x: 0, y: 0, z: 0 },
                                vk::Offset3D {
                                    x: final_extent.width as i32,
                                    y: final_extent.height as i32,
                                    z: 1 as i32,
                                },
                            ],
                        )
                        .dst_subresource(vk::ImageSubresourceLayers {
                            aspect_mask: vk::ImageAspectFlags::COLOR,
                            mip_level: 0,
                            base_array_layer: 0,
                            layer_count: 1,
                        })
                        .dst_offsets(
                            //We blit the whole image, therefor from 0/0 till extent
                            [
                                vk::Offset3D { x: 0, y: 0, z: 0 },
                                vk::Offset3D {
                                    x: swapchain_extent.width as i32,
                                    y: swapchain_extent.height as i32,
                                    z: 1 as i32,
                                },
                            ],
                        )
                        .build()],
                    vk::Filter::LINEAR, //Simple linear filtering.
                )
                .expect("Could not simple-blit engine image to swapchain!");
        }

        //Transition bot images back to normal
        command_buffer
            .cmd_pipeline_barrier(
                ash::vk::PipelineStageFlags::ALL_COMMANDS,
                ash::vk::PipelineStageFlags::ALL_COMMANDS,
                ash::vk::DependencyFlags::empty(),
                vec![],
                vec![],
                vec![
                    swapchain_image.clone().new_image_barrier(
                        Some(ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL),
                        Some(ash::vk::ImageLayout::PRESENT_SRC_KHR),
                        None,
                        None, //No queue transfer
                        None, //No old access mask
                        None, //We want to write to it only
                        None, //All of the image should be transfered
                    ),
                    final_image.new_image_barrier(
                        Some(vk::ImageLayout::TRANSFER_SRC_OPTIMAL),
                        Some(vk::ImageLayout::GENERAL),
                        None,
                        None,
                        Some(vk::AccessFlags::TRANSFER_READ),
                        Some(vk::AccessFlags::SHADER_READ | vk::AccessFlags::SHADER_WRITE),
                        None,
                    ),
                ],
            )
            .expect("Failed to add pipeline barrier while copying");
    }

    ///Updates the framebuffer extent of all sub system. This could potentially take a while
    pub fn update_framebuffer(
        &mut self,
        device: Arc<device::Device>,
        new_extent: (u32, u32),
        swapchain_image_count: usize,
        buffer_manager: &BufferManager,
    ) {
        self.swapchain_extent = new_extent;
        self.swapchain_image_count = swapchain_image_count;

        let mut render_area_extent = if let Some(area) = self.render_area {
            area
        } else {
            self.swapchain_extent
        };

        if render_area_extent.0 < 1 {
            render_area_extent.0 = 1;
        }

        if render_area_extent.1 < 1 {
            render_area_extent.1 = 1;
        }
        println!("Render area: {:?}", render_area_extent);

        self.primary_pass = PrimaryPass::new(
            device.clone(),
            self.graphics_queue.clone(),
            self.compute_queue.clone(),
            swapchain_image_count,
            render_area_extent,
            buffer_manager,
        );

        self.radiance_pass = RadiancePass::new(
            device.clone(),
            self.graphics_queue.clone(),
            self.compute_queue.clone(),
            swapchain_image_count,
            render_area_extent,
            self.shared_resources.get_bluenoise(),
            self.primary_pass.get_gbuffer(),
            buffer_manager,
        );

        let mut denoiser_images = Vec::with_capacity(swapchain_image_count);
        for i in 0..swapchain_image_count {
            denoiser_images.push((
                self.primary_pass.get_normal_depth_image(i),
                self.radiance_pass.get_final_image(i),
                self.primary_pass.get_albedo_roughness_image(i),
            ));
        }

        self.radiance_denoiser = RadianceDenoiser::new(
            device.clone(),
            self.graphics_queue.clone(),
            self.compute_queue.clone(),
            swapchain_image_count,
            render_area_extent,
            denoiser_images,
        );

        self.specular_pass = SpecularPass::new(
            device.clone(),
            self.graphics_queue.clone(),
            self.compute_queue.clone(),
            swapchain_image_count,
            render_area_extent,
            self.primary_pass.get_gbuffer(),
            buffer_manager,
        );

        self.debug_pass = DebugPass::new(
            device.clone(),
            swapchain_image_count,
            render_area_extent,
            self.graphics_queue.clone(),
            buffer_manager,
        );

        let mut primary_assemble_images = Vec::with_capacity(swapchain_image_count);
        for i in 0..swapchain_image_count {
            primary_assemble_images.push((
                self.specular_pass.get_final_image(i),
                self.radiance_pass.get_final_image(i),
            ));
        }

        self.primary_assemble = AssemblePass::new(
            device.clone(),
            self.graphics_queue.clone(),
            self.compute_queue.clone(),
            swapchain_image_count,
            render_area_extent,
            self.primary_pass.get_gbuffer(),
            primary_assemble_images,
            buffer_manager,
        );

        let mut merge_images = Vec::with_capacity(swapchain_image_count);
        for i in 0..swapchain_image_count {
            merge_images.push((
                self.debug_pass.get_color_image(i),
                self.primary_assemble
                    .get_data(i)
                    .expect("Could not get primary passes final image!")
                    .final_image
                    .clone(),
            ));
        }

        self.merge_pass = MergePass::new(
            device.clone(),
            self.graphics_queue.clone(),
            self.compute_queue.clone(),
            swapchain_image_count,
            render_area_extent,
            merge_images,
            buffer_manager,
        );
    }

    pub fn set_interface(&mut self, interface: InterfaceInfo) {
        self.interface = Some(interface);
    }

    fn in_debugger(&self) -> bool {
        self.config
            .read()
            .expect("Could not read debugger state from config")
            .in_debugger
    }

    ///Returns the image ratio between width and hight for the current render area.
    pub fn get_render_image_ratio(&self) -> f32 {
        let render_area_extent = if let Some(area) = self.render_area {
            area
        } else {
            self.swapchain_extent
        };

        render_area_extent.0 as f32 / render_area_extent.1 as f32
    }
}
