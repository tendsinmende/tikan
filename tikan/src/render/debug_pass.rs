/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::render::buffer_manager::BufferManager;
use marp::ash::vk;
use marp::buffer::*;
use marp::descriptor::*;
use marp::image::*;
use marp::memory::MemoryUsage;
use marp::pipeline::*;
use marp::shader::*;
use marp::*;
use std::sync::Arc;

use crate::data::triangle_object::*;
use crate::render::FrameInfo;
use tikan_shader::get_shader_path;

///Defines the per object data that can be uploaded
struct DebugPushConstant {
    ///Object transform
    transform: [[f32; 4]; 4],
    color: [f32; 4],
}

///Defines how the polys are drawn and in which color
#[derive(Clone)]
pub enum DebugDrawType {
    ///Draws filled triangles with the given color. Indices should be tribles, since each triangle has 3 vertices
    Filled([f32; 4]),
    ///Draws liens with the given color. Indices should be pairs, for start and end of each line.
    Lines([f32; 4]),
}

pub struct DebugData {
    color_image: Arc<image::Image>,
    depth_image: Arc<image::Image>,
    ///The frame buffer based on the color and depth image
    framebuffer: Arc<framebuffer::Framebuffer>,

    descriptor_set: Arc<DescriptorSet>,
}

///A small, triangle based debug pass that blends models into the ray traced final image.
pub struct DebugPass {
    render_pass: Arc<framebuffer::RenderPass>,

    data: Vec<DebugData>,
    depth_format: vk::Format,
    color_format: vk::Format,

    ///Used when a filled object is draw
    pipeline_filled: Arc<dyn pipeline::Pipeline + Send + Sync>,
    ///Used when a lines object is drawn
    pipeline_lines: Arc<dyn pipeline::Pipeline + Send + Sync>,

    descriptor_pool: Arc<StdDescriptorPool>,

    ///The extent this debug pass was created for
    extent: (u32, u32),
}

impl DebugPass {
    pub fn new(
        device: Arc<device::Device>,
        swapchain_image_count: usize,
        swapchain_extent: (u32, u32),
        _graphics_queue: Arc<device::Queue>,
        buffer_manager: &BufferManager,
    ) -> Arc<Self> {
        let depth_format = device
            .get_useable_format(
                vec![
                    vk::Format::D16_UNORM_S8_UINT,
                    vk::Format::D24_UNORM_S8_UINT,
                    vk::Format::D32_SFLOAT_S8_UINT,
                ],
                &ImageUsage {
                    depth_attachment: true,
                    input_attachment: true,
                    ..Default::default()
                },
                vk::ImageTiling::OPTIMAL,
            )
            .expect("Could not find a depth format");

        let depth_image_info = ImageInfo::new(
            ImageType::Image2D {
                width: swapchain_extent.0,
                height: swapchain_extent.1,
                samples: 1,
            },
            depth_format,
            None,
            Some(MipLevel::Specific(1)),
            ImageUsage {
                depth_attachment: true,
                input_attachment: true,
                transfer_dst: true,
                ..Default::default()
            },
            MemoryUsage::GpuOnly,
            None,
        );
        // TODO transition to DepthStencilAttachment_read/write access mask, and DepthStencilOptimal Layout

        let color_format = device
            .get_useable_format(
                vec![
                    //vk::Format::R16G16B16A16_SFLOAT, //16bit float should work everywhere
                    //vk::Format::B8G8R8A8_UNORM,
                    vk::Format::R8G8B8A8_SNORM, //for intel
                ],
                &ImageUsage {
                    color_attachment: true,
                    input_attachment: true,
                    transfer_src: true,
                    storage: true,
                    ..Default::default()
                },
                vk::ImageTiling::OPTIMAL,
            )
            .expect("Could not find a color format for debug pass");

        let color_image_info = ImageInfo::new(
            ImageType::Image2D {
                width: swapchain_extent.0,
                height: swapchain_extent.1,
                samples: 1,
            },
            color_format,
            None,
            Some(MipLevel::Specific(1)),
            ImageUsage {
                color_attachment: true,
                input_attachment: true,
                transfer_src: true,
                storage: true,
                ..Default::default()
            },
            MemoryUsage::GpuOnly,
            None,
        );

        //TODO transition all images into the correct layout and aspect mask.

        //Setup the renderpass we want to use while rendering the triangle
        let attachment_descs = vec![
            //The color attachment
            marp::framebuffer::AttachmentDescription::new(
                vk::SampleCountFlags::TYPE_1,
                color_format,
                vk::AttachmentStoreOp::STORE,
                vk::AttachmentLoadOp::CLEAR,
                None,                     // no stencil store
                None,                     // no stencil load
                None,                     //No initial layout needed since we clear anyways
                vk::ImageLayout::GENERAL, //After the renderpass, transition to TRANSFER_OPTIMAL
            ),
            //The depth image
            marp::framebuffer::AttachmentDescription::new(
                vk::SampleCountFlags::TYPE_1,
                depth_format,
                vk::AttachmentStoreOp::STORE,
                vk::AttachmentLoadOp::CLEAR,
                None,                     // no stencil store
                None,                     // no stencil load
                None,                     //No initial layout needed since we clear anyways
                vk::ImageLayout::GENERAL, //After the renderpass, transition to TRANSFER_OPTIMAL
            ),
        ];

        //This are the subpasses this renderpass goes through. However, this is easy in a triangle example.
        let render_pass = marp::framebuffer::RenderPass::new(attachment_descs)
            .add_subpass(vec![
                marp::framebuffer::AttachmentRole::OutputColor,
                marp::framebuffer::AttachmentRole::DepthStencil,
            ])
            .add_dependency(marp::framebuffer::SubpassDependency::new(
                vk::SUBPASS_EXTERNAL,                            //from external pass
                0,                                               //To first and only subpass
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT, //When color output has finished on src
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT, //Color output can start on this one
                vk::AccessFlags::empty(),                        //Don't know access type of source
                vk::AccessFlags::COLOR_ATTACHMENT_READ | vk::AccessFlags::COLOR_ATTACHMENT_WRITE, //We need color read and write support
            ))
            .add_dependency(marp::framebuffer::SubpassDependency::new(
                0,
                vk::SUBPASS_EXTERNAL,
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                vk::AccessFlags::COLOR_ATTACHMENT_READ | vk::AccessFlags::COLOR_ATTACHMENT_WRITE, //Don't know access type of source
                vk::AccessFlags::empty(),
            ))
            .build(device.clone())
            .expect("Failed to build renderpass!");

        //Generates our camera uniform buffer attachment
        let descriptor_pool = descriptor::StdDescriptorPool::new(
            device.clone(),
            vec![
                //One camera buffer per frame
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::UNIFORM_BUFFER)
                    .descriptor_count(swapchain_image_count as u32 * 1)
                    .build(),
            ]
            .as_slice(),
            swapchain_image_count as u32, //one set per frame
        )
        .expect("Failed to create descriptor_pool_size!");

        let mut data = Vec::with_capacity(swapchain_image_count);

        for idx in 0..swapchain_image_count {
            let color_image = marp::image::Image::new(
                device.clone(),
                color_image_info.clone(),
                buffer::SharingMode::Exclusive,
            )
            .expect("Failed to create color image!");

            let depth_image = marp::image::Image::new(
                device.clone(),
                depth_image_info.clone(),
                SharingMode::Exclusive,
            )
            .expect("Failed to create depth image!");

            let framebuffer = marp::framebuffer::Framebuffer::new(
                device.clone(),
                render_pass.clone(),
                vec![color_image.clone(), depth_image.clone()],
            )
            .expect("Failed to create frame buffer");

            let mut descriptor_set_builder = descriptor_pool.next();

            descriptor_set_builder
                .add(DescResource::new_buffer(
                    0,
                    vec![buffer_manager.get_graphics_camera_data(idx)],
                    vk::DescriptorType::UNIFORM_BUFFER,
                ))
                .expect("Failed to add descriptor to builder");

            let descriptor_set = descriptor_set_builder
                .build()
                .expect("Failed to build debug pass descriptor set builder");

            data.push(DebugData {
                framebuffer,
                depth_image,
                color_image,
                descriptor_set,
            });
        }
        //Now load the pipelines

        let vertex_shader =
            ShaderModule::new_from_spv(device.clone(), &get_shader_path("debug_vert.spv"))
                .expect("Failed to load vertex shader for debug pass");

        let fragment_shader =
            ShaderModule::new_from_spv(device.clone(), &get_shader_path("debug_frag.spv"))
                .expect("Failed to load fragment shader for debug pass");

        let vertex_stage = vertex_shader.to_stage(Stage::Vertex, "main");
        let fragment_stage = fragment_shader.to_stage(Stage::Fragment, "main");

        let vertex_state = get_default_vertex_state();

        let input_assembly_state_fill = InputAssemblyState {
            topology: vk::PrimitiveTopology::TRIANGLE_LIST,
            restart_enabled: false,
        };

        let input_assembly_state_lines = InputAssemblyState {
            topology: vk::PrimitiveTopology::LINE_LIST,
            restart_enabled: false,
        };

        let raster_info_fill = RasterizationState {
            depth_clamp_enabled: false,
            rasterizer_discard_enabled: false,
            polygone_mode: vk::PolygonMode::FILL,
            cull_mode: vk::CullModeFlags::NONE, //No culling for now
            front_face: vk::FrontFace::COUNTER_CLOCKWISE,
            line_width: None,
            depth_bias: None, //No depth bias for now
        };

        let multisample_state = MultisampleState {
            sample_count: vk::SampleCountFlags::TYPE_1,
            sample_shading: false,
            min_sample_shading: 1.0,
            alpha_to_coverage: false,
            alpha_to_one: false,
        };

        let raster_info_lines = RasterizationState {
            depth_clamp_enabled: false,
            rasterizer_discard_enabled: false,
            polygone_mode: vk::PolygonMode::LINE,
            cull_mode: vk::CullModeFlags::NONE, //No culling for now
            front_face: vk::FrontFace::COUNTER_CLOCKWISE,
            line_width: Some(2.0),
            depth_bias: None, //No depth bias for now
        };

        let color_blend_state = ColorBlendState::new(
            Some([0.0, 0.0, 0.0, 0.0]),
            Some(vk::LogicOp::COPY),
            vec![
                //Our Color attachment, we don't blend, but discard in the shader if the alpha value is below 1.0
                ColorBlendAttachmentState::NoBlending,
            ],
        );

        let depth_stencil_state = DepthStencilState {
            depth_test_enabled: true,
            depth_write_enabled: true,
            depth_compare_op: vk::CompareOp::LESS,
            stencil_test: StencilTestState::NoTest,
            depth_bounds: None,
        };

        let pipeline_state_fill = PipelineState::new(
            vertex_state.clone(),
            input_assembly_state_fill,
            None,
            ViewportState::new(ViewportMode::Static(vec![(
                Viewport::new(
                    [0.0, 0.0],
                    swapchain_extent.0 as f32,
                    swapchain_extent.1 as f32,
                    0.0,
                    1.0,
                ),
                Scissor {
                    offset: [0, 0],
                    extent: vk::Extent2D::builder()
                        .width(swapchain_extent.0)
                        .height(swapchain_extent.1)
                        .build(),
                },
            )])), //We have no viewport state since we set the viewport at runtime
            raster_info_fill,
            multisample_state,
            depth_stencil_state,
            Some(color_blend_state.clone()),
        );

        let pipeline_state_lines = PipelineState::new(
            vertex_state,
            input_assembly_state_lines,
            None,
            ViewportState::new(ViewportMode::Static(vec![(
                Viewport::new(
                    [0.0, 0.0],
                    swapchain_extent.0 as f32,
                    swapchain_extent.1 as f32,
                    0.0,
                    1.0,
                ),
                Scissor {
                    offset: [0, 0],
                    extent: vk::Extent2D::builder()
                        .width(swapchain_extent.0)
                        .height(swapchain_extent.1)
                        .build(),
                },
            )])), //We have no viewport state since we set the viewport at runtime
            raster_info_lines,
            multisample_state,
            depth_stencil_state,
            Some(color_blend_state),
        );

        //Fake the first push constant to create a pipeline layout
        let fake_push_constant = PushConstant::new(
            DebugPushConstant {
                transform: [[0.0; 4]; 4],
                color: [1.0; 4],
            },
            vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT,
        );

        let pipeline_layout = PipelineLayout::new(
            device.clone(),
            vec![*data[0].descriptor_set.layout()],
            vec![*fake_push_constant.range()],
        )
        .expect("Failed to setup debug pipeline layout!");

        let pipeline_filled = GraphicsPipeline::new(
            device.clone(),
            vec![vertex_stage.clone(), fragment_stage.clone()],
            pipeline_state_fill,
            pipeline_layout.clone(),
            render_pass.clone(),
            0,
        )
        .expect("Failed to create debug fill pipeline!");

        let pipeline_lines = GraphicsPipeline::new(
            device.clone(),
            vec![vertex_stage.clone(), fragment_stage.clone()],
            pipeline_state_lines,
            pipeline_layout.clone(),
            render_pass.clone(),
            0,
        )
        .expect("Failed to create debug fill pipeline!");

        Arc::new(DebugPass {
            render_pass,
            data,
            depth_format,
            color_format,

            pipeline_filled,
            pipeline_lines,

            descriptor_pool,
            extent: swapchain_extent,
        })
    }
    ///Adds all calls to this command buffer. Assumes that it is recording but not within
    /// a renderpass.
    pub fn execute_pass(
        &self,
        frame_info: &FrameInfo,
        command_buffer: Arc<command_buffer::CommandBuffer>,
        active_meshes: (Vec<MeshDrawCall>, Vec<MeshDrawCall>),
    ) {
        //TODO copy depth from a channel of the primary pass to the currently used depth image.

        let clear_values = vec![
            vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [0.0, 0.0, 0.0, 0.0], //Clear to nothing since this will be copied over the raytracing image
                },
            },
            vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0,
                },
            },
        ];

        let rendering_rect = vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: vk::Extent2D {
                width: self.extent.0,
                height: self.extent.1,
            },
        };

        command_buffer
            .cmd_begin_render_pass(
                self.render_pass.clone(),
                self.data[frame_info.index].framebuffer.clone(),
                rendering_rect,
                clear_values,
                vk::SubpassContents::INLINE,
            )
            .expect("Failed to start renderpass!");

        //Since widkan uses dynamic scissor/viewport, we set them as well for us to the whole extent

        //Now, for each vertex based object that is active, draw.
        //Get the mesh ids we wanna draw
        let (triangle_meshes, line_meshes) = active_meshes;

        //Bind camera desc set
        command_buffer
            .cmd_bind_descriptor_sets(
                vk::PipelineBindPoint::GRAPHICS,
                self.pipeline_filled.layout(),
                0,
                vec![self.data[frame_info.index].descriptor_set.clone()],
                vec![],
            )
            .expect("Failed to bind camera descriptor set");

        command_buffer
            .cmd_bind_pipeline(
                vk::PipelineBindPoint::GRAPHICS,
                self.pipeline_filled.clone(),
            )
            .expect("Failed to bind filled pipeline");

        //Now draw the filled meshes
        for filled_mesh in triangle_meshes {
            command_buffer
                .cmd_bind_vertex_buffers(0, vec![(filled_mesh.vertex_buffer, 0)])
                .expect("Failed to bind vertex buffer!");

            command_buffer
                .cmd_bind_index_buffer(filled_mesh.index_buffer, 0, vk::IndexType::UINT32)
                .expect("Failed to bind index buffer!");

            //Push constants for that mesh
            command_buffer
                .cmd_push_constants(
                    self.pipeline_filled.layout(),
                    &PushConstant::new(
                        DebugPushConstant {
                            transform: filled_mesh.transform,
                            color: filled_mesh.color,
                        },
                        vk::ShaderStageFlags::FRAGMENT | vk::ShaderStageFlags::VERTEX,
                    ),
                )
                .expect("Failed to push constants");

            command_buffer
                .cmd_draw_indexed(filled_mesh.index_count, 1, 0, 0, 0)
                .expect("Failed to draw filled mesh")
        }
        //And draw the lined meshes
        command_buffer
            .cmd_bind_pipeline(vk::PipelineBindPoint::GRAPHICS, self.pipeline_lines.clone())
            .expect("Failed to bind filled pipeline");

        for line_mesh in line_meshes {
            command_buffer
                .cmd_bind_vertex_buffers(0, vec![(line_mesh.vertex_buffer, 0)])
                .expect("Failed to bind vertex buffer!");

            command_buffer
                .cmd_bind_index_buffer(line_mesh.index_buffer, 0, vk::IndexType::UINT32)
                .expect("Failed to bind index buffer!");

            command_buffer
                .cmd_push_constants(
                    self.pipeline_lines.layout(),
                    &PushConstant::new(
                        DebugPushConstant {
                            transform: line_mesh.transform,
                            color: line_mesh.color,
                        },
                        vk::ShaderStageFlags::FRAGMENT | vk::ShaderStageFlags::VERTEX,
                    ),
                )
                .expect("Failed to push constants");

            command_buffer
                .cmd_draw_indexed(line_mesh.index_count, 1, 0, 0, 0)
                .expect("Failed to draw filled mesh")
        }
        command_buffer
            .cmd_end_render_pass()
            .expect("Could not end debug renderpass!");
    }

    pub fn get_color_image(&self, index: usize) -> Arc<image::Image> {
        self.data[index].color_image.clone()
    }
}
