/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use na;

///Describes the transformation of an component.
#[derive(Clone, Copy, Debug)]
pub struct Transform {
    trans_rot: na::Isometry3<f32>,
    //Since we use non uniform scaling but the api for affine transformation is strange, we add it in time
    //to the matrix4
    scaling: na::Vector3<f32>,
}

impl Transform {
    //Creates a transform that does nothing to the entity
    pub fn identity() -> Self {
        Transform {
            trans_rot: na::Isometry3::identity(),
            scaling: na::Vector3::new(1.0, 1.0, 1.0),
        }
    }

    ///Adds the given rotation. The easiest way is to use `na::UnitQuaternion::from_euler_angles()`
    pub fn add_rotation(&mut self, rotation: &na::UnitQuaternion<f32>) {
        self.trans_rot.append_rotation_mut(rotation);
    }

    pub fn rotation(&self) -> &na::UnitQuaternion<f32> {
        &self.trans_rot.rotation
    }

    pub fn rotation_mut(&mut self) -> &mut na::UnitQuaternion<f32> {
        &mut self.trans_rot.rotation
    }

    pub fn add_translation(&mut self, translation: na::Vector3<f32>) {
        self.trans_rot
            .append_translation_mut(&na::Translation3::from_vector(translation));
    }

    pub fn translation(&self) -> &na::Translation3<f32> {
        &self.trans_rot.translation
    }

    pub fn translation_mut(&mut self) -> &mut na::Translation3<f32> {
        &mut self.trans_rot.translation
    }

    pub fn add_scaling(&mut self, scaling: na::Vector3<f32>) {
        self.scaling += scaling;
    }

    ///returns the scaling on each axis in world space (before rotation and translation)
    pub fn scaling(&self) -> &na::Vector3<f32> {
        &self.scaling
    }

    pub fn scaling_mut(&mut self) -> &mut na::Vector3<f32> {
        &mut self.scaling
    }

    pub fn to_matrix4(&self) -> na::Matrix4<f32> {
        na::convert::<na::Isometry3<f32>, na::Matrix4<f32>>(self.trans_rot)
            * na::Matrix4::new_nonuniform_scaling(&self.scaling)
    }
}
