use crate::data::voxel_object::{GpuVoxel, VoxelObject};

///Indentifies CPU side voxel data within this manager.
pub type VoxelDataId = usize;

pub struct VoxelDataManager {
    ///Maps some VoxelObject to an id which can be used to get the data.
    voxel_data: Vec<VoxelObject>,
    free_ids: Vec<VoxelDataId>,
}

impl VoxelDataManager {
    pub fn new() -> Self {
        VoxelDataManager {
            voxel_data: Vec::new(),
            free_ids: Vec::new(),
        }
    }

    pub fn insert(&mut self, data: VoxelObject) -> VoxelDataId {
        if let Some(overwrite_id) = self.free_ids.pop() {
            self.voxel_data[overwrite_id] = data;
            return overwrite_id;
        } else {
            let id = self.voxel_data.len();
            self.voxel_data.push(data);
            return id;
        }
    }

    pub fn remove(&mut self, id: VoxelDataId) {
        self.free_ids.push(id);
        //Exchange data with an empty voxel object to remove data from ram.
        self.voxel_data[id] = VoxelObject::empty();
    }

    pub fn get(&self, id: &VoxelDataId) -> Option<&VoxelObject> {
        self.voxel_data.get(*id)
    }

    pub fn get_mut(&mut self, id: &VoxelDataId) -> Option<&mut VoxelObject> {
        self.voxel_data.get_mut(*id)
    }

    fn is_id_used(&self, id: &VoxelDataId) -> bool {
        if *id >= self.voxel_data.len() {
            return false;
        }

        for sid in self.free_ids.iter() {
            if *sid == *id {
                return false;
            }
        }

        true
    }

    pub fn get_all_changed(&mut self) -> Vec<(VoxelDataId, Vec<GpuVoxel>)> {
        let mut changed_objects = Vec::new();
        for (index, data) in self.voxel_data.iter_mut().enumerate() {
            if data.has_changed() {
                //Push into vector of changed things
                changed_objects.push((index, data.to_gpu_voxel_object()));
                //Now mark as unchanged
                data.reset_has_changed_status();
            }
        }

        changed_objects
    }

    //TODO when there are at some point RAM problems, implement streaming
}
