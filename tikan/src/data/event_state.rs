/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::collections::HashMap;
use widkan::msw::winit::event::{ElementState, Event, MouseButton, VirtualKeyCode, WindowEvent};
///Tracks the state of each key on the keyboard. Should
///be used when you don't want to react on a event but want to do stuff "while key is pressed"
pub struct KeyStates {
    key_states: HashMap<VirtualKeyCode, bool>,
    mouse_state: HashMap<MouseButton, bool>,
}

impl KeyStates {
    pub fn new() -> KeyStates {
        KeyStates {
            key_states: HashMap::new(),
            mouse_state: HashMap::new(),
        }
    }

    ///Returns true if `key` is currently pressed.
    pub fn is_pressed(&self, key: VirtualKeyCode) -> bool {
        if let Some(state) = self.key_states.get(&key) {
            *state
        } else {
            false
        }
    }

    pub fn is_pressed_mouse(&self, key: MouseButton) -> bool {
        if let Some(state) = self.mouse_state.get(&key) {
            *state
        } else {
            false
        }
    }

    pub fn update(&mut self, events: &[Event<()>]) {
        for e in events.iter() {
            match e {
                Event::WindowEvent {
                    event:
                        WindowEvent::KeyboardInput {
                            device_id: _,
                            input,
                            is_synthetic: _,
                        },
                    window_id: _,
                } => {
                    if let Some(k) = input.virtual_keycode {
                        //Add the key state of the key that was pressed, update if it already exists.
                        let new_key_state = if input.state == ElementState::Pressed {
                            true
                        } else {
                            false
                        };

                        if let Some(val) = self.key_states.get_mut(&k) {
                            *val = new_key_state;
                        } else {
                            self.key_states.insert(k, new_key_state);
                        }
                    }
                }
                Event::WindowEvent {
                    event:
                        WindowEvent::MouseInput {
                            device_id: _,
                            state,
                            button,
                            modifiers,
                        },
                    window_id: _,
                } => {
                    let state = if state == &ElementState::Pressed {
                        true
                    } else {
                        false
                    };
                    if let Some(v) = self.mouse_state.get_mut(&button) {
                        *v = state;
                    } else {
                        self.mouse_state.insert(*button, state);
                    }
                }
                _ => {}
            }
        }
    }
}
