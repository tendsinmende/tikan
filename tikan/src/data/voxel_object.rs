/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::collections::HashMap;
use std::convert::TryInto;

use voxer::{
    octree::{FlattenTree, OctNode, ToFlatVoxel},
    Accelerator, Object, Tooling, VoxelBuffer, SVO,
};

///A special voxel decleration for use with the gpu
#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct GpuVoxel {
    ///Contains the voxel location within its volume as well as the level on the fourth element.
    pub location: [u32; 4],
    ///Contains the emissive properties of this voxel. Alpha component is currently unused.
    pub emission: [f32; 4],

    ///Contains the skipping index to which the raytracer can skip,
    /// if this voxel does not intersect the ray. If it is `u32::MAX` it means that the raytraced can end.
    pub skip_idx: u32,
    ///Albedo and alpha stuffed into 4*8bit u32
    pub albedo_alpha: u32,
    ///8bit roughness, 8bit metall and 8bit is_leaf flag as well as 8 unsued bit, stuffed into 32bit
    pub roughness_metal_is_leaf_none: u32,
    ///Normals compressed into the first 3*8bit
    pub normal_none: u32,
}

///The Voxel declaration
#[derive(Clone)]
pub struct Voxel {
    pub albedo_alpha: [f32; 4],
    pub emission: [f32; 3],
    pub normal: [f32; 3],
    pub roughness: f32,
    pub metallness: f32,
    index: u32,
}

impl ToFlatVoxel<Voxel, GpuVoxel> for Voxel {
    fn flatten(&self, skip_index: usize, level: usize, node: &OctNode<Voxel>) -> GpuVoxel {
        self.to_gpu_voxel(
            skip_index.try_into().unwrap_or(std::u32::MAX),
            level.try_into().unwrap_or(std::u32::MAX),
            node.is_leaf(),
            node.get_min_max().0,
        )
    }
}

impl Voxel {
    ///Copys the everything apart from the children array into a new voxel.
    pub fn copy_to_new(&self) -> Self {
        Voxel {
            albedo_alpha: self.albedo_alpha,
            emission: self.emission,
            normal: self.normal,
            roughness: self.roughness,
            metallness: self.metallness,
            index: 0,
        }
    }

    pub fn albedo_alpha(&self) -> [f32; 4] {
        self.albedo_alpha
    }

    pub fn emission(&self) -> [f32; 3] {
        self.emission
    }

    pub fn normal(&self) -> [f32; 3] {
        self.normal
    }

    pub fn roughness(&self) -> f32 {
        self.roughness
    }

    pub fn metallness(&self) -> f32 {
        self.metallness
    }

    pub fn new_leaf(
        albedo_alpha: [f32; 4],
        emission: [f32; 3],
        normal: [f32; 3],
        roughness: f32,
        metallness: f32,
    ) -> Self {
        Voxel {
            albedo_alpha,
            emission,
            normal,
            roughness,
            metallness,
            index: 0,
        }
    }

    ///Checks if self is similar to other based on the given max difference.
    ///Albedo, emission, roughness, metallness are checked. If the difference is not too big, true is returned
    pub fn is_similar(&self, other: &Self, max_difference: f32) -> bool {
        for i in 0..4 {
            if (self.albedo_alpha[i] - other.albedo_alpha[0]).abs() > max_difference {
                return false;
            }
        }

        for i in 0..3 {
            if (self.emission[i] - other.emission[0]).abs() > max_difference {
                return false;
            }
        }

        if (self.roughness - other.roughness).abs() > max_difference {
            return false;
        }

        if (self.metallness - other.metallness).abs() > max_difference {
            return false;
        }
        //Came till here, therefore similarity is given
        true
    }

    ///Blends this voxel with another one based on alpha. The resulting voxel is alpha * self + (1-alpha) * other
    pub fn blend(&self, other: &Voxel, mut alpha: f32) -> Voxel {
        alpha = alpha.max(0.0).min(1.0);

        let mut new_voxel = Voxel::new_leaf([0.0; 4], [0.0; 3], [0.0; 3], 0.0, 1.0);

        new_voxel.albedo_alpha = [
            self.albedo_alpha[0] * alpha + (1.0 - alpha) * other.albedo_alpha[0],
            self.albedo_alpha[1] * alpha + (1.0 - alpha) * other.albedo_alpha[1],
            self.albedo_alpha[2] * alpha + (1.0 - alpha) * other.albedo_alpha[2],
            self.albedo_alpha[3] * alpha + (1.0 - alpha) * other.albedo_alpha[3],
        ];

        new_voxel.emission = [
            self.emission[0] * alpha + (1.0 - alpha) * other.emission[0],
            self.emission[1] * alpha + (1.0 - alpha) * other.emission[1],
            self.emission[2] * alpha + (1.0 - alpha) * other.emission[2],
        ];

        new_voxel.normal = [
            self.normal[0] * alpha + (1.0 - alpha) * other.normal[0],
            self.normal[1] * alpha + (1.0 - alpha) * other.normal[1],
            self.normal[2] * alpha + (1.0 - alpha) * other.normal[2],
        ];

        new_voxel.roughness = self.roughness * alpha + (1.0 - alpha) * other.roughness;
        new_voxel.roughness = self.metallness * alpha + (1.0 - alpha) * other.metallness;

        new_voxel
    }

    ///Interpolates a given set of voxel data into an average value. Ignores mins/maxs
    ///Since interpolation does not make sense there.
    pub fn interpolate_in_place(&mut self, other_voxels: &[Voxel]) {
        //Calculate the median albedo, roughness, normal and metallness for this voxel.
        let mut albedo_alpha = self.albedo_alpha;
        let mut emission = self.emission;
        let mut normal = self.normal;
        let mut roughness = self.roughness;
        let mut metallness = self.metallness;

        let mut med_vox_count = 1;

        for vox in other_voxels.iter() {
            albedo_alpha[0] += vox.albedo_alpha[0];
            albedo_alpha[1] += vox.albedo_alpha[1];
            albedo_alpha[2] += vox.albedo_alpha[2];
            albedo_alpha[3] += vox.albedo_alpha[3];

            normal[0] += vox.normal[0];
            normal[1] += vox.normal[1];
            normal[2] += vox.normal[2];

            emission[0] += vox.emission[0];
            emission[1] += vox.emission[1];
            emission[2] += vox.emission[2];

            roughness += vox.roughness;

            metallness += vox.metallness;

            med_vox_count += 1;
        }

        //Average all of them, also check if we have a 0.0 which would be fatal when dividing
        if med_vox_count != 0 {
            for i in 0..4 {
                albedo_alpha[i] /= med_vox_count as f32;
            }

            // TODO: Fix the normal calculation since this is currently not correctfor i in 0..3{
            for i in 0..3 {
                normal[i] /= med_vox_count as f32;
            }

            for i in 0..3 {
                emission[i] /= med_vox_count as f32;
            }

            roughness /= med_vox_count as f32;
            metallness /= med_vox_count as f32;
        }

        self.albedo_alpha = albedo_alpha;
        self.emission = emission;
        self.normal = normal;
        self.roughness = roughness;
        self.metallness = metallness;
    }

    ///Interpolates a given set of voxel data into an average value. Ignores mins/maxs
    ///Since interpolation does not make sense there.
    pub fn interpolate(other_voxels: &[Voxel]) -> Self {
        //Calculate the median albedo, roughness, normal and metallness for this voxel.
        let mut albedo_alpha = [0.0; 4];
        let mut emission = [0.0; 3];
        let mut normal = [0.0; 3];
        let mut roughness = 0.0;
        let mut metallness = 0.0;

        let mut med_vox_count = 1;

        for vox in other_voxels.iter() {
            albedo_alpha[0] += vox.albedo_alpha[0];
            albedo_alpha[1] += vox.albedo_alpha[1];
            albedo_alpha[2] += vox.albedo_alpha[2];
            albedo_alpha[3] += vox.albedo_alpha[3];

            normal[0] += vox.normal[0];
            normal[1] += vox.normal[1];
            normal[2] += vox.normal[2];

            emission[0] += vox.emission[0];
            emission[1] += vox.emission[1];
            emission[2] += vox.emission[2];

            roughness += vox.roughness;

            metallness += vox.metallness;

            med_vox_count += 1;
        }

        //Average all of them, also check if we have a 0.0 which would be fatal when dividing
        if med_vox_count != 0 {
            for i in 0..4 {
                albedo_alpha[i] /= med_vox_count as f32;
            }

            // TODO: Fix the normal calculation since this is currently not correctfor i in 0..3{
            for i in 0..3 {
                normal[i] /= med_vox_count as f32;
            }

            for i in 0..3 {
                emission[i] /= med_vox_count as f32;
            }

            roughness /= med_vox_count as f32;
            metallness /= med_vox_count as f32;
        }

        Voxel::new_leaf(albedo_alpha, emission, normal, roughness, metallness)
    }

    ///Turns this voxel into its gpu representation
    fn to_gpu_voxel(
        &self,
        skip_index: u32,
        level: u32,
        is_leaf: bool,
        location: [u32; 3],
    ) -> GpuVoxel {
        assert!(
            self.albedo_alpha[0] <= 1.0
                && self.albedo_alpha[0] >= 0.0
                && self.albedo_alpha[1] <= 1.0
                && self.albedo_alpha[1] >= 0.0
                && self.albedo_alpha[2] <= 1.0
                && self.albedo_alpha[2] >= 0.0
                && self.albedo_alpha[3] <= 1.0
                && self.albedo_alpha[3] >= 0.0,
            "Albedo alpha was not in range: {:?}",
            self.albedo_alpha
        );

        assert!(
            self.roughness <= 1.0 && self.roughness >= 0.0,
            "Roughness not in range!"
        );
        assert!(
            self.metallness <= 1.0 && self.metallness >= 0.0,
            "metallness not in range!"
        );

        assert!(
            self.normal[0] >= -1.0
                && self.normal[0] <= 1.0
                && self.normal[1] >= -1.0
                && self.normal[1] <= 1.0
                && self.normal[2] >= -1.0
                && self.normal[2] <= 1.0,
            "Normal not in range: {:?}!",
            self.normal
        );

        GpuVoxel {
            location: [location[0], location[1], location[2], level],
            emission: [self.emission[0], self.emission[1], self.emission[2], 0.0],
            skip_idx: skip_index,

            albedo_alpha: into_u32(
                f32_to_u8(self.albedo_alpha[0]),
                f32_to_u8(self.albedo_alpha[1]),
                f32_to_u8(self.albedo_alpha[2]),
                f32_to_u8(self.albedo_alpha[3]),
            ),
            roughness_metal_is_leaf_none: into_u32(
                f32_to_u8(self.roughness),
                f32_to_u8(self.metallness),
                if is_leaf { 1 as u8 } else { 0 as u8 },
                0,
            ),
            normal_none: into_u32(
                f32_to_i8(self.normal[0]),
                f32_to_i8(self.normal[1]),
                f32_to_i8(self.normal[2]),
                0,
            ),
        }
    }
}

impl Tooling for Voxel {
    fn empty() -> Self {
        Voxel {
            albedo_alpha: [1.0, 1.0, 1.0, 0.0],
            emission: [0.0; 3],
            normal: [1.0, 0.0, 0.0],
            roughness: 1.0,
            metallness: 0.0,
            index: 0,
        }
    }
    fn interpolate(&mut self, self_weight: f32, others: &[(f32, Self)]) {
        self.albedo_alpha = others.iter().fold(
            [
                self_weight * self.albedo_alpha[0],
                self_weight * self.albedo_alpha[1],
                self_weight * self.albedo_alpha[2],
                self_weight * self.albedo_alpha[3],
            ],
            |old_val, (tweight, tvoxel)| {
                [
                    old_val[0] + (tweight * tvoxel.albedo_alpha[0]),
                    old_val[1] + (tweight * tvoxel.albedo_alpha[1]),
                    old_val[2] + (tweight * tvoxel.albedo_alpha[2]),
                    old_val[3] + (tweight * tvoxel.albedo_alpha[3]),
                ]
            },
        );

        self.emission = others.iter().fold(
            [
                self_weight * self.emission[0],
                self_weight * self.emission[1],
                self_weight * self.emission[2],
            ],
            |old_val, (tweight, tvoxel)| {
                [
                    old_val[0] + (tweight * tvoxel.emission[0]),
                    old_val[1] + (tweight * tvoxel.emission[1]),
                    old_val[2] + (tweight * tvoxel.emission[2]),
                ]
            },
        );

        self.normal = others.iter().fold(
            [
                self_weight * self.normal[0],
                self_weight * self.normal[1],
                self_weight * self.normal[2],
            ],
            |old_val, (tweight, tvoxel)| {
                [
                    old_val[0] + (tweight * tvoxel.normal[0]),
                    old_val[1] + (tweight * tvoxel.normal[1]),
                    old_val[2] + (tweight * tvoxel.normal[2]),
                ]
            },
        );

        self.roughness = others.iter().fold(
            self_weight * self.roughness,
            |old_val, (tweight, tvoxel)| old_val + (tweight * tvoxel.roughness),
        );

        self.metallness = others.iter().fold(
            self_weight * self.metallness,
            |old_val, (tweight, tvoxel)| old_val + (tweight * tvoxel.metallness),
        );
    }
}

///Identifies a Voxel object within the buffer manager
#[derive(Copy, Clone, Debug, PartialEq, PartialOrd, Hash)]
pub struct VoxelObjectId {
    pub id: usize,
}

impl VoxelObjectId {
    pub fn id(&self) -> usize {
        self.id
    }
}

///Represents the gpu version of `VoxelObject`. Several Voxel objects can reference the same
///Octree therefore representing the same object at different locations for instance.
#[derive(Clone, Copy, Debug)]
pub struct GpuVoxelObject {
    ///The model matrix of the voxel object
    pub model_matrix: [[f32; 4]; 4],
    ///The inverse model matrix of the voxel object
    pub inverse_model_matrix: [[f32; 4]; 4],

    ///Mins of this object
    pub mins: [f32; 4],
    pub maxs: [f32; 4],

    ///Offset into the gpu voxel buffer at which the octree starts.
    pub offset: u32,
    ///The size of the octree so that offset+size=end_of_octree/last_leaf
    pub size: u32,
    ///Resolution of this voxel object.
    pub resolution: u32,
    ///Padding for the 16byte addressed shaders
    pub is_not_allocated: u32,
}

impl GpuVoxelObject {
    ///Creates a new gpu representation where only the transform is set up, resolution, offset and size have to be set by the update function.
    pub fn empty_with_transform(trans: na::Matrix4<f32>) -> Self {
        GpuVoxelObject {
            model_matrix: trans.into(),
            inverse_model_matrix: trans
                .try_inverse()
                .expect("Could not create inverse transform matrix!")
                .into(),
            mins: [0.0; 4],
            maxs: [0.0; 4],
            offset: std::u32::MAX,
            size: std::u32::MAX,
            resolution: 0,
            is_not_allocated: 1,
        }
    }

    pub fn setup_from_object(&mut self, cpu_parent: &VoxelObject) {
        self.maxs = [
            cpu_parent.maxs[0],
            cpu_parent.maxs[1],
            cpu_parent.maxs[2],
            0.0,
        ];
        self.mins = [
            cpu_parent.mins[0],
            cpu_parent.mins[1],
            cpu_parent.mins[2],
            0.0,
        ];

        let tmp_res = cpu_parent.voxel.get_accelerator().dimensions();
        self.resolution = tmp_res[0];
        assert!(
            (tmp_res[0] == tmp_res[1]) && (tmp_res[0] == tmp_res[2]) && (tmp_res[1] == tmp_res[2]),
            "Resolution of object does not match on all dimensions: {:?}",
            tmp_res
        );
    }

    pub fn setup_buffer_range(&mut self, octree_offset: u32, octree_size: u32) {
        self.offset = octree_offset;
        self.size = octree_size;
        self.is_not_allocated = 0;
    }
}

///Represents a whole voxel structure in ram containing a root node as well as a extent
/// in all directions.
pub struct VoxelObject {
    voxel: Object<Voxel, SVO<Voxel>>,

    mins: [f32; 3],
    maxs: [f32; 3],

    has_changed: bool,
}

impl VoxelObject {
    pub fn empty() -> Self {
        VoxelObject {
            voxel: Object::new(voxer::octree::SVO::new(Some(0))),
            mins: [0.0; 3],
            maxs: [0.0; 3],
            has_changed: false,
        }
    }

    pub fn from_data(voxel: Object<Voxel, SVO<Voxel>>, mins: [f32; 3], maxs: [f32; 3]) -> Self {
        VoxelObject {
            voxel,
            mins,
            maxs,
            has_changed: true,
        }
    }

    ///Creates a voxel object from a function. The function gets 3d coordinates supplied within an 3d `interval`.
    /// For the resulting voxel (or no voxel if it is not within the volume) an octree acceleration structure is created
    pub fn from_func<F>(resolution: u32, mins: [f32; 3], maxs: [f32; 3], f: F) -> Self
    where
        F: Fn([u32; 3]) -> Option<Voxel>,
    {
        let mut obj = Object::new(SVO::new(Some(resolution)));

        for x in 0..resolution {
            for y in 0..resolution {
                for z in 0..resolution {
                    if let Some(v) = f([x, y, z]) {
                        obj.insert([x, y, z], v)
                    }
                }
            }
        }

        VoxelObject {
            voxel: obj,
            mins,
            maxs,
            has_changed: true,
        }
    }

    ///Returns the inner voxel buffer which can be used for fast access of single voxels
    pub fn get_inner(&self) -> &Object<Voxel, SVO<Voxel>> {
        &self.voxel
    }

    pub fn get_inner_mut(&mut self) -> &mut Object<Voxel, SVO<Voxel>> {
        self.has_changed = true;
        &mut self.voxel
    }

    pub fn has_changed(&self) -> bool {
        self.has_changed
    }

    pub fn reset_has_changed_status(&mut self) {
        self.has_changed = false;
    }

    ///Translates the SVO of this voxel object into a linear, depth-first search array.
    ///The resulting vec can be directly put into gpu memory.
    pub fn to_gpu_voxel_object(&mut self) -> Vec<GpuVoxel> {
        self.voxel.to_flat_tree()
    }

    pub fn extent(&self) -> [f32; 3] {
        [
            self.maxs[0] - self.mins[0],
            self.maxs[1] - self.mins[1],
            self.maxs[2] - self.mins[2],
        ]
    }

    pub fn mins(&self) -> [f32; 3] {
        self.mins
    }

    pub fn maxs(&self) -> [f32; 3] {
        self.maxs
    }

    pub fn mins_maxs_with_transform(&self, transform: [[f32; 4]; 4]) -> ([f32; 3], [f32; 3]) {
        let matrix = na::Matrix4::from(transform);
        let org_min = na::Point3::from(self.mins);
        let org_max = na::Point3::from(self.maxs);

        let trans_min = matrix * org_min.to_homogeneous();
        let trans_max = matrix * org_max.to_homogeneous();

        let new_min = [
            trans_min.x.min(trans_max.x),
            trans_min.y.min(trans_max.y),
            trans_min.z.min(trans_max.z),
        ];
        let new_max = [
            trans_min.x.max(trans_max.x),
            trans_min.y.max(trans_max.y),
            trans_min.z.max(trans_max.z),
        ];

        (new_min, new_max)
    }

    pub fn num_voxels(&self) -> usize {
        self.voxel.get_accelerator().num_voxel() as usize
    }

    pub fn resolution(&self) -> u32 {
        self.voxel.get_accelerator().dimensions()[0]
    }
}

trait Index {
    ///Traverses down an octree and sets the index of each node correctly
    fn index(&self, index: &mut u32, buffer: &mut Object<Voxel, SVO<Voxel>>);
}

impl Index for OctNode<Voxel> {
    fn index(&self, index: &mut u32, buffer: &mut Object<Voxel, SVO<Voxel>>) {
        //Get voxel of this node from the buffer and setup to correct index.
        //Then traverse down.
        if let Some(vox) = buffer.get_voxel_by_index_mut(self.get_voxel_index()) {
            vox.index = *index;
        } else {
            assert!(
                false,
                "Could not get voxel for index, that should not happen!"
            );
        }

        *index += 1;

        for x in 0..2 {
            for y in 0..2 {
                for z in 0..2 {
                    if let Some(child) = &self.children()[x * 4 + y * 2 + z] {
                        child.index(index, buffer);
                    }
                }
            }
        }
    }
}

///Trait which implements the recursive transformation of some SVO into a linear,
///gpu handleable vector of voxels.
pub trait ToGpuVoxels {
    fn to_gpu(
        &self,
        skip_index: u32,
        level: u32,
        buffer: &mut Vec<GpuVoxel>,
        object: &mut Object<Voxel, SVO<Voxel>>,
    );
}

impl ToGpuVoxels for OctNode<Voxel> {
    fn to_gpu(
        &self,
        skip_index: u32,
        level: u32,
        buffer: &mut Vec<GpuVoxel>,
        object: &mut Object<Voxel, SVO<Voxel>>,
    ) {
        let own = object
            .get_voxel_by_index(self.get_voxel_index())
            .expect("Could not get voxel for index!")
            .to_gpu_voxel(skip_index, level, self.is_leaf(), self.get_min_max().0);

        buffer.push(own);
        //Find the dfs index we gave to this node and check that we are inserting at the right location.
        let node_index = object
            .get_voxel_by_index(self.get_voxel_index())
            .expect("Could not get voxel to node. Should not happen!")
            .index;
        assert!(
            buffer.len() as u32 == (node_index + 1),
            "Pushed voxel to wrong index: VoxelIdx: {}, buffer_len: {}",
            node_index,
            buffer.len()
        );

        for x in 0..2 {
            for y in 0..2 {
                for z in 0..2 {
                    if let Some(ref c) = self.children()[x * 4 + y * 2 + z] {
                        //The new skip index will be the next possible child, or, if there is non,
                        //the parents skip index.
                        let new_skip_index = {
                            let mut next_pos_child = x * 4 + y * 2 + z + 1;
                            let mut next_skip_index = None;

                            while next_pos_child < 8 {
                                if let Some(ref c) = self.children()[next_pos_child] {
                                    next_skip_index = Some(
                                        object
                                            .get_voxel_by_index(c.get_voxel_index())
                                            .expect("Failed to get voxel for index")
                                            .index,
                                    );
                                    break;
                                }
                                //Change to the next one
                                next_pos_child += 1;
                            }

                            if let Some(sk_idx) = next_skip_index {
                                sk_idx
                            } else {
                                //If we could not find a child with a skip index, use the parent
                                skip_index
                            }
                        };

                        c.to_gpu(new_skip_index, level + 1, buffer, object);
                    }
                }
            }
        }
    }
}

///Takes a float [0,1] and compresses it down into a u8.
pub fn f32_to_u8(f: f32) -> u8 {
    assert!(
        f >= 0.0 && f <= 1.0,
        "Float needs to be between 0,1 but was: {}",
        f
    );
    (f * 255.0) as u8
}

///Takes a float [-1,1] and compresses it down into a i8 (but as u8 for the gpu)
pub fn f32_to_i8(f: f32) -> u8 {
    assert!(
        f <= 1.0 && f >= -1.0,
        "Float needs to be between -1,1 but was: {}",
        f
    );
    let f = f.min(1.0).max(-1.0);
    (((f + 1.0) / 2.0) * 255.0) as u8
}

/// Packs four u8 into one u32. Mainly used to pack albedo data and similar
/// into the voxel structs on the gpu.
pub fn into_u32(one: u8, two: u8, three: u8, four: u8) -> u32 {
    let mut start: u32 = 0;
    //expand inputs to 32 bit, then shift them into their location and xor them together.
    let mut one: u32 = one.into();
    let mut two: u32 = two.into();
    let mut three: u32 = three.into();
    let four: u32 = four.into();

    one = one << 24;
    two = two << 16;
    three = three << 8;

    start = (((start | one) | two) | three) | four;

    start
}
