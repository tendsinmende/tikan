/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use marp::ash::vk;
use marp::buffer::Buffer;
use marp::pipeline::*;
use marp::*;
use std::mem;
use std::sync::Arc;

use crate::render::debug_pass::DebugDrawType;

///Defines a normal vertex which can be supplied to the standard  renderpass and pipeline used by the
/// renderer.
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Vertex {
    pub pos: [f32; 4],
}

#[macro_export]
macro_rules! vertex(
    (a: f32, b: f32, c:f32) => (
        Vertex{
            pos: [a,b,c,1.0]
        }
    );
);

///Returns the vertex state of `Vertex` which can be used to build a graphical pipeline which uses
///these vertices as input.
pub fn get_default_vertex_state() -> VertexInputState {
    //This one descibes how vertices are bound to the shader.
    pipeline::VertexInputState {
        vertex_binding_descriptions: vec![vk::VertexInputBindingDescription::builder()
            .binding(0)
            .stride(mem::size_of::<Vertex>() as u32)
            .input_rate(vk::VertexInputRate::VERTEX)
            .build()],
        vertex_attribute_descriptions: vec![
            //Description of the Pos attribute
            vk::VertexInputAttributeDescription::builder()
                .location(0)
                .binding(0)
                .format(vk::Format::R32G32B32A32_SFLOAT)
                .offset(offset_of!(Vertex, pos) as u32)
                .build(),
        ],
    }
}

///A used to setup a vertex mesh of some kind.
pub struct VertexMeshBuilder {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,
    pub draw_type: DebugDrawType,
}

impl VertexMeshBuilder {
    pub fn to_mesh(self, id: MeshId) -> TriangleMesh {
        TriangleMesh {
            id,
            vertices: self.vertices,
            indices: self.indices,
            draw_type: self.draw_type,
        }
    }
}

///Identifies a loaded mesh within the BufferManager.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct MeshId {
    pub id: usize,
}

pub struct TriangleMesh {
    id: MeshId,
    ///The raw vertices of this mesh
    pub vertices: Vec<Vertex>,
    ///Raw indices into this mesh, depending on the draw type either batches of 3 indices make a triangle,
    /// of batches of two a line
    pub indices: Vec<u32>,
    ///Defines if this is draw via lines primitives or as triangles.
    pub draw_type: DebugDrawType,
}

impl TriangleMesh {
    pub fn new(
        id: MeshId,
        vertices: Vec<Vertex>,
        indices: Vec<u32>,
        draw_type: DebugDrawType,
    ) -> Self {
        match draw_type {
            DebugDrawType::Filled(_) => {
                if (indices.len() % 3) != 0 {
                    panic!(
                        "When using DebugDrawType::Filled, the indices should be a multiple of 3"
                    )
                }
            }
            DebugDrawType::Lines(_) => {
                if (indices.len() % 2) != 0 {
                    panic!("When using DebugDrawType::Lines, the indices should be a multiple of 2")
                }
            }
        }

        TriangleMesh {
            id,
            vertices,
            indices,
            draw_type,
        }
    }

    pub fn draw_type(&self) -> DebugDrawType {
        self.draw_type.clone()
    }

    pub fn indice_count(&self) -> u32 {
        self.indices.len() as u32
    }
}

///Represents a draw call for a mesh id with some dynamic properties like its location
pub struct MeshDrawCall {
    pub index_count: u32,
    pub vertex_buffer: Arc<Buffer>,
    pub index_buffer: Arc<Buffer>,
    pub transform: [[f32; 4]; 4],
    pub color: [f32; 4],
}
