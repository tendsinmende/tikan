/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

///Defines voxel objects as well as how they are stored and loaded.
pub mod voxel_object;

///Defines how a triangle based object is loaded and used for either
/// Rendering it in the debug pass, or creating a voxel representation from it.
pub mod triangle_object;

///Manages all voxel and triangle objects that are loaded as well as BVH creation
pub mod data_manager;

///Defines a camera which returns information for primary ray generation.
pub mod camera;

///Can track the event states
pub mod event_state;

///Defines a scene. Is able to construct a acceleration structure for a scene
/// and to update its entities.
pub mod scene;

///Contains special component types that can be used by an entity.
pub mod components;

///Manages CPU Side voxel data
pub mod voxel_data_manager;

///Manages CPU Side triangle data
pub mod triangle_data_manager;

///Bvh implementation optimized for creation speed and usage on the gpu.
pub mod bvh;

pub use widkan::msw::winit::event::{Event, KeyboardInput, WindowEvent};
