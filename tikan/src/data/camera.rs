/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use na;
use std::f32::consts::PI;

use crate::config::Config;
use crate::data::event_state::KeyStates;
use widkan::msw::winit::event::*;

///The camera representation passed to the shaders
#[derive(Clone, Debug)]
#[repr(C)]
pub struct ShaderCamera {
    pub transform: [[f32; 4]; 4],
    pub transform_inverse: [[f32; 4]; 4],
    pub projection: [[f32; 4]; 4],

    pub fov: f32,
    pub depth: f32,
    pub quality_threshold: f32,
    pub cone_start: f32,

    pub cone_max_length: f32,
    pub cone_min_aperture: f32,
    pub aperture: f32,
    pub sensitivity: f32,

    pub shutter_speed: f32,
    pub pad: [f32; 3],
}

///The default, perspective camera
#[derive(Clone, Debug)]
pub struct Camera {
    ///World transform
    transform: na::Isometry3<f32>,

    fov: f32,
    depth: f32,

    aperture: f32,
    sensitivity: f32,
    shutter_speed: f32,
}

impl Camera {
    ///Builds a camera for an given image extent,
    /// an fov for the view,
    /// a max depth for the fragments in WorldspaceUnits and
    /// a given initial transform matrix which places the camera into worldspace,
    /// in opengl this would be the inverse view matrix.
    pub fn new(fov: f32, depth: f32, aperture: f32, sensitivity: f32, shutter_speed: f32) -> Self {
        Camera {
            transform: na::Isometry3::from_parts(
                na::Translation3::identity(),
                na::UnitQuaternion::from_euler_angles(0.0, PI, PI),
            ),
            fov,
            depth,
            aperture,
            sensitivity,
            shutter_speed,
        }
    }

    ///Can set the physical camera settings
    pub fn set_phys_settings(
        &mut self,
        aperture: Option<f32>,
        sensitivity: Option<f32>,
        shutter_speed: Option<f32>,
    ) {
        if let Some(a) = aperture {
            self.aperture = a;
        }

        if let Some(s) = sensitivity {
            self.sensitivity = s;
        }

        if let Some(s) = shutter_speed {
            self.shutter_speed = s;
        }
    }

    pub fn get_transform(&mut self) -> &mut na::Isometry3<f32> {
        &mut self.transform
    }

    pub fn get_shader_camera(&self, image_ratio: f32, config: &Config) -> ShaderCamera {
        //Transform means here from world space to camera space. Therefor we need an inverse tranform
        // to get primary from camera space into world space
        let transform_inverse: na::Matrix4<f32> = self
            .transform
            .to_homogeneous()
            .try_inverse()
            .expect("failed to invert");

        let transform: na::Matrix4<f32> = self.transform.to_homogeneous();

        let projection: na::Matrix4<f32> = na::Matrix4::new_perspective(
            image_ratio,                               //ratio
            self.fov * (std::f32::consts::PI / 180.0), //Use Fovy, but transform from degree to rad
            0.1,                                       //near plane
            self.depth,                                //far
        );

        ShaderCamera {
            transform: transform.into(),
            transform_inverse: transform_inverse.into(),
            projection: projection.into(),
            fov: self.fov,
            depth: self.depth,
            quality_threshold: config.tracing_quality_threshold,
            cone_start: config.cone_start,
            cone_max_length: config.cone_max_length,
            cone_min_aperture: config.cone_min_aperture,
            aperture: self.aperture,
            sensitivity: self.sensitivity,
            shutter_speed: self.shutter_speed,

            pad: [0.0; 3],
        }
    }
}

pub struct FreeFlightController {
    speed: f32,
}

impl FreeFlightController {
    pub fn new(speed: f32) -> Self {
        FreeFlightController { speed }
    }
}

impl CameraController for FreeFlightController {
    fn update(
        &mut self,
        camera: &mut Camera,
        time_delta: std::time::Duration,
        events: &[Event<()>],
        states: &KeyStates,
    ) {
        //Do not move is right mouse button is not pressed
        if !states.is_pressed_mouse(MouseButton::Right) {
            return;
        }

        let tdelta = time_delta.as_secs_f32();
        let forward: na::Vector3<f32> = camera
            .transform
            .rotation
            .transform_vector(&na::Vector3::new(0.0, 0.0, -1.0));
        let right: na::Vector3<f32> = camera
            .transform
            .rotation
            .transform_vector(&na::Vector3::new(1.0, 0.0, 0.0));

        let mut translation = na::Vector3::new(0.0, 0.0, 0.0);

        if states.is_pressed(VirtualKeyCode::W) {
            translation += forward * self.speed * tdelta as f32;
        }
        if states.is_pressed(VirtualKeyCode::S) {
            translation -= forward * self.speed * tdelta as f32;
        }
        if states.is_pressed(VirtualKeyCode::D) {
            translation += right * self.speed * tdelta as f32;
        }
        if states.is_pressed(VirtualKeyCode::A) {
            translation -= right * self.speed * tdelta as f32;
        }

        if states.is_pressed(VirtualKeyCode::E) {
            translation.y += self.speed * tdelta as f32;
        }
        if states.is_pressed(VirtualKeyCode::Q) {
            translation.y -= self.speed * tdelta as f32;
        }

        let mut mouse_deltas = [0.0, 0.0];

        if states.is_pressed(VirtualKeyCode::Up) {
            mouse_deltas[1] += 1.0;
        }
        if states.is_pressed(VirtualKeyCode::Down) {
            mouse_deltas[1] -= 1.0;
        }
        if states.is_pressed(VirtualKeyCode::Right) {
            mouse_deltas[0] += 1.0;
        }
        if states.is_pressed(VirtualKeyCode::Left) {
            mouse_deltas[0] -= 1.0;
        }

        for e in events {
            //println!("EV: {:?}", e);
            match e {
                Event::DeviceEvent {
                    event,
                    device_id: _,
                } => {
                    match event {
                        DeviceEvent::MouseMotion { delta } => {
                            mouse_deltas[0] += delta.0;
                            mouse_deltas[1] += delta.1;
                        }
                        _ => {} //no other device events needed
                    }
                }

                Event::WindowEvent {
                    window_id: _,
                    event:
                        WindowEvent::CursorMoved {
                            device_id: _,
                            position,
                            modifiers,
                        },
                } => {
                    //println!("Cursor moved!");
                    //println!("TODO: Handle non device event");
                }
                _ => {} //no other events needed
            }
        }

        //println!("DeltaMouse:  {:?}, time_delta: {}", mouse_deltas, tdelta);
        //println!("Forward: {}, right: {}, translation: {}", forward, right, self.transform.translation.vector);

        //Build rotation from the inputs and add it to the current rotation.
        let rotation_yaw = na::UnitQuaternion::from_axis_angle(
            &na::Unit::new_normalize(na::Vector3::new(0.0, 1.0, 0.0)), //Pitch is always around the y axis (up)
            mouse_deltas[0] as f32 * tdelta as f32,
        );

        let rotation_pitch = na::UnitQuaternion::from_axis_angle(
            &na::Unit::new_normalize(right), //We move the camera "up" around the current right axis
            mouse_deltas[1] as f32 * tdelta as f32,
        );

        let rotation = rotation_yaw * rotation_pitch;

        camera.get_transform().rotation = rotation * camera.get_transform().rotation;
        camera.get_transform().translation.vector += translation;
    }
}

pub trait CameraController {
    //Executed on each update of the camera controller
    fn update(
        &mut self,
        camera: &mut Camera,
        time_delta: std::time::Duration,
        events: &[Event<()>],
        states: &KeyStates,
    ) {
    }
}
