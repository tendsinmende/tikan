/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::data::{
    bvh::{Bvh, GpuBvhNode},
    components::*,
    triangle_object::*,
    voxel_data_manager::{VoxelDataId, VoxelDataManager},
    voxel_object::*,
};
use crate::render::buffer_manager::BufferManager;
use std::collections::hash_map::HashMap;
use std::path::Path;
use std::sync::Arc;
use tikan_ecs::*;

use std::any::TypeId;
use std::convert::TryInto;

impl_ecs!(
    name: Ecs,
    entity: Entity{
        components: [
            transform: Transform,
            triangle_mesh: MeshId,
            voxel_object: VoxelDataId
        ],
        member:[
            name: String
        ]
    }
);

macro_rules! impl_getter_setter {
    ($set_name:ident, $get_name:ident, $get_mut_name:ident, $component_name:ident : $component_type:ty) => {
        ///Sets the value of this component for the given entity, does nothing if the entity
        /// does not exist.
        pub fn $set_name(&mut self, entity: &EntityId, $component_name: $component_type) {
            if let Some(component) = self.ecs.$component_name(entity) {
                *component = Some($component_name);
            }
        }

        ///Returns a mutable reference to the component for `entity`. This is None there is not Entity for this id.
        /// If it is Some(T), the T could be None, this means that the entity does not use this component
        ///
        ///NOTE: If you want to process loads of components together it is better to iterate over all components of a type
        /// by using `get_mut()` on the ecs like this: `let (mut pos, vel) = scene.get_ecs_mut().get_mut(&my_entity);`.
        pub fn $get_mut_name(&mut self, entity: &EntityId) -> Option<&mut Option<$component_type>> {
            self.ecs.$component_name(entity)
        }
    };
}

pub struct Scene {
    ///ECS context for this scene
    ecs: Ecs,
    entities: Vec<EntityId>,

    //The current set of active voxel objects as well as the bvh that was created for this set of active objects.
    //Can be used to trace the voxels between frames (for collision test for instance) but is also used to
    //create the top-level acceleration bvh for the next frame
    voxel_bvh: Bvh,
    //The set of active voxels, used for the bvh generation
    active_voxel_buffer: Vec<(EntityId, VoxelDataId, GpuVoxelObject)>,
}

impl Scene {
    ///Creates a new, empty scene.
    pub fn new() -> Self {
        Scene {
            ecs: Ecs::new(),
            entities: Vec::new(),
            voxel_bvh: Bvh::from_objects(vec![]),
            active_voxel_buffer: vec![],
        }
    }

    impl_getter_setter!(
        set_transform,
        get_transform,
        get_transform_mut,
        transform: Transform
    );

    impl_getter_setter!(
        set_triangle_mesh,
        get_triangle_mesh,
        get_triangle_mesh_mut,
        triangle_mesh: MeshId
    );

    impl_getter_setter!(
        set_voxel_object,
        get_voxel_object,
        get_voxel_object_mut,
        voxel_object: VoxelDataId
    );

    ///Creates a new entity in the ecs, returns the id to indentify it later.
    pub fn new_entity(&mut self, name: &str) -> EntityId {
        let id = self.ecs.new_entity(name.to_owned());
        self.entities.push(id);
        id
    }

    ///Returns a possible id for an entity with that name.

    ///Loads a scene from a path.
    /// # Warning
    /// Currently unimplemented!
    pub fn load(path: &Path) -> Self {
        unimplemented!("Loading a scene from file is unimplemented!");
    }

    ///Returns a reference to the underlying ECS, should be used to manipulate components and entities
    pub fn get_ecs(&self) -> &Ecs {
        &self.ecs
    }

    pub fn get_ecs_mut(&mut self) -> &mut Ecs {
        &mut self.ecs
    }

    ///Builds all mesh draw calls. The first vec are the filled ones, the second the lines
    pub fn get_all_meshes(&self) -> Vec<(MeshId, Transform)> {
        let mut all_meshes = Vec::new();
        for e in self.entities.iter() {
            if let Some((transform, tri_mesh_id, _voxel_id)) = self.ecs.get(e) {
                match (transform, tri_mesh_id) {
                    (Some(t), Some(mesh_id)) => all_meshes.push((*mesh_id, *t)),
                    _ => {}
                }
            }
        }

        return all_meshes;
        /*
        let mut line_ms = Vec::new();
        let mut triangle_ms = Vec::new();
        for e in self.entities.iter(){
            if let Some((transform, triangle_mesh, _voxel_id)) = self.ecs.get(e){

                match (transform, triangle_mesh){
                    (Some(transform), Some(mesh_id)) => {
                        if let Some(mesh_data) = buffer_manager.get_mesh(*mesh_id){

                            //Now try to get the buffers, if there is one missing, notify the buffer manager that the
                            // mesh has to be loaded. This will trigger an upload
                            //next time it is possible
                            let vertex_buffer = if let Some(vb) = buffer_manager.get_mesh_vertex_buffer(mesh_id){
                                vb
                            }else{
                                //Is not loaded, add notification then skip
                                buffer_manager.notify_mesh_needed(mesh_id);
                                continue;
                            };

                            let index_buffer = if let Some(ib) = buffer_manager.get_mesh_index_buffer(mesh_id){
                                ib
                            }else{
                                //Is not loaded, add notification then skip
                                buffer_manager.notify_mesh_needed(mesh_id);
                                continue;
                            };

                            //Now check which pipeline we have to use and add the drawcall
                            match mesh_data.draw_type(){
                                DebugDrawType::Filled(col) => {
                                    //Add to color meshes
                                    triangle_ms.push(
                                        MeshDrawCall{
                                            index_count: mesh_data.indice_count(),
                                            vertex_buffer: vertex_buffer,
                                            index_buffer: index_buffer,
                                            transform: transform.to_matrix4().into(),
                                            color: col
                                        }
                                    );
                                },
                                DebugDrawType::Lines(col) => {
                                    //Add to lines
                                    line_ms.push(
                                        MeshDrawCall{
                                            index_count: mesh_data.indice_count(),
                                            vertex_buffer: vertex_buffer,
                                            index_buffer: index_buffer,
                                            transform: transform.to_matrix4().into(),
                                            color: col
                                        }
                                    )
                                }
                            }
                        }else{
                            println!("No data under id!");
                            //Currently always loading all meshes, later we can only load those who are needed in some way
                            buffer_manager.notify_mesh_needed(mesh_id);
                        }
                    },
                    _ => {
                        //println!("No data for them!");
                    }
                }


            }
        }

        (triangle_ms, line_ms)
         */
    }

    ///Updates the inner accerleration structures etc.
    pub fn update(&mut self, voxel_data_manager: &VoxelDataManager) {
        //First find all active voxel objects. They are used to build a bvh.
        let active_objects: Vec<(EntityId, VoxelDataId, GpuVoxelObject)> = self
            .entities
            .iter()
            .filter_map(|entry| {
                if let Some((transform, _triangle_mesh, voxel_data_id)) = self.ecs.get(entry) {
                    match (transform, voxel_data_id) {
                        (Some(trans), Some(voxdid)) => {
                            //Before adding the voxel object to the collection, check if there is actually some data
                            if voxel_data_manager.get(voxdid).is_some() {
                                Some((
                                    *entry,
                                    *voxdid,
                                    GpuVoxelObject::empty_with_transform(trans.to_matrix4()),
                                ))
                            } else {
                                None
                            }
                        }
                        _ => None,
                    }
                } else {
                    None
                }
            })
            .collect();

        //Now fetch the extents of each active voxel object from the voxel data manager
        //and build a bvh for that.
        let bvh_builder_objects: Vec<(EntityId, u32, [f32;3], [f32;3])> = active_objects.iter().enumerate().map(|(idx, (eid, voxel_data_id, gpu_voxel))|{
            if let Some(data) = voxel_data_manager.get(voxel_data_id){

                let (min, max) = data.mins_maxs_with_transform(gpu_voxel.model_matrix);
                (
                    *eid,
                    idx.try_into().expect("Could not convert bvh's object_index into u32!"),
                    min,
                    max
                )
            }else{
                panic!("Could not build bvh data set since there was no data for the given voxel data id!");
            }
        }).collect();

        let bvh = Bvh::from_objects(bvh_builder_objects);

        self.voxel_bvh = bvh;
        self.active_voxel_buffer = active_objects;

        //self.voxel_bvh.print()
    }

    ///Returns all currently Voxel objects as an id and its gpu representation.
    pub fn get_active_voxel_objects(&self) -> Vec<(VoxelDataId, GpuVoxelObject)> {
        self.active_voxel_buffer
            .clone()
            .into_iter()
            .map(|(_e, id, obj)| (id, obj))
            .collect()
    }

    pub fn get_gpu_bvh(&self) -> Vec<GpuBvhNode> {
        self.voxel_bvh.to_flat()
    }
}
