/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::data::{
    bvh::GpuBvhNode,
    camera::{Camera, CameraController, FreeFlightController, ShaderCamera},
    components::transform::Transform,
    event_state::KeyStates,
    scene::*,
    triangle_object::*,
    voxel_data_manager::{VoxelDataId, VoxelDataManager},
    voxel_object::{GpuVoxel, GpuVoxelObject, VoxelObject, VoxelObjectId},
};

use crate::config::Config;
use std::sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard};
use std::time::{Duration, Instant};
use widkan::msw::winit;

use crate::message::*;
use crate::render::InterfaceInfo;

///Handles all loaded object as well as scene management of those objects.
pub struct DataManager {
    config: Arc<RwLock<Config>>,

    interface: RwLock<Option<InterfaceInfo>>,

    camera: RwLock<Camera>,
    events: RwLock<Vec<winit::event::Event<'static, ()>>>,
    key_states: RwLock<KeyStates>,
    last_update: RwLock<Instant>,

    scene: RwLock<Scene>,

    ///Holds cpu side voxel data
    voxel_data_manager: RwLock<VoxelDataManager>,

    ///Saves all services that have to be executed per update.
    services: RwLock<Vec<Arc<RwLock<dyn Service + Send + Sync>>>>,
    camera_controller: RwLock<Arc<RwLock<dyn CameraController + Send + Sync>>>,
}

impl DataManager {
    pub fn new(config: Arc<RwLock<Config>>) -> Arc<Self> {
        //always load an empty scene first.
        let mini_scene = Scene::new();
        let camera = RwLock::new(Camera::new(
            config
                .read()
                .expect("Could not get config for active camera!")
                .camera_fov,
            config
                .read()
                .expect("Could not get config for active camera!")
                .camera_depth,
            25.0,       //Aperture
            500.0,      //iso
            1.0 / 25.0, //Shutterspeed
        ));

        Arc::new(DataManager {
            config,
            interface: RwLock::new(None),
            camera,
            events: RwLock::new(Vec::new()),
            key_states: RwLock::new(KeyStates::new()),
            last_update: RwLock::new(Instant::now()),

            scene: RwLock::new(mini_scene),
            voxel_data_manager: RwLock::new(VoxelDataManager::new()),
            services: RwLock::new(Vec::new()),
            camera_controller: RwLock::new(Arc::new(RwLock::new(FreeFlightController::new(20.0)))),
        })
    }

    ///Appends elements to be processed.
    pub fn add_events(&self, mut events: std::vec::Vec<winit::event::Event<'static, ()>>) {
        //Copy them into Vec
        self.events
            .write()
            .expect("Failed to add new events!")
            .append(&mut events);
    }

    ///Updates the current subsystems by locking parts of them at a time.
    pub fn update(&self, message: Vec<ObjectMessage>) {
        //Handle messages that have been passed since the last update
        for m in message.into_iter() {
            match m {
                ObjectMessage::SetAperture(a) => self
                    .camera
                    .write()
                    .expect("Could not set camera")
                    .set_phys_settings(Some(a), None, None),
                ObjectMessage::SetIso(a) => self
                    .camera
                    .write()
                    .expect("Could not set camera")
                    .set_phys_settings(None, Some(a), None),
                ObjectMessage::SetShutterSpeed(s) => self
                    .camera
                    .write()
                    .expect("Could not set camera")
                    .set_phys_settings(None, None, Some(s)),
                ObjectMessage::Job(j) => j(self),
                ObjectMessage::ConvertGltf {
                    location: _,
                    resolution: _,
                    sampling_count: _,
                } => println!("Converting Gltf in engine is unimplemented!"),
                ObjectMessage::ImportBin(location) => println!("Importing bin is unimplemented!"),
                ObjectMessage::TransformEntity { id, trans } => {
                    println!("Transforming entity is unimplemented!")
                }
                ObjectMessage::RegisterService(service) => {
                    self.services
                        .write()
                        .expect("Could not push new service")
                        .push(service.clone());
                    service
                        .write()
                        .expect("Failed to lock service for onCreate call")
                        .on_create(self);
                }
                ObjectMessage::Dummy => {}
            }
        }

        //Update key states
        let mut events = Vec::with_capacity(
            self.events
                .read()
                .expect("Could not get event length")
                .len(),
        );
        std::mem::swap(
            &mut events,
            &mut self
                .events
                .write()
                .expect("Could not get events since last update"),
        );

        //Update inner keymap to new events
        self.key_states
            .write()
            .expect("Failed to update key-states")
            .update(&events);

        //find delta in seconds
        let delta_duration = self
            .last_update
            .read()
            .expect("failed to read last update!")
            .elapsed();

        *self
            .last_update
            .write()
            .expect("Failed to set new time stamp for delta calc") = Instant::now();

        let mut service_iterator = Vec::with_capacity(
            self.services
                .read()
                .expect("Could not get services size")
                .len(),
        );

        std::mem::swap(
            &mut service_iterator,
            &mut self.services.write().expect("Could not swap services"),
        );
        let mut size_changed = false;
        service_iterator = service_iterator
            .into_iter()
            .filter_map(|service| {
                service
                    .write()
                    .expect("Failed to lock service for execution")
                    .on_update(
                        self,
                        delta_duration,
                        &self.key_states.read().expect("Could not get keystates"),
                        &events,
                    );

                //Check if this service should stop, if thats the case, remove
                if service
                    .read()
                    .expect("Could not check services remove state")
                    .should_stop()
                {
                    size_changed = true;
                    None
                } else {
                    Some(service)
                }
            })
            .collect();

        //Swap services back into local storage
        std::mem::swap(
            &mut service_iterator,
            &mut self.services.write().expect("Could not swap services back"),
        );
        //If needed fix allocation of array
        if size_changed {
            self.services
                .write()
                .expect("Could not check serivces size")
                .shrink_to_fit();
        }

        //update the camera with the new signals
        self.camera_controller
            .write()
            .expect("Failed to lock camera controller!")
            .write()
            .expect("Failed to lock inner camera controller")
            .update(
                &mut self.camera.write().expect("Could not lock camera!"),
                delta_duration,
                &events,
                &self
                    .key_states
                    .read()
                    .expect("Could not read key states for camera!"),
            );

        //Now update the scenes internal bvh
        self.scene.write().expect("Could not update scene!").update(
            &self
                .voxel_data_manager
                .read()
                .expect("Could not lock voxel data manager for scene update!"),
        );
    }

    pub fn get_render_data(
        &self,
        image_ratio: f32,
    ) -> (
        ShaderCamera, //Camera data used to which is rendered
        Vec<(VoxelDataId, GpuVoxelObject)>,
        Vec<GpuBvhNode>,
        Vec<(MeshId, Transform)>,
    ) {
        let camera = self
            .camera
            .read()
            .expect("Failed to get camera")
            .get_shader_camera(
                image_ratio,
                &self.config.read().expect("Failed to get config"),
            );

        let buffer_lock = self
            .voxel_data_manager
            .read()
            .expect("Failed to read voxel data manager");
        let setuped_voxels = self
            .get_active_voxel_objects()
            .into_iter()
            .filter_map(|(id, mut object)| {
                if let Some(cpu_object) = buffer_lock.get(&id) {
                    object.setup_from_object(cpu_object);
                    Some((id, object))
                } else {
                    None
                }
            })
            .collect();

        let bvh = self
            .scene
            .read()
            .expect("Could not lock scene for gpu bvh building")
            .get_gpu_bvh();

        (camera, setuped_voxels, bvh, self.get_active_meshes())
    }

    pub fn get_gpu_load_calls(&self) -> Vec<(VoxelDataId, Vec<GpuVoxel>)> {
        self.voxel_data_manager
            .write()
            .expect("Failed to get voxel data manager")
            .get_all_changed()
    }

    ///Returns two sets of draw calls. The first one are triangle meshes, the second one are line meshes
    pub fn get_active_meshes(&self) -> Vec<(MeshId, Transform)> {
        self.get_scene().get_all_meshes()
    }

    ///Returns all currently active voxel objects in this scene.
    fn get_active_voxel_objects(&self) -> Vec<(VoxelDataId, GpuVoxelObject)> {
        self.get_scene().get_active_voxel_objects()
    }
    /*
        ///Loads a new mesh from data and returns the Id under which it is referenced.
        pub fn add_triangle_mesh(&self, mesh: VertexMeshBuilder) -> MeshId{
            self.get_buffer_manager_mut().add_triangle_mesh(mesh)
        }
    */
    ///Loads a voxel object into the manager.
    pub fn add_voxel_object(&self, voxel_object: VoxelObject) -> VoxelDataId {
        self.voxel_data_manager
            .write()
            .expect("Failed to get vdm")
            .insert(voxel_object)
    }

    ///Returns a reference to the scene with an id, or if no id is given to the current active one
    pub fn get_scene<'a>(&'a self) -> RwLockReadGuard<'a, Scene> {
        self.scene.read().expect("Failed to read scene!")
    }

    ///Returns a mutable reference to the current scene.
    ///
    /// # Safety and performance
    ///
    ///This blocks execution of every other service
    ///that needs to read (or write) data from or to the scene.
    ///Therefore the lock should be held as short as possible
    pub fn get_scene_mut<'a>(&'a self) -> RwLockWriteGuard<'a, Scene> {
        self.scene.write().expect("Failed to write to scene!")
    }

    pub fn set_interface(&self, interface: InterfaceInfo) {
        *self.interface.write().expect("Failed to set interface") = Some(interface);
    }

    ///Exchanges the current camera controller with the new one.
    pub fn set_camera_controller(
        &self,
        new_controller: Arc<RwLock<dyn CameraController + Send + Sync>>,
    ) {
        *self
            .camera_controller
            .write()
            .expect("Could not set camera controller") = new_controller;
    }
}

pub fn duration_to_float_secs(dur: Duration) -> f64 {
    let secs = dur.as_secs();
    let nanos = dur.subsec_nanos();

    return secs as f64 + (nanos as f64 / 1_000_000_000.0 as f64);
}
