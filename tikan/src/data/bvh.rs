/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::data::{scene::EntityId, voxel_object::GpuVoxelObject};
use std::convert::TryInto;

///Specialized bvh that is created over the scene.
/// Can be turned into a linear dfs tree and used on the gpu, but also traced on the cpu via best-first-recursion.
///
///The Bvh is trimmed for construction speed, not quality since it is build on a per frame basis.
pub struct Bvh {
    root: Box<BvhNode>,
    num_nodes: u32,
}

impl Bvh {
    //Objects with (entity_id, index into the current gpu VoxelObject Buffer, min, max, morton_code)
    pub fn from_objects(objects: Vec<(EntityId, u32, [f32; 3], [f32; 3])>) -> Self {
        //Do not build an bvh if there are no nodes
        if objects.len() == 0 {
            return Bvh {
                root: Box::new(BvhNode {
                    left: None,
                    right: None,

                    index: 0,
                    object_index: None,
                    //If set this node references the given entity
                    entity_id: None,
                    //Worldspace min
                    min: [0.0; 3],
                    max: [0.0; 3],
                }),
                num_nodes: 0,
            };
        }

        //Calc morton code for each object and sort them according to the code.
        //Therefore objects that are next to each other in the vector are also next to each other in the real world.
        let mut objects_with_code: Vec<_> = objects
            .into_iter()
            .map(|(id, index, min, max)| {
                let center = [max[0] - min[0], max[1] - min[1], max[2] - min[2]];

                let code = unbound_morton(center);

                (id, index, min, max, code)
            })
            .collect();

        objects_with_code.sort_unstable_by(
            |(id1, index1, min1, max1, code1), (id2, index2, min2, max2, code2)| code1.cmp(code2),
        );

        let mut walking_index = 0;
        let root_node = BvhNode::from_objects(objects_with_code, &mut walking_index);

        Bvh {
            root: root_node,
            num_nodes: walking_index,
        }
    }

    pub fn num_nodes(&self) -> u32 {
        self.num_nodes
    }

    pub fn print(&self) {
        println!("BVH: num_nodes: {}", self.num_nodes);
        self.root.print(0);
    }

    pub fn to_flat(&self) -> Vec<GpuBvhNode> {
        let initial_skip_index = std::u32::MAX;
        let mut buffer = Vec::with_capacity(self.num_nodes as usize);

        self.root.to_flat(&mut buffer, initial_skip_index);
        buffer
    }
}

#[derive(Clone, Copy)]
pub struct GpuBvhNode {
    pub min: [f32; 4],
    pub max: [f32; 4],
    pub skip_idx: u32,
    pub is_leaf: u32,
    pub object_index: u32,
    pub pad: u32,
}

///represents a inner node on the bvh
struct BvhNode {
    left: Option<Box<BvhNode>>,
    right: Option<Box<BvhNode>>,

    index: u32,
    object_index: Option<u32>,
    //If set this node references the given entity
    entity_id: Option<EntityId>,
    //Worldspace min
    min: [f32; 3],
    max: [f32; 3],
}

impl BvhNode {
    fn from_objects(
        //Objects with (index into the current gpu VoxelObject Buffer, min, max, morton_code)
        mut objects: Vec<(EntityId, u32, [f32; 3], [f32; 3], u128)>, //Set of all objects
        index: &mut u32,                                             //Current index
    ) -> Box<Self> {
        let this_index = *index;
        *index += 1;

        //This is a leaf node, therefore do not recurse, but return node
        if objects.len() == 1 {
            return Box::new(BvhNode {
                left: None,
                right: None,
                index: this_index,
                entity_id: Some(objects.first().unwrap().0),
                object_index: Some(objects.first().unwrap().1),
                min: objects.first().unwrap().2,
                max: objects.first().unwrap().3,
            });
        }

        //Currently using some fake SHA in which we use the bounding boxes Surface area and try to blance the left and right tree as good as possible
        let whole_area = objects
            .iter()
            .fold(0.0, |before, (_eid, _objidx, min, max, _code)| {
                let extent = [max[0] - min[0], max[1] - min[1], max[2] - min[2]];
                let area = 2.0 * (extent[0] * extent[1])
                    + 2.0 * (extent[0] * extent[2])
                    + 2.0 * (extent[1] * extent[2]);
                before + area
            });

        //Find the index at which we have to split
        let mut split_index = 0;
        let mut current_area = 0.0;
        for (idx, (_id, _index, min, max, _code)) in objects.iter().enumerate() {
            split_index = idx;
            if current_area > (whole_area / 2.0) {
                break;
            }
            let extent = [max[0] - min[0], max[1] - min[1], max[2] - min[2]];
            let area = 2.0 * (extent[0] * extent[1])
                + 2.0 * (extent[0] * extent[2])
                + 2.0 * (extent[1] * extent[2]);
            current_area += area;
        }

        let back_part = objects.split_off(split_index);

        let (mut min, mut max) = ([std::f32::INFINITY; 3], [std::f32::NEG_INFINITY; 3]);
        //Generate left and right tree
        let left = if objects.len() > 0 {
            let left_tree = BvhNode::from_objects(objects, index);

            min[0] = min[0].min(left_tree.min[0]);
            min[1] = min[1].min(left_tree.min[1]);
            min[2] = min[2].min(left_tree.min[2]);

            max[0] = max[0].max(left_tree.max[0]);
            max[1] = max[1].max(left_tree.max[1]);
            max[2] = max[2].max(left_tree.max[2]);

            Some(left_tree)
        } else {
            None
        };

        let right = if back_part.len() > 0 {
            let right_tree = BvhNode::from_objects(back_part, index);

            min[0] = min[0].min(right_tree.min[0]);
            min[1] = min[1].min(right_tree.min[1]);
            min[2] = min[2].min(right_tree.min[2]);

            max[0] = max[0].max(right_tree.max[0]);
            max[1] = max[1].max(right_tree.max[1]);
            max[2] = max[2].max(right_tree.max[2]);

            Some(right_tree)
        } else {
            None
        };

        Box::new(BvhNode {
            left,
            right,
            index: this_index,
            entity_id: None,
            object_index: None,
            min,
            max,
        })
    }

    fn print(&self, level: usize) {
        for _l in 0..level {
            print!("   ");
        }

        print!(
            "{}: min: {:?}, max: {:?}, is_leaf: {}\n",
            level,
            self.min,
            self.max,
            self.entity_id.is_some()
        );

        if let Some(l) = &self.left {
            l.print(level + 1);
        }

        if let Some(r) = &self.right {
            r.print(level + 1);
        }
    }

    fn to_gpu_node(&self, skip_index: u32) -> GpuBvhNode {
        GpuBvhNode {
            min: [self.min[0], self.min[1], self.min[2], 1.0],
            max: [self.max[0], self.max[1], self.max[2], 1.0],
            skip_idx: skip_index,
            is_leaf: if self.object_index.is_some() { 1 } else { 0 },
            object_index: if let Some(i) = self.object_index {
                i
            } else {
                0
            }, //TODO get object index into GpuVoxelObject Vector somehow.
            pad: 0,
        }
    }

    fn to_flat(&self, buffer: &mut Vec<GpuBvhNode>, skip_index: u32) {
        assert!(
            buffer.len() == self.index.try_into().unwrap(),
            "Node index and BvhBuffer length did not match"
        );

        buffer.push(self.to_gpu_node(skip_index));

        //If there is a right child, this is the new skip index, otherwise
        //the old skip index is used.
        let lefts_skip_index = if let Some(right) = &self.right {
            right.index
        } else {
            skip_index
        };

        if let Some(left) = &self.left {
            left.to_flat(buffer, lefts_skip_index);
        }

        //Right node gets the old skip index anyways
        if let Some(right) = &self.right {
            right.to_flat(buffer, skip_index);
        }
    }
}

///Take a 32bit uint and expands it to an 128bit uint where each original has two zeros till the next one.
/// Example: original: 1234 -> _ _ 1 _ _ 2 _ _ 3 _ _ 4
// taken from [here](https://stackoverflow.com/questions/1024754/how-to-compute-a-3d-morton-number-interleave-the-bits-of-3-ints#1024889)
pub fn expand_bits_128(mut v: u32) -> u128 {
    v = v ^ (1 << 31);
    let mut x = v as u128;
    //Magic bit shifting... ending with x_ _ x _ _ x _ _ .. where each x is a original bit and each
    // _ is a zero
    x &= 0x3ffffffffff;
    x = (x | x << 64) & 0x3ff0000000000000000ffffffffu128;
    x = (x | x << 32) & 0x3ff00000000ffff00000000ffffu128;
    x = (x | x << 16) & 0x30000ff0000ff0000ff0000ff0000ffu128;
    x = (x | x << 8) & 0x300f00f00f00f00f00f00f00f00f00fu128;
    x = (x | x << 4) & 0x30c30c30c30c30c30c30c30c30c30c3u128;
    x = (x | x << 2) & 0x9249249249249249249249249249249u128;
    x
}

///takes a set of floating points as a 3d point in space and calculates the morton code from them in 128bit
// (actually 32*3 bit are used). The point can be within any region.
pub fn unbound_morton(point: [f32; 3]) -> u128 {
    //shift each axis so have a gap of two zeros between each bit
    let xx = expand_bits_128(point[0].to_bits());
    let yy = expand_bits_128(point[1].to_bits());
    let zz = expand_bits_128(point[2].to_bits());
    //now assemble
    return xx | (yy << 1) | (zz << 2);
}
