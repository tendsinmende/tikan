# Tikan-Engine
This is the main engine crate. While it can be used standalone, I'd highly recommend to use the editor for engine interaction, and if a application is build the client to start it.

Most function will be documented, so if you are extending the editor you can use this crates documentation to explore the API.

# Main modules
The engine contains two main modules: `render` and `object`.
The `render` contains all strictly rendering related sub modules like command buffer creation, ray-tracing related resources etc.
The `object` module contains all renderable object related sub-modules as well as scene management and bvh creation logic.

Both modules are build for concurrency, so you'll mostly see `Arc<T>` types since most of them need to be shared across threads.
