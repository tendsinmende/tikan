use nalgebra as na;
use tikan;
use tikan::object::voxel_object::*;

fn main() {
    let loc = na::Vector3::new(0.0, 0.0, 0.0);
    let r = 1.9;

    let vo = VoxelObject::from_func(0.5, [-2.0, -2.0, -2.0], [2.0, 2.0, 2.0], |coord| {
        let cord_vec = na::Vector3::new(coord[0], coord[1], coord[2]);
        let rad_vec = loc - cord_vec;

        if rad_vec.magnitude().abs() < r {
            Some(Voxel::new_leaf([1.0; 3], [0.0, 1.0, 0.0], 0.0, 1.0))
        } else {
            None
        }
    });

    println!("has {} voxels!", vo.num_voxels());
    let gpu = vo.to_gpu_voxel_object();

    vo.print();
    for (i, v) in gpu.into_iter().enumerate() {
        print!(" | [{}] {} ", i, v.skip_idx);
    }
    println!("|");

    println!(
        "Size: {} which is {} mod 16",
        std::mem::size_of::<GpuVoxel>(),
        std::mem::size_of::<GpuVoxel>() % 16
    );
}
