# Editor

The editor will provide some graphical tools to edit an application as well as level and assets.

## Design Ideas
- Async asset loading (as specially first time import and voxelisazion)
- When starting a debug play exchange parts of the editor gui with runtime information
- Blend editor debug objects into the final image via the depth image.
- Tab different editors similar to the godot engine.

## Planned editors
- Scene editor
- Material editor
- Object viewer/editor
- Script creation (will start a default text-editor)
- Composite editor (can combine several objects and object controllers into more complex objects)

## Current TODOs
- [ ] Implement messaging mechanism between editor and engine
- [ ] implement proper import tool into scene
- [ ] implement "clicking" of stuff in scene
- [ ] implement proper camera handling for the editor
- [ ] 
