/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */


use tikan::*;
use widkan;
use widkan::widget::*;

use std::sync::Arc;
///Handles the main interface of the editor
pub struct MainInterface{
    root_node: Arc<Widget + Send + Sync>,
    engine_widget: Arc<render::framebuffer_widget::FrambufferWidget>,
}

impl MainInterface{
    pub fn new(config: &widkan::config::Config) -> Self{

        let engine_widget = render::framebuffer_widget::FrambufferWidget::new(); 
        let root_tree = layout::LayoutBox::new(
            config,
            Some(
                layout::LayoutBox::new(
                    config,
                    Some(io::Button::new(
                        config,
                        io::ButtonContent::Text("Some Button you can press!".to_string()),
                        || println!("Some button was pressed!")
                    )),
                    Some(io::label::Label::new(
                        config,
                        "Some label!",
                        Some(32),
                        Some(Alignment::Center)
                    )),
                    layout::SplitType::VerticalMoveable(0.5)
                )
            ),
            Some(engine_widget.clone()),
            layout::SplitType::HorizontalFixedAtPixel(500.0)
        );

        MainInterface{
            root_node: root_tree,
            engine_widget,
        }
    }

    
    pub fn get_engine_widget(&self) -> Arc<render::framebuffer_widget::FrambufferWidget>{
        self.engine_widget.clone()
    }

    pub fn get_root(&self) -> Arc<Widget + Send + Sync>{
        self.root_node.clone()
    }
}
