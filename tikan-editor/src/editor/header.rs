/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::widkan::config::Config;
use crate::widkan::rendering::primitive::PrimitiveCall;
use crate::widkan::rendering::*;
use crate::widkan::widget::events::Event;
use crate::widkan::widget::*;

use crate::EngineConfig;

use tikan::message::{Messenger, RenderMessage};

use std::sync::Arc;

pub struct Header {
    ///Used to control the engine through messages
    messenger: Messenger,

    ///The inner widget that is rendered
    widget: Arc<HeaderWidget>,

    last_quality: f32,
    default_quality: f32,

    last_cone_start: f32,
    default_cone_start: f32,

    last_cone_length: f32,
    default_cone_length: f32,

    last_cone_min_aperture: f32,
    default_cone_min_aperture: f32,
}

impl Header {
    pub fn new(config: &Config, message_system: &Messenger, init_config: &EngineConfig) -> Header {
        //Sets rendering quality for the gbuffer
        let render_quality_w =
            io::NumberEdit::new(config, init_config.tracing_quality_threshold, None, None);

        let cone_start_w = io::NumberEdit::new(config, init_config.cone_start, None, None);

        let cone_max_dist_w = io::NumberEdit::new(config, init_config.cone_max_length, None, None);

        let cone_min_aperture_w =
            io::NumberEdit::new(config, init_config.cone_min_aperture, None, None);

        //Collects all items for the config sub field in layoutBoxes where each has a label to describe whats set and the pre-created item
        let config_list = layout::StaticList::new(
            config,
            vec![
                layout::LayoutBox::new(
                    config,
                    Some(io::Label::new(
                        config,
                        "Render Quality",
                        None,
                        Some(Alignment::UpLeft),
                    )),
                    Some(render_quality_w.clone()),
                    layout::SplitType::HorizontalFixed(layout::Offset::PixelLeft(150.0)),
                ),
                layout::LayoutBox::new(
                    config,
                    Some(io::Label::new(
                        config,
                        "Cone Start",
                        None,
                        Some(Alignment::UpLeft),
                    )),
                    Some(cone_start_w.clone()),
                    layout::SplitType::HorizontalFixed(layout::Offset::PixelLeft(150.0)),
                ),
                layout::LayoutBox::new(
                    config,
                    Some(io::Label::new(
                        config,
                        "Cone Max Length",
                        None,
                        Some(Alignment::UpLeft),
                    )),
                    Some(cone_max_dist_w.clone()),
                    layout::SplitType::HorizontalFixed(layout::Offset::PixelLeft(150.0)),
                ),
                layout::LayoutBox::new(
                    config,
                    Some(io::Label::new(
                        config,
                        "Cone Min Aperture",
                        None,
                        Some(Alignment::UpLeft),
                    )),
                    Some(cone_min_aperture_w.clone()),
                    layout::SplitType::HorizontalFixed(layout::Offset::PixelLeft(150.0)),
                ),
            ],
            layout::SLDirection::Horizontal,
        );

        //Collects the Headers top buttons and the fields for them
        let header_items: Vec<(String, Arc<dyn Widget + Send + Sync>)> = vec![
            (String::from("Config"), config_list),
            (
                String::from("Info"),
                io::Label::new(config, "Frames Per Second: ", None, Some(Alignment::UpLeft)),
            ),
            (
                String::from("Tools"),
                io::Label::new(
                    config,
                    "Gonna be some tools here",
                    None,
                    Some(Alignment::UpLeft),
                ),
            ),
        ];

        let widget = Arc::new(HeaderWidget {
            header_bar: layout::Bar::new(config, header_items),
            render_quality_w: render_quality_w.clone(),
            cone_start_w: cone_start_w.clone(),
            cone_max_length_w: cone_max_dist_w.clone(),
            cone_min_aperture_w: cone_min_aperture_w.clone(),
        });

        Header {
            messenger: message_system.clone(),
            widget,
            last_quality: render_quality_w
                .get_value()
                .expect("Failed to get last quality"),
            default_quality: render_quality_w
                .get_value()
                .expect("Failed to get default quality"),

            last_cone_start: cone_start_w
                .get_value()
                .expect("Failed to get last quality"),
            default_cone_start: cone_start_w
                .get_value()
                .expect("Failed to get last quality"),

            last_cone_length: cone_max_dist_w
                .get_value()
                .expect("Failed to get last quality"),
            default_cone_length: cone_max_dist_w
                .get_value()
                .expect("Failed to get last quality"),

            last_cone_min_aperture: cone_min_aperture_w
                .get_value()
                .expect("Failed to get last quality"),
            default_cone_min_aperture: cone_min_aperture_w
                .get_value()
                .expect("Failed to get last quality"),
        }
    }

    pub fn update(&mut self) {
        //TRACING QUALITY CHANGE
        let new_quality = if let Ok(new) = self.widget.render_quality_w.get_value() {
            new
        } else {
            self.default_quality
        };

        if new_quality != self.last_quality {
            if new_quality > 0.0 {
                self.last_quality = new_quality;
                self.messenger
                    .send_render_message(RenderMessage::ChangeRenderQuality(self.last_quality));
            }
        }

        let new_start = if let Ok(new) = self.widget.cone_start_w.get_value() {
            new
        } else {
            self.default_cone_start
        };

        if new_start != self.last_cone_start {
            if new_start > 0.0 {
                self.last_cone_start = new_start;
                self.messenger
                    .send_render_message(RenderMessage::ChangeConeStart(self.last_cone_start));
            }
        }

        let new_max_length = if let Ok(new) = self.widget.cone_max_length_w.get_value() {
            new
        } else {
            self.default_cone_length
        };

        if new_max_length != self.last_cone_length {
            if new_max_length > 0.0 {
                self.last_cone_length = new_max_length;
                self.messenger
                    .send_render_message(RenderMessage::ChangeConeMaxLength(self.last_cone_length));
            }
        }

        let new_min_aperture = if let Ok(new) = self.widget.cone_min_aperture_w.get_value() {
            new
        } else {
            self.default_cone_min_aperture
        };

        if new_min_aperture != self.last_cone_min_aperture {
            if new_min_aperture > 1.0 {
                self.last_cone_min_aperture = new_min_aperture;
                self.messenger
                    .send_render_message(RenderMessage::ChangeConeMinAperture(
                        self.last_cone_min_aperture,
                    ));
            }
        }
    }

    pub fn get_root_node(&self) -> Arc<dyn Widget + Send + Sync> {
        self.widget.header_bar.clone()
    }
}

pub struct HeaderWidget {
    ///The tree that is updated.
    header_bar: Arc<layout::Bar>,
    ///All control widgets
    render_quality_w: Arc<io::NumberEdit<f32>>,
    cone_start_w: Arc<io::NumberEdit<f32>>,
    cone_max_length_w: Arc<io::NumberEdit<f32>>,
    cone_min_aperture_w: Arc<io::NumberEdit<f32>>,
}

impl Widget for HeaderWidget {
    fn render(&self, renderer: &mut Renderer, level: u32) -> Vec<PrimitiveCall> {
        self.header_bar.render(renderer, level)
    }
    fn update(&self, new_area: Area, events: &Vec<Event>, renderer: &Renderer) -> UpdateRetState {
        self.header_bar.update(new_area, events, renderer)
    }
}
