/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use tikan::{message::Messenger, *};

use widkan;
use widkan::widget::*;

use crate::EngineConfig;

pub mod header;

use std::sync::Arc;
///Handles the main interface of the editor
pub struct Editor {
    root_node: Arc<dyn Widget + Send + Sync>,
    engine_framebuffer: Arc<render::framebuffer_widget::FrambufferWidget>,
    header: header::Header,
    resource_view: Arc<dyn Widget + Send + Sync>,
    scene_view: Arc<dyn Widget + Send + Sync>,
}

impl Editor {
    pub fn new(
        config: &widkan::config::Config,
        messanger: &Messenger,
        initial_config: &EngineConfig,
    ) -> Self {
        let engine_framebuffer = render::framebuffer_widget::FrambufferWidget::new();
        let header = header::Header::new(config, messanger, initial_config);
        let resource_view = io::label::Label::new(
            config,
            "This is going to be the resource of the engine",
            None,
            None,
        );

        let scene_view = io::label::Label::new(
            config,
            "This is going to be the scene_view of the engine",
            None,
            None,
        );

        //Split header from rest
        let root_tree = layout::LayoutBox::new(
            config,
            Some(header.get_root_node()),
            Some(
                //Split resource view from the left to the rest
                layout::LayoutBox::new(
                    config,
                    Some(resource_view.clone()),
                    Some(
                        //Split main middle view (currently only the engine widget), from the scene view to the left
                        layout::LayoutBox::new(
                            config,
                            Some(engine_framebuffer.clone()),
                            Some(scene_view.clone()),
                            layout::SplitType::HorizontalMoveable(layout::Offset::PixelRight(
                                250.0,
                            )),
                        ),
                    ),
                    layout::SplitType::HorizontalMoveable(layout::Offset::PixelLeft(250.0)),
                ),
            ),
            layout::SplitType::VerticalMoveable(layout::Offset::PixelLeft(100.0)),
        );

        Editor {
            root_node: root_tree,
            engine_framebuffer,
            header,
            resource_view,
            scene_view,
        }
    }

    pub fn get_engine_framebuffer(&self) -> Arc<render::framebuffer_widget::FrambufferWidget> {
        self.engine_framebuffer.clone()
    }

    pub fn get_root(&self) -> Arc<dyn Widget + Send + Sync> {
        self.root_node.clone()
    }

    ///Updates the editor. For instance sends all commands made to the engine etc.
    pub fn update(&mut self) {
        self.header.update();
    }
}
