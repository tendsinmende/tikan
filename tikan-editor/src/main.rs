/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use tikan::data::{
    data_manager::DataManager,
    triangle_object::MeshId,
    triangle_object::Vertex,
    triangle_object::VertexMeshBuilder,
    voxel_object::{Voxel, VoxelObject},
};
use tikan::marp::ash::vk;
use tikan::render::debug_pass::DebugDrawType;
use tikan::widkan;
use tikan::*;
use tikan_converter::*;

use tikan::config::Config as EngineConfig;

extern crate marp_surface_winit;
use marp_surface_winit::winit;
use winit::{
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

pub mod editor;

use std::sync::Arc;

fn main() {
    //Create the main window and interface.
    let mut events_loop = winit::event_loop::EventLoop::new();
    let window = Arc::new(
        winit::window::WindowBuilder::new()
            .with_title("Tikan-Editor")
            .build(&events_loop)
            .expect("TIKAN-EDITOR: ERROR: Could not open editor window!"),
    );

    let mut engine_builder = EngineBuilder::new(Some("engine.ron"))
        .expect("TIKAN-EDITOR: ERROR: Could not create engine builder");

    //Create the surface we use for the engine based on this window
    let surface =
        marp_surface_winit::WinitSurface::new(engine_builder.get_instance(), &window, &events_loop)
            .expect("TIKAN-EDITOR: ERROR: Failed to create swapchain surface!");

    let window_extent = window.inner_size();

    //Save initial config to load right interface values
    let engine_config = engine_builder.get_config().clone();

    //Now start the engine
    let mut engine = engine_builder
        .build(window.clone(), surface)
        .expect("TIKAN-EDITOR: ERROR: Could not create engine");

    //Start engine and get messenger system
    let messanger_system = engine.start_engine(None);

    //Create widkan interface context
    let interface = widkan::interface::Interface::new(
        engine.device(),
        engine.graphics_queue(),
        (window_extent.width as f64, window_extent.height as f64),
        "editor_interface_config.ron",
        vk::Format::B8G8R8A8_UNORM,
    );

    //Create the editor based on the interface and the messanger system
    let mut editor =
        editor::Editor::new(&interface.get_config(), &messanger_system, &engine_config);
    //Build the interface info for the engine
    let interface_info = render::InterfaceInfo {
        interface: interface.clone(),
        target_widget: editor.get_engine_framebuffer(),
    };
    interface.set_root(editor.get_root());

    //Finally, supply the editor interface to the engine.
    //NOTE: This will be updated in the near future to some other toolkit, since widkan is more
    // or less garbage.
    engine.set_interface(interface_info);

    //DEBUG-----------------------------------------------------
    //Add a debug mesh to the object manager.
    //Then register the returned id within our scene.

    let sphere_radius = 16;

    let sphere_id = std::sync::Arc::new(std::sync::RwLock::new(0));
    let inner_id = sphere_id.clone();
    messanger_system.send_obj_message(ObjectMessage::Job(Box::new(move |om| {
        let loc = na::Vector3::new(
            sphere_radius as f32,
            sphere_radius as f32,
            sphere_radius as f32,
        );

        let min = -0.1;
        let max = 0.1;

        /* This was building a triangle debug boundingbox around the sphere some time ago, but is currently not supported ... I think
            let mesh_id = om.add_triangle_mesh(
                VertexMeshBuilder{
            vertices: vec![
                Vertex{pos: [min, min, min, 1.0]},
                Vertex{pos: [min, min, max, 1.0]},
                Vertex{pos: [max, min, min, 1.0]},
                Vertex{pos: [max, min, max, 1.0]},

                Vertex{pos: [min, max, min, 1.0]},
                Vertex{pos: [min, max, max, 1.0]},
                Vertex{pos: [max, max, min, 1.0]},
                Vertex{pos: [max, max, max, 1.0]},
            ],
            indices: vec![
                0,1,
                0,2,
                2,3,
                3,1,
                1,5,
                3,7,
                2,6,
                0,4,
                4,5,
                4,6,
                5,7,
                6,7
            ],
            draw_type: DebugDrawType::Lines([1.0, 1.0, 0.0, 1.0]),
            }
        );
            */

        //At this point create a procedural sphere via VoxelObject::from_func()
        let block_id = om.get_scene_mut().new_entity("SphereBoundingBox");
        *inner_id.write().expect("Failed to set sphere id") = block_id;

        let mut transform = Transform::identity();
        transform.add_translation(na::Vector3::new(1.5, 1.0, 0.0));
        //transform.add_rotation(&na::UnitQuaternion::from_euler_angles(10.0,1.0,2.0));
        //transform.add_scaling(na::Vector3::new(4.0,4.0,4.0));

        let voxel_object = VoxelObject::from_func(
            2 * sphere_radius,
            [min, min, min],
            [max, max, max],
            |coord| {
                let cord_vec = na::Vector3::new(coord[0] as f32, coord[1] as f32, coord[2] as f32);
                let center_to_coord = cord_vec - loc;

                let normalized = if center_to_coord.x == 0.0
                    || center_to_coord.y == 0.0
                    || center_to_coord.z == 0.0
                {
                    let mut nor = center_to_coord;
                    nor.x = center_to_coord.x.min(1.0).max(-1.0);
                    nor.y = center_to_coord.y.min(1.0).max(-1.0);
                    nor.z = center_to_coord.z.min(1.0).max(-1.0);
                    nor
                } else {
                    center_to_coord.normalize()
                };

                if center_to_coord.magnitude().abs() < (sphere_radius as f32) {
                    Some(Voxel::new_leaf(
                        [1.0, 1.0, 1.0, 1.0], //Albedo
                        [5_000.0, 4_500.0, 4_500.0],
                        normalized.into(), //normal
                        0.0,               //Mirror like surface
                        0.0,               //Metal
                    ))
                } else {
                    None
                }
            },
        );

        println!("Sphere has {} voxels...", voxel_object.num_voxels());
        //Add sphere to scene
        let vox_id = om.add_voxel_object(voxel_object);

        //om.get_scene_mut().set_triangle_mesh(&block_id, mesh_id);
        om.get_scene_mut().set_transform(&block_id, transform);
        om.get_scene_mut().set_voxel_object(&block_id, vox_id);

        //Setup array of spheres on the ground
        let height = 2.0;
        let radius = 10.0;
        let locations = [
            //("1", na::Vector3::new(-5.0, height, -radius)),
            //("2", na::Vector3::new(-2.5, height, -radius)),
            ("3", na::Vector3::new(0.0, height, -radius)),
            //("4", na::Vector3::new(2.5, height, -radius)),
            //("5", na::Vector3::new(5.0, height, -radius)),

            //("6", na::Vector3::new(-5.0, height, radius)),
            //("7", na::Vector3::new(-2.5, height, radius)),
            ("8", na::Vector3::new(0.0, height, radius)),
            //("9", na::Vector3::new(2.5, height, radius)),
            //("10", na::Vector3::new(5.0, height, radius)),
        ];
        for (name, loc) in &locations {
            let id = om.get_scene_mut().new_entity(name);
            let mut transform = Transform::identity();
            transform.add_translation(*loc);
            om.get_scene_mut().set_transform(&id, transform);
            om.get_scene_mut().set_voxel_object(&id, vox_id);
        }
    })));

    {
        //Load scene from file and place into the scene as well
        let mut transform = Transform::identity();
        //transform.add_translation(na::Vector3::new(2.5,0.0,3.0));
        transform.add_scaling(na::Vector3::new(1.0, 1.0, 1.0));

        let voxel_object = format::load_from_file("Gate.bin").expect("Failed to load Gate!");

        messanger_system.send_obj_message(ObjectMessage::Job(Box::new(move |om: &DataManager| {
            let vox_id = om.add_voxel_object(voxel_object);

            let gate_id = om.get_scene_mut().new_entity("Gate1");
            om.get_scene_mut().set_transform(&gate_id, transform);
            om.get_scene_mut().set_voxel_object(&gate_id, vox_id);
        })));
    }

    //DEBUG END -----------------------------------------------
    let mut cursor_pos = (0.0, 0.0);

    let mut start_time = std::time::Instant::now();

    println!("Starting event loop");
    //Start the event loop
    events_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        //Setup job to move sphere, could also be done through some server, but whatever.
        let t = start_time.elapsed().as_secs_f32();
        let inner_id = sphere_id.clone();
        messanger_system.send_obj_message(ObjectMessage::Job(Box::new(move |om: &DataManager| {
            let mut trans = Transform::identity();
            trans.add_translation(na::Vector3::new(t.sin() * 1.5, 1.0, 0.0));
            om.get_scene_mut()
                .set_transform(&inner_id.read().expect("Failed to read sphere id"), trans);
        })));

        //Check the event for close event or escape, in this case, tell the engine to stopp and emit a exit flow command
        match event {
            winit::event::Event::WindowEvent {
                event: winit::event::WindowEvent::CloseRequested,
                window_id: _,
            } => {
                engine.end_engine();
                *control_flow = ControlFlow::Exit;
            }
            winit::event::Event::WindowEvent {
                event:
                    winit::event::WindowEvent::KeyboardInput {
                        device_id: _,
                        input:
                            winit::event::KeyboardInput {
                                scancode: _,
                                state: _,
                                virtual_keycode: Some(winit::event::VirtualKeyCode::Escape),
                                modifiers: _,
                            },
                        is_synthetic: _,
                    },
                window_id: _,
            } => {
                engine.end_engine();
                *control_flow = ControlFlow::Exit;
            }
            winit::event::Event::WindowEvent {
                event: winit::event::WindowEvent::Resized(_new),
                window_id: _,
            } => {
                println!("Had a resize event!");
            }
            _ => {}
        }

        //Update the widkan interface of the editor with the newest event
        let (upd_events, new_cursor_pos) =
            widkan::widget::events::to_events(cursor_pos, &event, &window);

        cursor_pos = new_cursor_pos;
        interface.update(&upd_events, engine.graphics_queue());

        editor.update();
        if let Some(static_ev) = event.to_static() {
            engine.handle_events(vec![static_ev]);
        }
    });
}
