/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! The ModelScene is a simple representation of an loaded triangle scene.
//! It contains all triangles correctly transformed as well as all texture resources as well as pointers
//! between a triangle and its textures.

use tikan::data::voxel_object::*;
use voxer::octree::SVO;

use crate::gltf_import::to_image;
use image::GenericImage;
use image::ImageFormat;
use image::RgbaImage;
use image::{load_from_memory_with_format, open};

use std::collections::HashMap;
use std::convert::TryInto;
use std::sync::mpsc::*;
use std::sync::{Arc, RwLock};
use std::thread::JoinHandle;

use gltf;
use gltf::texture::Texture;

use na;

use rand;
use rand::Rng;
use rand_distr;
use rand_distr::Distribution;

use bvh::bounding_hierarchy::BHShape;

use rayon::prelude::*;

use voxer;

///Represents a really small value used when calculating ray->triangle intersections
pub const EPSILON: f32 = 0.000001;

pub struct Material {
    pub alpha_cutoff: f32,
    pub base_color_factor: [f32; 4],
    ///index into model_scenes loaded texturs.
    ///originally the jsons texture index.
    pub base_color_texture: Option<usize>,
    pub metallic_factor: f32,
    pub roughness_factor: f32,
    pub metallic_roughness_texture: Option<usize>,
    pub normal_texture: Option<usize>,
    pub occlusion_texture: Option<usize>,
    pub emissive_texture: Option<usize>,
    pub emissive_factor: [f32; 3],
}

pub struct Vertex {
    pub position: na::Point3<f32>,
    pub uv_coord: na::Vector2<f32>,
    pub normal: na::Vector3<f32>,
    pub tangent: na::Vector4<f32>,
    pub color: na::Vector4<f32>,
}

pub struct Face {
    pub indices: [u32; 3],
    pub bounding_box: bvh::aabb::AABB,
    pub bvh_index: usize,
}

impl bvh::aabb::Bounded for Face {
    fn aabb(&self) -> bvh::aabb::AABB {
        self.bounding_box
    }
}

impl BHShape for Face {
    fn set_bh_node_index(&mut self, a: usize) {
        self.bvh_index = a;
    }
    fn bh_node_index(&self) -> usize {
        self.bvh_index
    }
}

pub struct Object {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,
    pub faces: Vec<Face>,
    ///Bvh for this node, used to intersect.
    pub bvh: bvh::bvh::BVH,
    pub material: Material,
    ///The worldspace min extent of this object
    pub mins: na::Vector3<f32>,
    ///The worldspace max extent of this object
    pub maxs: na::Vector3<f32>,
}

fn sample(image: &RgbaImage, u: f32, v: f32) -> [f32; 4] {
    let u = if u > 1.0 {
        u.fract()
    } else if u < 0.0 {
        1.0 - u.fract().abs()
    } else {
        u
    };

    let v = if v > 1.0 {
        v.fract()
    } else if v < 0.0 {
        1.0 - v.fract().abs()
    } else {
        v
    };

    assert!(
        u >= 0.0 && u <= 1.0 && v >= 0.0 && v <= 1.0,
        "uv, not in 0,1: {},{}",
        u,
        v
    );

    let mut x = ((image.width()) as f32 * u).floor() as u32;
    let mut y = ((image.height()) as f32 * v).floor() as u32;

    if x >= image.width() {
        x = x % image.width();
    }

    if y >= image.height() {
        y = y % image.height();
    }

    let pixel = image.get_pixel(x, y);

    //Map to float rgba color
    [
        pixel.0[0] as f32 / (std::u8::MAX as f32),
        pixel.0[1] as f32 / (std::u8::MAX as f32),
        pixel.0[2] as f32 / (std::u8::MAX as f32),
        pixel.0[3] as f32 / (std::u8::MAX as f32),
    ]
}

///Interpolates data from the three vertices u,v,w by interpolating based on the weights of u/v
fn interpolate(vu: [f32; 3], vv: [f32; 3], vw: [f32; 3], u: f32, v: f32) -> [f32; 3] {
    [
        vu[0] * (1.0 - u - v) + vv[0] * u + vw[0] * v,
        vu[1] * (1.0 - u - v) + vv[1] * u + vw[1] * v,
        vu[2] * (1.0 - u - v) + vv[2] * u + vw[2] * v,
    ]
}

fn interpolate_uv(vu: [f32; 2], vv: [f32; 2], vw: [f32; 2], u: f32, v: f32) -> (f32, f32) {
    (
        vu[0] * (1.0 - u - v) + vv[0] * u + vw[0] * v,
        vu[1] * (1.0 - u - v) + vv[1] * u + vw[1] * v,
    )
}

trait ToVec<T>
where
    T: Copy + PartialEq + std::fmt::Debug + 'static,
{
    fn to_vec(&self) -> na::Vector3<T>;
}

impl ToVec<f32> for na::Point3<f32> {
    fn to_vec(&self) -> na::Vector3<f32> {
        na::Vector3::new(self.x, self.y, self.z)
    }
}

/// Intersects the triangle returns the triple of (u,v,t) where u/v are the weights to v0/v1 and t is the distance on the ray
/// at which the intersection occurred.
fn moller_trumbore(
    direction: &na::Vector3<f32>,
    origin: &na::Vector3<f32>,
    v0: &Vertex,
    v1: &Vertex,
    v2: &Vertex,
) -> Option<(f32, f32, f32)> {
    let edge1 = v1.position.to_vec() - v0.position.to_vec();
    let edge2 = v2.position.to_vec() - v0.position.to_vec();

    let h = direction.cross(&edge2);
    let a = edge1.dot(&h);
    if a.abs() < EPSILON {
        return None; //Ra is parallel to plane
    }

    let f = 1.0 / a;
    let s = origin - v0.position.to_vec();

    let u = f * s.dot(&h);
    if u < 0.0 || u > 1.0 {
        return None;
    }

    let q = s.cross(&edge1);
    let v = f * direction.dot(&q);

    if v < 0.0 || (u + v) > 1.0 {
        return None;
    }

    let t = f * edge2.dot(&q);
    if t > EPSILON && t < (1.0 / EPSILON) {
        Some((u, v, t))
    } else {
        None
    }
}

///Intersects the triangle with the rays, if there is an intersection it calculates the voxel representing the intersection.
///You can supply several origins to cast several rays at this triangle.
fn intersect_triangle(
    origin: &na::Vector3<f32>,
    direction: &na::Vector3<f32>,
    v0: &Vertex,
    v1: &Vertex,
    v2: &Vertex,
    model_scene: &ModelScene,
    object: &Object,
) -> Option<(f32, Voxel)> {
    let (u, v, t) = if let Some(f) = moller_trumbore(direction, origin, v0, v1, v2) {
        f
    } else {
        return None;
    };

    //Get texture coords
    let (su, sv) = interpolate_uv(
        v0.uv_coord.into(),
        v1.uv_coord.into(),
        v2.uv_coord.into(),
        u,
        v,
    );

    //Since we came here, there is an valid intersection, we can us the u/v to generate the voxel data by interpolating the values between the three vertexes
    let pos = interpolate(
        [
            (v0.position.x + 1.0) / 2.0,
            (v0.position.y + 1.0) / 2.0,
            (v0.position.z + 1.0) / 2.0,
        ],
        [
            (v1.position.x + 1.0) / 2.0,
            (v1.position.y + 1.0) / 2.0,
            (v1.position.z + 1.0) / 2.0,
        ],
        [
            (v2.position.x + 1.0) / 2.0,
            (v2.position.y + 1.0) / 2.0,
            (v2.position.z + 1.0) / 2.0,
        ],
        u,
        v,
    );
    //find the base color of this voxel
    let mut base_color = if let Some(tex_id) = object.material.base_color_texture {
        let texture = model_scene
            .get_texture(tex_id)
            .expect("Failed to get base texture for object!");
        let color = sample(texture, su, sv);
        //Integrate factor into texture
        [
            color[0] * object.material.base_color_factor[0],
            color[1] * object.material.base_color_factor[1],
            color[2] * object.material.base_color_factor[2],
            color[3] * object.material.base_color_factor[3],
        ]
    } else {
        object.material.base_color_factor
    };

    base_color[0] = base_color[0].max(0.0).min(1.0);
    base_color[1] = base_color[1].max(0.0).min(1.0);
    base_color[2] = base_color[2].max(0.0).min(1.0);
    base_color[3] = base_color[3].max(0.0).min(1.0);

    assert!(base_color[3] <= 1.0, "Alpha too high!");

    if base_color[3] <= 0.0 {
        //We have a transparent voxel, therefore omit it
        return None;
    }

    //Find normal values
    let mut normal = if let Some(tex_id) = object.material.normal_texture {
        //Got a normal texture, therefore got to create a tbn matrix to translate vectors into model space
        let texture = model_scene
            .get_texture(tex_id)
            .expect("Failed to get normal texture for object!");
        let normals = sample(texture, su, sv);

        let normal_texture = [normals[0], normals[1], normals[2]];
        //Now transform surface normals into model space
        //Map to [-1, 1] range
        let normal = (na::Vector3::new(
            normal_texture[0] * 2.0 - 1.0,
            normal_texture[1] * 2.0 - 1.0,
            normal_texture[2] * 2.0 - 1.0,
        ))
        .normalize();

        //Now find the surface normal and tangent, then calculate TBN matrix to translate the textures normals into model space
        let surf_normal: na::Vector3<f32> = na::Vector3::from(interpolate(
            v0.normal.into(),
            v1.normal.into(),
            v2.normal.into(),
            u,
            v,
        ));

        let surf_tangent: na::Vector3<f32> = na::Vector3::from(interpolate(
            [v0.tangent.x, v0.tangent.y, v0.tangent.z],
            [v1.tangent.x, v1.tangent.y, v1.tangent.z],
            [v2.tangent.x, v2.tangent.y, v2.tangent.z],
            u,
            v,
        ));

        let surf_bitangent = surf_normal.cross(&surf_tangent);

        let tbn = na::Matrix3::from_columns(&[surf_tangent, surf_bitangent, surf_normal]);

        //Finally transform normal into model space
        (tbn * normal).normalize()
    } else {
        //Since we have no normal texture, just use the surface normal values
        na::Vector3::from(interpolate(
            v0.normal.into(),
            v1.normal.into(),
            v2.normal.into(),
            u,
            v,
        ))
    };

    normal[0] = normal[0].max(-1.0).min(1.0);
    normal[1] = normal[1].max(-1.0).min(1.0);
    normal[2] = normal[2].max(-1.0).min(1.0);

    assert!(
        normal[0] <= 1.0 && normal[0] >= -1.0,
        "Normal x was {}",
        normal[0]
    );
    assert!(
        normal[1] <= 1.0 && normal[1] >= -1.0,
        "Normal y was {}",
        normal[1]
    );
    assert!(
        normal[2] <= 1.0 && normal[2] >= -1.0,
        "Normal z was {}",
        normal[2]
    );

    //Last but not least, calculate the roughness and metallness value from the given material and uvs
    let (metallic, roughness) = if let Some(tex_id) = object.material.metallic_roughness_texture {
        let texture = model_scene
            .get_texture(tex_id)
            .expect("Failed to get metallic roughness texture for object!");
        let metallic_roughhness = sample(texture, su, sv);

        (
            metallic_roughhness[2] * object.material.metallic_factor,
            metallic_roughhness[1] * object.material.roughness_factor,
        )
    } else {
        (
            object.material.metallic_factor,
            object.material.roughness_factor,
        )
    };

    let emission = if let Some(tex_id) = object.material.emissive_texture {
        let texture = model_scene
            .get_texture(tex_id)
            .expect("Could not get emission texture!");
        let emission = sample(texture, su, sv);

        [
            emission[0] * object.material.emissive_factor[0],
            emission[1] * object.material.emissive_factor[1],
            emission[2] * object.material.emissive_factor[2],
        ]
    } else {
        [
            object.material.emissive_factor[0],
            object.material.emissive_factor[1],
            object.material.emissive_factor[2],
        ]
    };

    let voxel = Voxel::new_leaf(
        [base_color[0], base_color[1], base_color[2], base_color[3]],
        emission,
        [normal.x, normal.y, normal.z],
        roughness,
        metallic,
    );

    Some((t, voxel))
}

impl Object {
    ///Intersects a ray returns for each calculation the way `t` along the ray at which the intersection happned
    /// as well as the Voxel data.
    /// **WARNING**: The mins/maxs of the voxel are set to 0.0 since there is no information where in a voxel object
    /// it will be placed.
    fn ray_intersect(
        &self,
        origins: &[na::Vector3<f32>],
        voxelization_direction: &VoxelizingDirection,
        direction: na::Vector3<f32>,
        model_scene: &ModelScene,
        voxel_size: f32,
    ) -> Vec<(f32, Voxel)> {
        //early test object bounding box against ray, since the direction is always on x we can just test if the origin
        //x/y is within the bounding box x/y

        let mut intersecting_origins = Vec::new();
        for origin in origins.iter() {
            if voxelization_direction.origin_intersects(*origin, self.mins, self.maxs) {
                intersecting_origins.push(*origin);
            }
        }
        let mut intersections = Vec::new();

        for origin in origins {
            let ray = bvh::ray::Ray::new(
                bvh::nalgebra::Point3::new(origin.x, origin.y, origin.z),
                bvh::nalgebra::Vector3::new(direction.x, direction.y, direction.z),
            );

            let possible_faces = self.bvh.traverse(&ray, &self.faces);

            let mut ray_intersections = Vec::new();
            for face in possible_faces {
                if let Some(i) = intersect_triangle(
                    &origin,
                    &direction,
                    &self.vertices[face.indices[0] as usize],
                    &self.vertices[face.indices[1] as usize],
                    &self.vertices[face.indices[2] as usize],
                    model_scene,
                    self,
                ) {
                    ray_intersections.push(i);
                }
            }

            intersections.append(&mut ray_intersections);
        }

        intersections
    }

    ///returns a voxel which contains the data of the intersection at origin + t*direction where t is the first tuple element.
    fn get_best_intersection(
        &self,
        origin: na::Vector3<f32>,
        direction: na::Vector3<f32>,
        model_scene: &ModelScene,
    ) -> Option<(f32, Voxel)> {
        let ray = bvh::ray::Ray::new(
            bvh::nalgebra::Point3::new(origin.x, origin.y, origin.z),
            bvh::nalgebra::Vector3::new(direction.x, direction.y, direction.z),
        );

        let faces = self.bvh.traverse(&ray, &self.faces);

        if faces.len() <= 0 {
            return None;
        }

        let mut ray_intersections = Vec::new();
        for face in faces.into_iter() {
            if let Some(i) = intersect_triangle(
                &origin,
                &direction,
                &self.vertices[face.indices[0] as usize],
                &self.vertices[face.indices[1] as usize],
                &self.vertices[face.indices[2] as usize],
                model_scene,
                self,
            ) {
                ray_intersections.push(i);
            }
        }
        if ray_intersections.len() <= 0 {
            return None;
        }
        //Sort and return the best
        ray_intersections.sort_unstable_by(|(at, av), (bt, bv)| at.partial_cmp(bt).unwrap());
        Some(ray_intersections[0].clone())
    }
}

pub enum VoxelizingDirection {
    ///Ray goes from the y/z plane in x direction
    X,
    ///Ray goes from the x/z plane in y direction
    Y,
    ///Ray goes from the x/y plane in z direction
    Z,
}

impl VoxelizingDirection {
    fn origin_intersects(
        &self,
        origin: na::Vector3<f32>,
        mins: na::Vector3<f32>,
        maxs: na::Vector3<f32>,
    ) -> bool {
        match self {
            VoxelizingDirection::X => {
                if origin.y < mins.y || origin.z < mins.z || origin.y > maxs.y || origin.z > maxs.z
                {
                    false
                } else {
                    true
                }
            }
            VoxelizingDirection::Y => {
                if origin.x < mins.x || origin.z < mins.z || origin.x > maxs.x || origin.z > maxs.z
                {
                    false
                } else {
                    true
                }
            }
            VoxelizingDirection::Z => {
                if origin.x < mins.x || origin.y < mins.y || origin.x > maxs.x || origin.y > maxs.y
                {
                    false
                } else {
                    true
                }
            }
        }
    }

    fn to_direction_vector(&self) -> na::Vector3<f32> {
        match self {
            VoxelizingDirection::X => na::Vector3::new(1.0, 0.0, 0.0),
            VoxelizingDirection::Y => na::Vector3::new(0.0, 1.0, 0.0),
            VoxelizingDirection::Z => na::Vector3::new(0.0, 0.0, 1.0),
        }
    }

    fn origin_for_step(
        &self,
        a: usize,
        b: usize,
        from: na::Vector3<f32>,
        voxel_size: f32,
    ) -> na::Vector3<f32> {
        match self {
            VoxelizingDirection::X => {
                (from + na::Vector3::new(0.0, a as f32 * voxel_size, b as f32 * voxel_size))
            }
            VoxelizingDirection::Y => {
                (from + na::Vector3::new(a as f32 * voxel_size, 0.0, b as f32 * voxel_size))
            }
            VoxelizingDirection::Z => {
                (from + na::Vector3::new(a as f32 * voxel_size, b as f32 * voxel_size, 0.0))
            }
        }
    }

    //Calculates a random origin offset, used for creating additional sampling origins
    pub fn random_origin_offsets(
        &self,
        num_offsets: usize,
        voxel_size: f32,
    ) -> Vec<na::Vector3<f32>> {
        let poi = rand_distr::Poisson::new(2.0).unwrap();
        let mut generator = rand::thread_rng();

        let offsets: Vec<_> = (0..num_offsets)
            .into_iter()
            .map(|i| {
                /*
                let a: f32 = poi.sample(&mut generator);
                let b: f32 = poi.sample(&mut generator);
                let c: f32 = poi.sample(&mut generator);
                */
                let a: f32 = generator.gen();
                let b: f32 = generator.gen();
                let c: f32 = generator.gen();

                na::Vector3::new(a * voxel_size, b * voxel_size, c * voxel_size)
            })
            .collect();

        match self {
            VoxelizingDirection::X => offsets
                .into_iter()
                .map(|o| na::Vector3::new(0.0, o.y, o.z))
                .collect(),
            VoxelizingDirection::Y => offsets
                .into_iter()
                .map(|o| na::Vector3::new(o.x, 0.0, o.z))
                .collect(),
            VoxelizingDirection::Z => offsets
                .into_iter()
                .map(|o| na::Vector3::new(o.x, o.y, 0.0))
                .collect(),
        }
    }

    fn insert_voxels(
        &self,
        bottom_buffer: &mut HashMap<[usize; 3], Voxel>,
        intersections: HashMap<usize, Vec<Voxel>>,
        steps: usize,
        a: usize,
        b: usize,
    ) {
        match self {
            VoxelizingDirection::X => {
                for (x, mut voxels) in intersections.into_iter() {
                    let voxel = if voxels.len() > 1 {
                        Voxel::interpolate(&voxels)
                    } else if voxels.len() == 1 {
                        voxels.remove(0)
                    } else {
                        //There is no voxel... shouldn't happen tho, but save is save :D
                        continue;
                    };

                    if let Some(ref mut inner_voxel) = bottom_buffer.get_mut(&[x, a, b]) {
                        inner_voxel.interpolate_in_place(&vec![voxel]);
                    } else {
                        bottom_buffer.insert([x, a, b], voxel);
                    }
                }
            }
            VoxelizingDirection::Y => {
                for (y, mut voxels) in intersections.into_iter() {
                    let voxel = if voxels.len() > 1 {
                        Voxel::interpolate(&voxels)
                    } else if voxels.len() == 1 {
                        voxels.remove(0)
                    } else {
                        //There is no voxel... shouldn't happen tho, but save is save :D
                        continue;
                    };

                    if let Some(ref mut inner_voxel) = bottom_buffer.get_mut(&[a, y, b]) {
                        inner_voxel.interpolate_in_place(&vec![voxel]);
                    } else {
                        bottom_buffer.insert([a, y, b], voxel);
                    }
                }
            }
            VoxelizingDirection::Z => {
                for (z, mut voxels) in intersections.into_iter() {
                    let voxel = if voxels.len() > 1 {
                        Voxel::interpolate(&voxels)
                    } else if voxels.len() == 1 {
                        voxels.remove(0)
                    } else {
                        //There is no voxel... shouldn't happen tho, but save is save :D
                        continue;
                    };

                    if let Some(ref mut inner_voxel) = bottom_buffer.get_mut(&[a, b, z]) {
                        inner_voxel.interpolate_in_place(&vec![voxel]);
                    } else {
                        bottom_buffer.insert([a, b, z], voxel);
                    }
                }
            }
        }
    }
}

pub trait Voxelizer {
    fn voxelize(
        &self,
        resolution: u32,
        num_samples: usize,
        compression: f32,
        solidify: bool,
    ) -> (usize, VoxelObject);
    fn voxelize_in_direction(
        &self,
        direction: VoxelizingDirection,
        from: na::Vector3<f32>,
        voxel_size: f32,
        num_samples: usize,
        steps: usize,
    ) -> (JoinHandle<HashMap<[usize; 3], Voxel>>, Receiver<f32>);
}

pub struct ModelScene {
    objects: Vec<Object>,
    //Currently just saves the image, might later take the given sampler into account.
    textures: HashMap<usize, RgbaImage>,
    gltf_file_directory: String,
}

impl ModelScene {
    pub fn new(file_path: String) -> Self {
        let path = String::from(
            std::path::Path::new(&file_path)
                .parent()
                .expect("No Parent directory for gltf file!")
                .to_str()
                .expect("Could not generate path as string!"),
        );

        ModelScene {
            objects: Vec::new(),
            textures: HashMap::new(),
            gltf_file_directory: path,
        }
    }

    pub fn get_texture(&self, key: usize) -> Option<&RgbaImage> {
        self.textures.get(&key)
    }

    pub fn add_object(&mut self, object: Object) {
        self.objects.push(object);
    }

    ///Adds this texture to the scenes storage if it wasn't added before. Return the index under which it can be referenced.
    pub fn add_texture(&mut self, texture: Texture, buffers: &[gltf::buffer::Data]) -> usize {
        if !self.textures.contains_key(&texture.index()) {
            //Since the texture hasn't been loaded yet, add it
            let image = match texture.source().source() {
                gltf::image::Source::Uri { uri, mime_type: _ } => {
                    //Load from path
                    //println!("Loading texture from: {}", self.gltf_file_directory.clone() + "/" + uri);
                    open(self.gltf_file_directory.clone() + "/" + uri).expect(&format!(
                        "Failed to load image from path: {}",
                        self.gltf_file_directory.clone() + "/" + uri
                    ))
                }
                gltf::image::Source::View { view, mime_type } => {
                    let image_data = to_image(&view, buffers)
                        .expect("Failed to load image data from internal buffer!");
                    let format = format_from_str(mime_type);

                    let image = load_from_memory_with_format(&image_data, format)
                        .expect("Failed to load image from buffer!");
                    image
                }
            };

            let _was_used = self.textures.insert(texture.index(), image.to_rgba());
            texture.index()
        } else {
            texture.index()
        }
    }
}

impl Voxelizer for Arc<ModelScene> {
    fn voxelize(
        &self,
        resolution: u32,
        num_samples: usize,
        compression: f32,
        solidify: bool,
    ) -> (usize, VoxelObject) {
        //Step one is currently implemented naively by just intersecting each voxel against the whole scene. This should be accelerated later.
        //First find the min and max dimensions of this scene and find a fitting buffer size for the bottom level.
        let mut mins = na::Vector3::new(std::f32::MAX, std::f32::MAX, std::f32::MAX);
        let mut maxs = na::Vector3::new(std::f32::MIN, std::f32::MIN, std::f32::MIN);

        for model in self.objects.iter() {
            mins.x = mins.x.min(model.mins.x);
            mins.y = mins.y.min(model.mins.y);
            mins.z = mins.z.min(model.mins.z);

            maxs.x = maxs.x.max(model.maxs.x);
            maxs.y = maxs.y.max(model.maxs.y);
            maxs.z = maxs.z.max(model.maxs.z);
        }

        let extent = maxs - mins;
        //Based on the resolution and the volumes max extent, calculate the voxel size
        let max_extent = extent.x.max(extent.y.max(extent.z));
        let voxel_size = max_extent / (resolution as f32);
        maxs = mins
            + na::Vector3::new(
                resolution as f32 * voxel_size,
                resolution as f32 * voxel_size,
                resolution as f32 * voxel_size,
            );

        //Now iterate through each voxel and, if we have an intersection calculate the voxel at this point.

        println!("Voxelizing Scene with {} voxels per axis", resolution);
        //This step could easily be done on the gpu by spawning a thread per voxel and let it do the intersection
        //However, currently we do some kind of 3d rasterization by shooting rays from the x/y plane in the z direction.
        // Each ray intersects the whole scene and places appropriate voxels at each location it finds.

        println!("X Direction");
        let (x_buf, proc_x) = self.voxelize_in_direction(
            VoxelizingDirection::X,
            mins,
            voxel_size,
            num_samples,
            resolution as usize,
        );
        println!("Y Direction");
        let (y_buf, proc_y) = self.voxelize_in_direction(
            VoxelizingDirection::Y,
            mins,
            voxel_size,
            num_samples,
            resolution as usize,
        );

        println!("Z Direction");
        let (z_buf, proc_z) = self.voxelize_in_direction(
            VoxelizingDirection::Z,
            mins,
            voxel_size,
            num_samples,
            resolution as usize,
        );

        //Wait for thread percentage and update until 95 percent, after that wait for the first join
        let mut percentages = [0.0; 3];
        let mut percentage = 0.0;
        let mut last_update = 0.0;
        println!("Voxelizing progress: ");
        println!("\t0%");

        while percentage < 1.0 {
            //Add percentage per thread as one third
            if let Ok(perc) = proc_x.try_recv() {
                percentages[0] = perc;
            }

            if let Ok(perc) = proc_y.try_recv() {
                percentages[1] = perc;
            }

            if let Ok(perc) = proc_z.try_recv() {
                percentages[2] = perc;
            }

            percentage = (percentages[0] + percentages[1] + percentages[2]) / 3.0;

            if percentage - last_update > 0.05 {
                println!("\t{}%", percentage * 100.0);
                last_update = percentage;
            }
        }

        println!("Finished voxelizing!");
        //Combine all outputs
        let mut bottom_buffer = x_buf.join().expect("Failed to get x buffer!");
        println!("Interpolating voxels with y-Buffer, this might take a while.");
        for (loc, vox) in y_buf.join().expect("Failed to get y buffer!").into_iter() {
            if let Some(ref mut inner) = bottom_buffer.get_mut(&loc) {
                inner.interpolate_in_place(&vec![vox]);
            } else {
                bottom_buffer.insert(loc, vox);
            }
        }
        println!("Interpolating voxels with z-Buffer, this might take a while.");
        for (loc, vox) in z_buf.join().expect("Failed to get y buffer!").into_iter() {
            if let Some(ref mut inner) = bottom_buffer.get_mut(&loc) {
                inner.interpolate_in_place(&vec![vox]);
            } else {
                bottom_buffer.insert(loc, vox);
            }
        }

        for (_, vox) in bottom_buffer.iter() {
            assert!(
                vox.albedo_alpha()[3] <= 1.0,
                "Albedo too high after interpolation!"
            );
        }

        if solidify {
            println!("Making model solid...");
            solidify_buffer(
                &mut bottom_buffer,
                mins,
                voxel_size,
                resolution as usize,
                self,
            );
        }

        let max_voxel_count = 8.0_f32.powf((bottom_buffer.len() as f32).log(8.0).ceil()) as usize
            + bottom_buffer.len();
        println!(
            "Building acceleration structure for {} voxels ...",
            bottom_buffer.len()
        );

        //Insert all voxels into new voxel object
        let mut inner_object: voxer::Object<Voxel, SVO<Voxel>> = voxer::Object::new(SVO::new(None));
        let leaf_voxel_count = bottom_buffer.len();
        for (location, voxel) in bottom_buffer.into_iter() {
            inner_object.insert(
                [
                    location[0].try_into().unwrap_or(std::u32::MAX),
                    location[1].try_into().unwrap_or(std::u32::MAX),
                    location[2].try_into().unwrap_or(std::u32::MAX),
                ],
                voxel,
            )
        }

        //After inserting, create abstract voxel object
        let voxel_object = VoxelObject::from_data(inner_object, mins.into(), maxs.into());

        println!(
            "Final object size: {}mb with {} voxels",
            (leaf_voxel_count * std::mem::size_of::<Voxel>()) / 1000 / 1000,
            leaf_voxel_count
        );
        (leaf_voxel_count, voxel_object)
    }
    ///Returns the joint handle that will contain the generated voxels, as well as a reciver that sends a
    /// voxelizing percentage from time to time as an f32.
    fn voxelize_in_direction(
        &self,
        direction: VoxelizingDirection,
        from: na::Vector3<f32>,
        voxel_size: f32,
        num_samples: usize,
        steps: usize,
    ) -> (JoinHandle<HashMap<[usize; 3], Voxel>>, Receiver<f32>) {
        let model_scene: Arc<ModelScene> = self.clone();

        let (send, recv) = channel();

        let handle = std::thread::spawn(move || {
            let mut bottom_buffer: HashMap<[usize; 3], Voxel> = HashMap::new();

            for a in 0..steps {
                for b in 0..steps {
                    let initial_origin = direction.origin_for_step(a, b, from, voxel_size);
                    let origins: Vec<_> = direction
                        .random_origin_offsets(num_samples, voxel_size)
                        .into_iter()
                        .map(|o| initial_origin + o)
                        .collect();
                    let ray_direction = direction.to_direction_vector();
                    //Generate all intersection for this direction and all its origins
                    let mut intersections = Vec::new();
                    for obj in model_scene.objects.iter() {
                        intersections.append(&mut obj.ray_intersect(
                            &origins,
                            &direction,
                            ray_direction,
                            model_scene.as_ref(),
                            voxel_size,
                        ));
                    }

                    //Now sort all intersections based on the distance t
                    let mut t_sorted_voxels: HashMap<usize, Vec<Voxel>> = HashMap::new();

                    //println!("Found {} intersections", intersections.len());
                    for (t, i_vox) in intersections {
                        //Calc at which index we would have to store this voxel.
                        let voxel_index = (t / voxel_size).floor() as usize;
                        if let Some(voxel_inter) = t_sorted_voxels.get_mut(&voxel_index) {
                            voxel_inter.push(i_vox);
                        } else {
                            t_sorted_voxels.insert(voxel_index, vec![i_vox]);
                        }
                    }

                    //Finally insert them at the right position based on their direction
                    direction.insert_voxels(&mut bottom_buffer, t_sorted_voxels, steps, a, b);
                }
                //Calc how far we are
                let perc = (a * steps) as f32 / (steps * steps) as f32;
                send.send(perc)
                    .expect("Failed to send voxelizing percentage!");
            }
            send.send(1.0)
                .expect("Failed to send voxelizing percentage!");
            return bottom_buffer;
        });
        (handle, recv)
    }
}

fn solidify_buffer(
    buffer: &mut HashMap<[usize; 3], Voxel>,
    from: na::Vector3<f32>,
    voxel_size: f32,
    steps: usize,
    scene: &Arc<ModelScene>,
) {
    let directions = [
        na::Vector3::new(1.0, 0.0, 0.0),
        na::Vector3::new(0.0, 1.0, 0.0),
        na::Vector3::new(0.0, 0.0, 1.0),
        na::Vector3::new(-1.0, 0.0, 0.0),
        na::Vector3::new(0.0, -1.0, 0.0),
        na::Vector3::new(0.0, 0.0, -1.0),
    ];

    let new_voxels: Vec<([usize; 3], Voxel)> = (0..steps)
        .into_par_iter()
        .map(|a| {
            (0..steps)
                .into_par_iter()
                .map(|b| {
                    (0..steps)
                        .into_par_iter()
                        .filter_map(|z| {
                            let mut best_intersection = None;
                            //For each object, test if we are within this object, if so, add a voxel
                            for obj in scene.objects.iter() {
                                let mut intersections: [Option<(f32, Voxel)>; 6] =
                                    [None, None, None, None, None, None];

                                let origin = na::Vector3::new(
                                    (from.x + (a as f32) * voxel_size),
                                    (from.y + (b as f32) * voxel_size),
                                    (from.z + (z as f32) * voxel_size),
                                );
                                //If the object does not intersect the voxel, early return
                                if origin.x < obj.mins.x
                                    || origin.x > obj.maxs.x
                                    || origin.y < obj.mins.y
                                    || origin.y > obj.maxs.y
                                    || origin.z < obj.mins.z
                                    || origin.z > obj.maxs.z
                                {
                                    break;
                                }

                                //Trace in all directions and check if we always intersect "outwards pointing" faces. If so, we are within an object and therefore
                                // we can place a voxel here.
                                for i in 0..6 {
                                    intersections[i] = insert_intersection(
                                        intersections[i].clone(),
                                        obj.get_best_intersection(origin, directions[i], scene),
                                    );
                                }

                                //If we have just intersections, compute inner voxel and add to buffer
                                if let (
                                    Some(v0),
                                    Some(v1),
                                    Some(v2),
                                    Some(v3),
                                    Some(v4),
                                    Some(v5),
                                ) = (
                                    intersections[0].clone(),
                                    intersections[1].clone(),
                                    intersections[2].clone(),
                                    intersections[3].clone(),
                                    intersections[4].clone(),
                                    intersections[5].clone(),
                                ) {
                                    best_intersection = Some((
                                        [a, b, z],
                                        Voxel::interpolate(&[v0.1, v1.1, v2.1, v3.1, v4.1, v5.1]),
                                    ));
                                };
                            }

                            best_intersection
                        })
                        .collect::<Vec<([usize; 3], Voxel)>>()
                })
                .flatten()
                .collect::<Vec<([usize; 3], Voxel)>>()
        })
        .flatten()
        .collect();

    println!("Inserting {} solid voxels", new_voxels.len());
    for (k, v) in new_voxels.into_iter() {
        buffer.insert(k, v);
    }
}

fn insert_intersection(
    into: Option<(f32, Voxel)>,
    other: Option<(f32, Voxel)>,
) -> Option<(f32, Voxel)> {
    let mut best = into.clone();
    if let Some(old_i) = into {
        if let Some(new_i) = other {
            //There is already an intersection, only substitude if t is smaller
            if old_i.0 > new_i.0 {
                best = Some(new_i);
            }
        }
    } else {
        if other.is_some() {
            best = other.clone();
        }
    }

    best
}

///Creates and image::ImageFormat from a mime_type string
pub fn format_from_str<'a>(mime_type: &'a str) -> ImageFormat {
    let mime_type_string = mime_type.to_string();

    if mime_type_string.contains("png") {
        return ImageFormat::PNG;
    }

    if mime_type_string.contains("jpeg") {
        return ImageFormat::JPEG;
    }

    if mime_type_string.contains("gif") {
        return ImageFormat::GIF;
    }

    if mime_type_string.contains("webp") {
        return ImageFormat::WEBP;
    }

    if mime_type_string.contains("pnm") {
        return ImageFormat::PNM;
    }

    if mime_type_string.contains("tiff") {
        return ImageFormat::TIFF;
    }

    if mime_type_string.contains("tga") {
        return ImageFormat::TGA;
    }

    if mime_type_string.contains("bmp") {
        return ImageFormat::BMP;
    }

    if mime_type_string.contains("ico") {
        return ImageFormat::ICO;
    }

    if mime_type_string.contains("hdr") {
        return ImageFormat::HDR;
    }

    ImageFormat::PNG
}
