/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use gltf::buffer;
use gltf::image;
use gltf::Gltf;
use std::cmp::{max, min};
use tikan::data::voxel_object::*;

use crate::model_scene::*;
use std::result::Result;
use std::sync::Arc;

#[derive(Debug, Copy, Clone)]
pub enum ConvertError {
    ///Could not load the gltf file for some reason
    FailedToLoadFile,
}

///Loads a gltf file into a ModelScene object.0
pub fn load(path: &str) -> Result<Arc<ModelScene>, ConvertError> {
    let model = match Gltf::open(path) {
        Ok(f) => f,
        Err(e) => {
            println!("Failed to load gltf file: {}", e);
            return Err(ConvertError::FailedToLoadFile);
        }
    };

    let (_document, buffers, images) = match gltf::import(path) {
        Ok(f) => f,
        Err(e) => {
            println!("Failed to load gltf buffers: {}", e);
            return Err(ConvertError::FailedToLoadFile);
        }
    };

    //After reading the gltf file we iterate over all triangle models and build up a correctly transformed
    // model of all scenes in this file.
    // The final model will later be ray-traced to form our final leaf-level voxel structure.

    let mut model_scene = ModelScene::new(String::from(path));

    for scene in model.scenes() {
        //println!("Loading Scene: {}", scene.index());
        for node in scene.nodes() {
            //println!("\tLoading root node: {}", node.index());
            //worker transformation of the current node. Used to correctly
            //transform child nodes triangles.
            let mut transform: na::Matrix4<f32> = na::Matrix4::identity();
            node.load(&mut model_scene, transform, &buffers);
        }
    }

    return Ok(Arc::new(model_scene));
}

trait LoadNode {
    fn load(
        &self,
        model_scene: &mut ModelScene,
        parent_transform: na::Matrix4<f32>,
        buffers: &[buffer::Data],
    );
}

impl LoadNode for gltf::Node<'_> {
    fn load(
        &self,
        model_scene: &mut ModelScene,
        parent_transform: na::Matrix4<f32>,
        buffers: &[buffer::Data],
    ) {
        let transform = parent_transform * na::Matrix4::from(self.transform().matrix());
        //If there is one, load mesh into scene with given transfrom
        if let Some(m) = self.mesh() {
            //load all primitives in the model_scene
            for prim in m.primitives() {
                prim.load(model_scene, transform, buffers);
            }
        }

        //Now load children of this node
        for child in self.children() {
            child.load(model_scene, transform, buffers);
        }
    }
}

impl LoadNode for gltf::Primitive<'_> {
    fn load(
        &self,
        model_scene: &mut ModelScene,
        parent_transform: na::Matrix4<f32>,
        buffers: &[buffer::Data],
    ) {
        //load the material
        let material = {
            let material = self.material();
            //Pre load the textures then build the material
            let base_color_texture =
                if let Some(t) = material.pbr_metallic_roughness().base_color_texture() {
                    Some(model_scene.add_texture(t.texture(), buffers))
                } else {
                    None
                };

            let metallic_roughness_texture = if let Some(t) = material
                .pbr_metallic_roughness()
                .metallic_roughness_texture()
            {
                Some(model_scene.add_texture(t.texture(), buffers))
            } else {
                None
            };

            let normal_texture = if let Some(t) = material.normal_texture() {
                Some(model_scene.add_texture(t.texture(), buffers))
            } else {
                None
            };

            let occlusion_texture = if let Some(t) = material.occlusion_texture() {
                Some(model_scene.add_texture(t.texture(), buffers))
            } else {
                None
            };

            let emissive_texture = if let Some(t) = material.emissive_texture() {
                Some(model_scene.add_texture(t.texture(), buffers))
            } else {
                None
            };

            println!(
                "Loaded with emissive factor: {:?}",
                material.emissive_factor()
            );

            Material {
                alpha_cutoff: material.alpha_cutoff(),
                base_color_factor: material.pbr_metallic_roughness().base_color_factor(),
                base_color_texture,
                metallic_factor: material.pbr_metallic_roughness().metallic_factor(),
                roughness_factor: material.pbr_metallic_roughness().roughness_factor(),
                metallic_roughness_texture,
                normal_texture,
                occlusion_texture,
                emissive_texture,
                emissive_factor: material.emissive_factor(),
            }
        };

        //Now load the triangles and transform them based on the given matrix.
        let mesh_reader = self.reader(|buffer| Some(&buffers[buffer.index()]));

        //Now load the vertex data, construct the vertices and transfrom all of them, then save it to the scene
        //Read indices
        let indices = if let Some(indice_iter) = mesh_reader.read_indices() {
            let mut indices = Vec::new();
            let u_iter = indice_iter.into_u32();
            for idx in u_iter {
                indices.push(idx);
            }
            indices
        } else {
            println!("WARNING: Gltf could not load indice iterator!",);
            Vec::new()
        };

        let positions_iter: Vec<[f32; 3]> = if let Some(pos_iter) = mesh_reader.read_positions() {
            pos_iter.collect()
        } else {
            Vec::new()
        };

        let normals_iter: Vec<[f32; 3]> = if let Some(norm_iter) = mesh_reader.read_normals() {
            norm_iter.collect()
        } else {
            Vec::new()
        };

        let tangents_iter: Vec<[f32; 4]> = if let Some(tan_iter) = mesh_reader.read_tangents() {
            tan_iter.collect()
        } else {
            Vec::new()
        };

        let colors_iter: Vec<[f32; 4]> = if let Some(col_iter) = mesh_reader.read_colors(0) {
            col_iter.into_rgba_f32().collect()
        } else {
            Vec::new()
        };

        let texcord_iter: Vec<[f32; 2]> = if let Some(tex_iter) = mesh_reader.read_tex_coords(0) {
            tex_iter.into_f32().collect()
        } else {
            Vec::new()
        };

        if positions_iter.len() <= 0 {
            println!("WARNING: Did not found any position data for primitive!");
        }

        //Now use the index buffer, always take three indices, and, from them build a triangle.
        //TODO handle lines and points somehow? currently just panic when its not a triangle based mesh
        if self.mode() != gltf::mesh::Mode::Triangles {
            panic!(
                "Cannot load non Mode::Triangles meshes, mode was : {:?}",
                self.mode()
            );
        }

        let mut vertices = Vec::new();
        let mut mins: na::Vector3<f32> = na::Vector3::from([std::f32::MAX; 3]);
        let mut maxs: na::Vector3<f32> = na::Vector3::from([std::f32::MIN; 3]);

        for (idx, pos) in positions_iter.into_iter().enumerate() {
            //Create data and push vertex
            let mut position = na::Point3::new(pos[0], pos[1], pos[2]);
            position = parent_transform.transform_point(&position);

            //update the mins and max values
            maxs.x = position.x.max(maxs.x);
            maxs.y = position.y.max(maxs.y);
            maxs.z = position.z.max(maxs.z);

            mins.x = position.x.min(mins.x);
            mins.y = position.y.min(mins.y);
            mins.z = position.z.min(mins.z);

            let normal =
                na::Vector3::from(normals_iter.get(idx).unwrap_or(&[0.0, 1.0, 0.0]).clone());
            let uv_coord = na::Vector2::from(texcord_iter.get(idx).unwrap_or(&[0.0, 0.0]).clone());
            let tangent = na::Vector4::from(
                tangents_iter
                    .get(idx)
                    .unwrap_or(&[0.0, 1.0, 0.0, 0.0])
                    .clone(),
            );
            let color = na::Vector4::from(
                colors_iter
                    .get(idx)
                    .unwrap_or(&[0.0, 0.0, 0.0, 1.0])
                    .clone(),
            );

            vertices.push(Vertex {
                position,
                uv_coord,
                normal,
                tangent,
                color,
            });
        }

        assert!(
            (indices.len() % 3) == 0,
            "Indice length is not multiple of 3!"
        );
        let mut faces = Vec::with_capacity(indices.len() / 3);
        let mut indice_iter = indices.iter();
        loop {
            //retrieve indices
            let v1 = if let Some(v) = indice_iter.next() {
                *v as usize
            } else {
                break;
            };
            let v2 = if let Some(v) = indice_iter.next() {
                *v as usize
            } else {
                break;
            };
            let v3 = if let Some(v) = indice_iter.next() {
                *v as usize
            } else {
                break;
            };

            let mins = bvh::nalgebra::Point3::new(
                vertices[v1]
                    .position
                    .x
                    .min(vertices[v2].position.x.min(vertices[v3].position.x)),
                vertices[v1]
                    .position
                    .y
                    .min(vertices[v2].position.y.min(vertices[v3].position.y)),
                vertices[v1]
                    .position
                    .z
                    .min(vertices[v2].position.z.min(vertices[v3].position.z)),
            );

            let maxs = bvh::nalgebra::Point3::new(
                vertices[v1]
                    .position
                    .x
                    .max(vertices[v2].position.x.max(vertices[v3].position.x)),
                vertices[v1]
                    .position
                    .y
                    .max(vertices[v2].position.y.max(vertices[v3].position.y)),
                vertices[v1]
                    .position
                    .z
                    .max(vertices[v2].position.z.max(vertices[v3].position.z)),
            );

            let face = Face {
                indices: [v1 as u32, v2 as u32, v3 as u32],
                bounding_box: bvh::aabb::AABB::with_bounds(mins, maxs),
                bvh_index: 0,
            };
            //Calc face for it and push
            faces.push(face);
        }

        let bvh = bvh::bvh::BVH::build(&mut faces);

        model_scene.add_object(Object {
            vertices,
            indices,
            faces,
            bvh,
            material,
            mins,
            maxs,
        });
    }
}

///Extracts the bytes from a `buffer` out of a gltf files `buffers`
pub fn to_buffer<'a>(buffer: &gltf::Buffer, buffers: &'a [gltf::buffer::Data]) -> Option<&'a [u8]> {
    //Try to read the buffer at index
    if buffer.index() >= buffers.len() {
        println!("WARNING: gltf import, tried to read outside of buffer",);
        None
    } else {
        Some(buffers[buffer.index()].0.as_slice())
    }
}

///Shortcut for extracting an image from a gltf files bufffers
pub fn to_image<'a>(
    view: &gltf::buffer::View,
    buffers: &'a [gltf::buffer::Data],
) -> Option<&'a [u8]> {
    let data_buffer = to_buffer(&view.buffer(), buffers);
    data_buffer.map(|data| {
        let begin = view.offset();
        let end = begin + view.length();
        &data[begin..end]
    })
}
