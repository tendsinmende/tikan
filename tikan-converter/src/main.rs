/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

extern crate bvh;
extern crate byteorder as bo;
extern crate gltf;
extern crate image;
extern crate nalgebra as na;
extern crate rayon;
extern crate voxer;

pub mod format;
pub mod gltf_import;
pub mod model_scene;
use model_scene::Voxelizer;

use std::convert::TryInto;
use std::time::Instant;
use tikan::data::voxel_object::*;

enum LastAction {
    InputFile,
    OutputFile,
    SamplingCount,
    Resolution,
    Compresssion,
    Solidify,
}

fn main() {
    let mut last_action = None;
    let mut input_file = None;
    let mut output_file = None;
    let mut sampling_count: usize = 1;
    let mut resolution: u32 = 512;
    let mut compression = 0.1;
    let mut solidify = true;

    for arg in std::env::args() {
        //If we had an action argument use that
        if let Some(action) = last_action.take() {
            match action {
                LastAction::InputFile => {
                    input_file = Some(arg);
                }
                LastAction::OutputFile => {
                    output_file = Some(arg);
                }
                LastAction::SamplingCount => {
                    sampling_count = arg
                        .parse::<u64>()
                        .expect(
                            "Failed to parse sampling count into uint, was it a whole number > 0 ?",
                        )
                        .try_into()
                        .expect("Could not convert sampling count into usize!");
                }
                LastAction::Resolution => {
                    resolution = arg.parse::<u32>().expect(
                        "Failed to parse voxel size into a f32, was it a floating point number?",
                    );
                }
                LastAction::Compresssion => {
                    compression = arg
                        .parse::<f32>()
                        .expect(
                            "Failed to get compression value! Must be a float between 0.0 and 1.0",
                        )
                        .min(1.0)
                        .max(0.0);
                }
                LastAction::Solidify => {
                    solidify = arg
                        .parse::<bool>()
                        .expect("Failed to parse argument for solidify! Must be true or false");
                }
            }
        } else {
            //Parse
            match arg.as_str() {
                "-i" => last_action = Some(LastAction::InputFile),
                "-o" => last_action = Some(LastAction::OutputFile),
                "-h" | "--help" => print_help(),
                "-s" => last_action = Some(LastAction::SamplingCount),
                "-v" => last_action = Some(LastAction::Resolution),
                "-c" => last_action = Some(LastAction::Compresssion),
                "-f" => last_action = Some(LastAction::Solidify),
                _ => println!("Argument {} not defined!", arg),
            }
        }
    }

    let input_file = if input_file.is_none() {
        println!("No Input file was specified, aboarding!");
        return;
    } else {
        input_file.expect("Could not find input file, but wasnt none")
    };

    let output_file = if let Some(o) = output_file {
        o
    } else {
        println!("No output file specified, using \"out.bin\"");
        String::from("out.bin")
    };

    //Fix up resolution to power of 8 if it isn't already
    if !resolution.is_power_of_two() {
        resolution = resolution.next_power_of_two();
        println!("Resolution wasn't power of two, using: {}", resolution);
    }

    println!(
        "Starting Converter with following settings: \n
              \tinput file: {}\n
              \toutput file: {}\n
              \tsampling count: {}\n
              \tresolution: {}\n
              \tcompression: {}\n
              \tsolidify: {}",
        input_file, output_file, sampling_count, resolution, compression, solidify
    );

    //Now load the scene from this file
    let mut start = Instant::now();
    println!("Loading scene!");

    let scene = gltf_import::load(&input_file).expect("Failed to load gltf file!");
    println!("Loading scene took: {}sec", start.elapsed().as_secs_f32());
    start = Instant::now();

    let (leaf_count, model) = scene.voxelize(resolution, sampling_count, compression, solidify);
    println!("voxelizing took: {} sec", start.elapsed().as_secs_f32());
    start = Instant::now();
    //write to given file
    format::save_to_file(&model, &output_file, leaf_count);
    println!(
        "writing to file took: {} sec",
        start.elapsed().as_secs_f32()
    );
}

fn print_help() {
    println!("\nTIKAN MODELL CONVERTER\n
an command line utility to convert an input gltf file to a voxel binary \n
\n
Arguments: \n
-i: input file location\n
-o: output file name, if not set \"out.bin\" is used\n
-s: sampling count (must be > 0 and a whole number)\n
-v: resolution of the volume, must be power of two, otherwise the next bigger one will be used\n
-c: compression factor (should be between 0 and 1) This will collapse children into one parent voxel if they are at least this percentage similar to each other. Default is 0.1/ 10%\n
-f: Solidify model. This will add voxels within objects. Should always be used when the source model is closed. Otherwise cone-tracing results are not perfect.

\n
");
}
