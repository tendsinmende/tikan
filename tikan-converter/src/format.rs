/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Format
//! ## General Information
//! The binary format of voxel objects contains an header
//! Which specifies general data like format version, min/max extent of the object etc.
//! The general data part is followed by the voxel octree.
//! The Octree is stored on a per voxel basis, it starts with the root voxel and is then
//! layed out in a depth first order.
//!
//! Each voxel knows for each of its children the index of that child within the octree-data part of that file.
//! The load function loads all voxels into a big vec and the hierachy can then be build by reading the
//! correct index for each child, if there is one
//!
//! # Speical Notes
//! - All data is layout out in little-endian
//! - A voxel is written out as: [u32;3] location,  [f32; 4] albedo_alpha, [f32;3] emission, [f32;3] normal, f32 roughness,
//! f32 metallness
//!
//! Only leafs are written into the format, intermediate voxels in the octree are generated when loading it.
use bo::*;
use tikan::data::voxel_object::*;

use voxer;
use voxer::{octree::SVO, Accelerator};

use std::fs::File;
use std::io::{BufReader, BufWriter, Read, Write};

pub const FORMAT_VERSION: u32 = 3;

///A Files header
struct Header {
    ///Version of the format that was used when saving this voxel object.
    version: u32,
    ///minimal coordinate of this object
    mins: [f32; 3],
    ///Max coordinate of this object
    maxs: [f32; 3],

    ///Number of voxels the object contains
    num_voxels: u64,

    resolution: u32,
}

impl Header {
    ///Writes the header to a write. For it to be compliant with the described format, the write has to be at the
    ///Start of the given file.
    fn write_to<W>(&self, writer: &mut W)
    where
        W: WriteBytesExt,
    {
        writer
            .write_u32::<LittleEndian>(self.version)
            .expect("Failed to write format version!");
        write_vec3(self.mins, writer);
        write_vec3(self.maxs, writer);
        writer
            .write_u64::<LittleEndian>(self.num_voxels)
            .expect("Failed to write objects voxel count!");
        writer
            .write_u32::<LittleEndian>(self.resolution)
            .expect("Could not write out voxel resolution!");
    }

    ///Reads the Header from a given reader. For it to comply with the described format, the reader must be at the
    ///start of the given file.
    fn from_reader<R>(reader: &mut R) -> Self
    where
        R: ReadBytesExt,
    {
        let version = reader
            .read_u32::<LittleEndian>()
            .expect("Failed to read format version");
        let mins = read_vec3(reader);
        let maxs = read_vec3(reader);
        let num_voxels = reader
            .read_u64::<LittleEndian>()
            .expect("Failed to read objects voxel number!");
        let resolution = reader
            .read_u32::<LittleEndian>()
            .expect("Could not read resolution");

        Header {
            version,
            mins,
            maxs,
            num_voxels,
            resolution,
        }
    }
}

///Saves a voxel object file into a binary file saved at `file`
pub fn save_to_file(object: &VoxelObject, file: &str, num_leaf_voxels: usize) {
    let header = Header {
        version: FORMAT_VERSION,
        mins: object.mins(),
        maxs: object.maxs(),
        num_voxels: num_leaf_voxels as u64,
        resolution: object.resolution(),
    };

    let file = File::create(file).expect("Failed to create binary file!");
    //Resize to the size we need to store all information
    let len = {
        //header
        (std::mem::size_of::<u32>() + std::mem::size_of::<u64>()
            //voxel data
                + object.num_voxels() * (
                    14 * std::mem::size_of::<f32>() + 8 * std::mem::size_of::<u64>()
                )) as u64
    };
    file.set_len(len).expect("Failed to set files length!");

    let mut buffered_writer = BufWriter::new(file);

    //Add header data to file
    header.write_to(&mut buffered_writer);
    write_voxels(object, &mut buffered_writer);

    buffered_writer
        .flush()
        .expect("Failed to flush rest of write out to file!");
}

fn write_voxels<W>(object: &VoxelObject, writer: &mut W)
where
    W: WriteBytesExt,
{
    let resolution = object.get_inner().get_accelerator().dimensions()[0];

    for x in 0..resolution {
        for y in 0..resolution {
            for z in 0..resolution {
                if let Some(v) = object.get_inner().get(&[x, y, z]) {
                    write_voxel([x, y, z], v, writer);
                }
            }
        }
    }

    //assert!(index == object.num_voxels(), "Did not write voxel count to file!");
}

fn write_voxel<W>(location: [u32; 3], voxel: &Voxel, writer: &mut W)
where
    W: WriteBytesExt,
{
    //Write out the data followed by the childrens indexes
    //writer.write_u8(voxel.level).expect("Failed to write voxels level");

    //writer.write_u8(if voxel.is_leaf() { 1 } else { 0 }).expect("Failed to write \"is_leaf\" flag");

    //let (mins, maxs) = voxel.mins_maxs();
    //write_vec3(mins, writer);
    //write_vec3(maxs, writer);

    //Writeout location.
    writer
        .write_u32::<LittleEndian>(location[0])
        .expect("Failed to write location x");
    writer
        .write_u32::<LittleEndian>(location[1])
        .expect("Failed to write location y");
    writer
        .write_u32::<LittleEndian>(location[2])
        .expect("Failed to write location z");

    let albedo_alpha = voxel.albedo_alpha();
    assert!(
        albedo_alpha[0] <= 1.0 && albedo_alpha[0] >= 0.0,
        "albedo was: {:?}",
        albedo_alpha
    );
    assert!(
        albedo_alpha[1] <= 1.0 && albedo_alpha[1] >= 0.0,
        "albedo was: {:?}",
        albedo_alpha
    );
    assert!(
        albedo_alpha[2] <= 1.0 && albedo_alpha[2] >= 0.0,
        "albedo was: {:?}",
        albedo_alpha
    );
    assert!(
        albedo_alpha[3] <= 1.0 && albedo_alpha[3] >= 0.0,
        "albedo was: {:?}",
        albedo_alpha
    );

    write_vec4(voxel.albedo_alpha(), writer);
    write_vec3(voxel.emission(), writer);
    write_vec3(voxel.normal(), writer);

    writer
        .write_f32::<LittleEndian>(voxel.roughness())
        .expect("Failed to write roughness!");
    writer
        .write_f32::<LittleEndian>(voxel.metallness())
        .expect("Failed to write metallness!");
}

fn write_vec3<W>(vec: [f32; 3], writer: &mut W)
where
    W: WriteBytesExt,
{
    writer
        .write_f32::<LittleEndian>(vec[0])
        .expect("Failed to write vector!");
    writer
        .write_f32::<LittleEndian>(vec[1])
        .expect("Failed to write vector!");
    writer
        .write_f32::<LittleEndian>(vec[2])
        .expect("Failed to write vector!");
}

fn read_vec3<R>(reader: &mut R) -> [f32; 3]
where
    R: ReadBytesExt,
{
    [
        reader
            .read_f32::<LittleEndian>()
            .expect("Failed to read vector!"),
        reader
            .read_f32::<LittleEndian>()
            .expect("Failed to read vector!"),
        reader
            .read_f32::<LittleEndian>()
            .expect("Failed to read vector!"),
    ]
}
fn write_vec4<W>(vec: [f32; 4], writer: &mut W)
where
    W: WriteBytesExt,
{
    writer
        .write_f32::<LittleEndian>(vec[0])
        .expect("Failed to write vector!");
    writer
        .write_f32::<LittleEndian>(vec[1])
        .expect("Failed to write vector!");
    writer
        .write_f32::<LittleEndian>(vec[2])
        .expect("Failed to write vector!");
    writer
        .write_f32::<LittleEndian>(vec[3])
        .expect("Failed to write vector!");
}

fn read_vec4<R>(reader: &mut R) -> [f32; 4]
where
    R: ReadBytesExt,
{
    [
        reader
            .read_f32::<LittleEndian>()
            .expect("Failed to read vector!"),
        reader
            .read_f32::<LittleEndian>()
            .expect("Failed to read vector!"),
        reader
            .read_f32::<LittleEndian>()
            .expect("Failed to read vector!"),
        reader
            .read_f32::<LittleEndian>()
            .expect("Failed to read vector!"),
    ]
}
#[derive(Debug)]
pub enum LoadError {
    WrongVersion,
}

impl std::fmt::Display for LoadError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            LoadError::WrongVersion => write!(
                f,
                "Importers version does not match files version, file version should be: {}",
                FORMAT_VERSION.to_string()
            ),
        }
    }
}

///Loads a binary file from `file`
pub fn load_from_file(file: &str) -> Result<VoxelObject, LoadError> {
    //create the reader file by loading the given file, then read the header and after that read all voxels into a big array.
    let mut reader = BufReader::new(File::open(file).expect("Failed to open file!"));

    let header = Header::from_reader(&mut reader);

    if header.version != FORMAT_VERSION {
        return Err(LoadError::WrongVersion);
    }

    //println!("Found header: version: {}, num: {}, extent: {:?}", header.version, header.num_voxels, header.extent);

    //Now read the object recursively by first loading all data into a big vec and then jumping within
    //the vec to the children until we used all indices.
    //The vec contains the raw data voxel (without children) as well as the ordered array of the
    //indices at which the children are located.
    let mut voxels: Vec<([u32; 3], Voxel)> = Vec::with_capacity(header.num_voxels as usize);

    for i in 0..header.num_voxels {
        read_voxel(&mut voxels, &mut reader, i);
    }

    //Setup internal octree by inserting all voxels
    let mut object: voxer::Object<Voxel, SVO<Voxel>> =
        voxer::Object::new(SVO::new(Some(header.resolution)));
    for (loc, vox) in voxels {
        object.insert(loc, vox);
    }

    Ok(VoxelObject::from_data(object, header.mins, header.maxs))
}

fn read_voxel<R>(buffer: &mut Vec<([u32; 3], Voxel)>, reader: &mut R, loop_index: u64)
where
    R: ReadBytesExt,
{
    let mut location = [0; 3];
    location[0] = reader
        .read_u32::<LittleEndian>()
        .expect("Failed to read voxel location x");
    location[1] = reader
        .read_u32::<LittleEndian>()
        .expect("Failed to read voxel location y");
    location[2] = reader
        .read_u32::<LittleEndian>()
        .expect("Failed to read voxel location z");

    let albedo_alpha = read_vec4(reader);

    assert!(
        albedo_alpha[0] <= 1.0 && albedo_alpha[0] >= 0.0,
        "albedo was: {:?}",
        albedo_alpha
    );
    assert!(
        albedo_alpha[1] <= 1.0 && albedo_alpha[1] >= 0.0,
        "albedo was: {:?}",
        albedo_alpha
    );
    assert!(
        albedo_alpha[2] <= 1.0 && albedo_alpha[2] >= 0.0,
        "albedo was: {:?}",
        albedo_alpha
    );
    assert!(
        albedo_alpha[3] <= 1.0 && albedo_alpha[3] >= 0.0,
        "albedo was: {:?}",
        albedo_alpha
    );

    let emission = read_vec3(reader);
    let normal = read_vec3(reader);

    assert!(
        normal[0] >= -1.0 && normal[0] <= 1.0,
        "Normal was: {:?}",
        normal
    );
    assert!(
        normal[1] >= -1.0 && normal[1] <= 1.0,
        "Normal was: {:?}",
        normal
    );
    assert!(
        normal[2] >= -1.0 && normal[2] <= 1.0,
        "Normal was: {:?}",
        normal
    );

    let roughness = reader
        .read_f32::<LittleEndian>()
        .expect("Failed to read roughness!");
    let metallness = reader
        .read_f32::<LittleEndian>()
        .expect("Failed to read metallness!");

    let mut voxel = Voxel::new_leaf(albedo_alpha, emission, normal, roughness, metallness);

    //Push voxel and children
    buffer.push((location, voxel));
}
