# Usage
First build the whole engine and its sub crates via 
```
cargo build --release
```

then convert any file for instance like this:
```
./target/release/tikan-converter -i Path_to_my_gltf.gltf -o OutputName.bin -v 1024 -s 8 -f false
```

the converter can also print its help via `-h`.
