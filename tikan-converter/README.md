# Tikan Converter
## Description
The converter tool converts a gltf2.0 model into a voxelized model of the given file.

The voxel model is saved in tikans own binary format.

## Future
I might add other file formats later, however, currently gltf provides everything that's needed.

## Usage

### Converting a GLTF file
Change into tikan git directory and type:
``` sh
cargo build --release && ./target/release/tikan-converter -i [GLTF-File] -o [Binary-File-Name]
```

where `[GLTF-File]` is the absolute path to the gltf file and `[Binary-File-Name]` is the name of the resulting voxel-model-file.

The following arguments for the converter are supported:
- `-o` output file name (if not specified out.bin is used)
- `-i` input file name (must be set)
- `-h` prints help
- `-s` sampling count per voxel -> higher samples means better voxel data but longer voxelization (default is 1)
- `-v` voxel size -> high means more memory usage and longer voxelization, but much better visuals (default is 0.5)

### As library
The converter can also be used as a library. For instance when loading a binary file. However, this is usually only useful to either learn the binary format or when used within the tikan engine.
