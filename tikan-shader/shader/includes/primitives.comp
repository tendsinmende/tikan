/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

///Generic Ray primitive
struct Ray{
  vec3 origin;
  vec3 direction;
};

Ray new_ray(vec3 origin, vec3 direction){
  //vec3 inv_dir = 1.0 / direction;
  return Ray(origin, direction);
}

///Transforms a ray according to the specfied transformation matrix
Ray transform_ray(Ray ray, mat4 space){
  //Transform the ray into new space
  vec4 new_direction = space * vec4(ray.direction, 0.0);
  vec3 direction = normalize(new_direction.xyz);

  vec4 new_pos = space * vec4(ray.origin, 1.0);
  vec3 origin = new_pos.xyz / new_pos.w;

  return new_ray(origin, direction);
}

float deg2rad(float deg){
  return deg * ( 3.1415926535 / 180.0);
}

///Generates a primary ray for the camera based on a given location of the pixel, an image extent and an camera.
/// The ray is in camera space
Ray primary_ray(ivec2 pixel_location, ivec2 image_extent, float fov){
  //First generate cmaera space rays
  const float aspect_ratio = float(image_extent.x) / float(image_extent.y);
  //Find fov based scalig
  const float fov_scale = tan(deg2rad(fov / 2));

  const float x = (2.0 * (pixel_location.x + 0.5) / float(image_extent.x) - 1.0) * aspect_ratio * fov_scale;
  const float y = (2.0 * (pixel_location.y + 0.5) / float(image_extent.y) - 1.0) * fov_scale;

  const vec4 direction = vec4(x, y, -1.0, 0.0);

  return new_ray(vec3(0.0), normalize(direction.xyz));
}


///Generic Cone Primitive
struct Cone{
  vec3 direction;
  //Half opening angle of the cone.
  float angle;
  vec3 origin;
};

Cone transform_cone(Cone cone, mat4 space){
  //Transform the ray into new space
  vec4 new_direction = space * vec4(cone.direction, 0.0);
  vec3 direction = normalize(new_direction.xyz);

  vec4 new_pos = space * vec4(cone.origin, 1.0);
  vec3 origin = new_pos.xyz / new_pos.w;

  return Cone(direction, cone.angle, cone.origin);
}

struct Sphere{
  vec3 origin;
  float radius;
};


float sq(float a){
  return a*a;
}
bool sphere_intersect_box(Sphere s, vec3 box_min, vec3 box_max){
  /*
  vec3 min_point = max(box_min, min(s.origin, box_max));
  float dist = sqrt(
		    (min_point.x - s.origin.x) * (min_point.x - s.origin.x) +
		    (min_point.y - s.origin.y) * (min_point.y - s.origin.y) +
		    (min_point.z - s.origin.z) * (min_point.z - s.origin.z)
		    );
  
  return dist < s.radius;
  */

  float distsq = s.radius * s.radius;

  if(s.origin.x < box_min.x) distsq -= sq(s.origin.x - box_min.x);
  else if (s.origin.x > box_max.x) distsq -= sq(s.origin.x - box_max.x);

  if(s.origin.y < box_min.y) distsq -= sq(s.origin.y - box_min.y);
  else if (s.origin.y > box_max.y) distsq -= sq(s.origin.y - box_max.y);

  if(s.origin.z < box_min.z) distsq -= sq(s.origin.z - box_min.z);
  else if (s.origin.z > box_max.z) distsq -= sq(s.origin.z - box_max.z);

  
  return distsq > 0.0;
  
}
