/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */


#define VOXEL_UNUSED 4294967295 //THIS SHOULD BE u32::MAX

//Defines the gpu voxel
struct Voxel{
  ///object local position of this voxel within its volume. on xyz, w is the level
  uvec4 location_level;
  ///Emissive property of the voxel on rgb, is currently unused
  vec4 emission;
  
  uint skip_index;
  //First 24bit are the albedo rgb channels, the last 8bit is the alpha value of this voxel.
  uint albedo_alpha;
  ///The first 8bit are roughness, the second 8bit are the metallic value, the third 8bit are 1 if this is a leaf, otherwise its 0, the fourth is unsued atm.
  uint roughness_metal_is_leaf_none;
  ///First 24 bit are the xyz components of the normal, the last part is unused
  uint normal_none;
};



//Defines the definition of a single voxel object. If offset and size are both uint::MAX it means that the object is not used
struct VoxelObject{
  ///Model matrix, used to transform from model to world space
  mat4 model;
  ///Inverse model matrix to transform from world to object space
  mat4 inv_model;

  //Mins and maxs properties of this object
  vec4 mins;
  vec4 maxs; 
  
  ///the offset into the `Voxels` buffer where this objects octree is stored
  uint offset;
  ///The size of voxels we have to read till we reach the octrees end within the `Voxels` buffer
  uint size;
  //Resolution of this volume
  uint resolution;
  ///is != 0 if the object is unallocated. If thats the case it can not be raytraced.
  uint is_not_allocated;
};

//Loads an input uint containing four 8bit uints into normalized floats between 0/1.
//Assumes the layout it [msb][one,two,three,four][lsb]
vec4 into_vec4(uint inp){
  //Shift bits according to location into first 8 bits,
  //then throw away everything after the first 8bit.
  uint one = ((inp >> 24) & 0x000000FF);
  uint two = ((inp >> 16) & 0x000000FF);
  uint three = ((inp >> 8) & 0x000000FF);
  uint four = inp & 0x000000FF;
  
  return vec4(
	      float(one) / 255.0,
	      float(two) / 255.0,
	      float(three) / 255.0,
	      float(four) / 255.0
	      );
}

//Loads the n-th 8bit value into a normalized float, when "location" > 3 it will be mapped to 3
float get_float(uint source, uint location){
  location = min(location, 3);
  //Same as before, but programmed, shift based on location, then throw away everything except for the first 8 bit.
  //finally convert into a normalized float
  return float( (source >> ((3 - location) * 8)) & 0x000000FF) / 255.0;
}

uint get_uint(uint source, uint location){
  location = min(location, 3);
  return ((source >> ((3 - location) * 8)) & 0x000000FF);
}

vec4 albedo_alpha(Voxel voxel){
  return into_vec4(voxel.albedo_alpha);
}

float roughness(Voxel voxel){
  return get_float(voxel.roughness_metal_is_leaf_none, 0);
}

float metallness(Voxel voxel){
  return get_float(voxel.roughness_metal_is_leaf_none, 1);
}

vec3 normal(Voxel voxel){
  //Since we packet a float between -1,1 into u8, we have to translate here.
  return (into_vec4(voxel.normal_none).xyz * 2.0) - 1.0;
}

bool is_leaf(Voxel voxel){
  if (get_uint(voxel.roughness_metal_is_leaf_none, 2) == 1){
    return true;
  }else{
    return false;
  }
}

uint get_level(Voxel voxel){
  return voxel.location_level.w;
}

vec3 get_location(VoxelObject object, Voxel voxel){
  vec3 extent = object.maxs.xyz - object.mins.xyz;
  vec3 voxmins = vec3(voxel.location_level.xyz) / vec3(object.resolution);
  voxmins = object.mins.xyz + (voxmins * extent);
  return voxmins;
}

vec3 get_obj_location(VoxelObject obj){
  vec3 ext = obj.maxs.xyz - obj.mins.xyz;

  vec3 origin = obj.mins.xyz + (ext / 2.0);
  vec4 tmp_transformed = obj.model * vec4(origin, 1.0);
  return tmp_transformed.xyz / tmp_transformed.w;
}

void get_mins_maxs(VoxelObject object, Voxel voxel, out vec3 voxmins, out vec3 voxmaxs){
  vec3 extent = object.maxs.xyz - object.mins.xyz;

  vec3 voxel_extent = extent;
  
  if (get_level(voxel) > 0){
    voxel_extent /= vec3(pow(2.0,float(get_level(voxel)))); 
  }
  voxmins = vec3(voxel.location_level.xyz) / vec3(object.resolution);
  voxmins = object.mins.xyz + (voxmins * extent);

  voxmaxs = voxmins + voxel_extent;

}
