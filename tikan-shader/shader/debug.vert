/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#version 450

#include "includes/camera.comp"

//The vertex data we get
layout (location = 0) in vec4 pos;
layout (location = 0) out vec4 view_loc;

//The camera we use
layout (set = 0, binding = 0) uniform PrimaryCamera{
  Camera camera;
}cam;

//The drawing parameters
layout (push_constant) uniform PC{
  //Object tranform
  mat4 transform;
  vec4 color;
}pc;

void main(){

  vec4 location = cam.camera.transform_inverse * pc.transform * pos;
  view_loc = location;
  gl_Position = cam.camera.projection * location;
}
