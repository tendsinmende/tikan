/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#version 450

layout (location = 0) in vec4 view_loc;
layout (location = 0) out vec4 uFragColor;

//The drawing parameters
layout (push_constant) uniform PC{
  //Object tranform
  mat4 transform;
  vec4 color;
}pc;

void main(){
  uFragColor = vec4(view_loc.xyz, 1.0);
  //uFragColor = pc.color;
}
