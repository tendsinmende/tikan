/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::io::{Read, Write};

extern crate shaderc;
///TODO implement compile through shaderc

///Proivdes the standard shader director.
const SHADER_DIR_PATH: &str = "tikan-shader/shader";

fn include(
    file_name: &str,
    _include_type: shaderc::IncludeType,
    _target_shader: &str,
    _size: usize,
) -> core::result::Result<shaderc::ResolvedInclude, String> {
    //Load the file given in the include and load it
    let mut content = String::new();
    let mut include_file = std::fs::File::open(&(SHADER_DIR_PATH.to_string() + "/" + file_name))
        .expect("Failed to open shader include file!");
    include_file
        .read_to_string(&mut content)
        .expect("Could not read include file to shader!");

    Ok(shaderc::ResolvedInclude {
        resolved_name: String::from(file_name),
        content,
    })
}

fn to_spath(name: &str) -> String {
    SHADER_DIR_PATH.to_string() + "/" + name
}

fn compile_shader(name: &str, out_name: &str, shader_type: shaderc::ShaderKind) {
    let file_path = SHADER_DIR_PATH.to_string() + "/" + name;

    println!("Compiling {} ...", file_path);

    let mut file = std::fs::File::open(&file_path).expect("Failed to read shader!");
    let mut source = String::new();
    file.read_to_string(&mut source)
        .expect("Failed to read shader!");

    let mut compiler = shaderc::Compiler::new().expect("Failed to start shaderc compiler!");

    let mut compile_options =
        shaderc::CompileOptions::new().expect("Failed to create compile options!");
    compile_options.add_macro_definition("EP", Some("main"));
    //Always go for performance
    compile_options.set_optimization_level(shaderc::OptimizationLevel::Performance);
    compile_options.set_include_callback(include);

    println!("COMPILING!");
    let result = compiler
        .compile_into_spirv(
            &source,
            shader_type,
            name,
            "main", //always entry at main atm.
            Some(&compile_options),
        )
        .expect("Failed to compile shader!");
    println!("FINISHED");
    if result.get_num_warnings() > 0 {
        println!("\tWarnings: {}", result.get_warning_messages());
    }

    let mut out_file =
        std::fs::File::create(&to_spath(out_name)).expect("Could not create spv file!");
    let len = out_file
        .write(result.as_binary_u8())
        .expect("Failed to write out binary spv file");
    if len != result.as_binary_u8().len() {
        println!("Failed to write out shader binary!");
    }

    /*
       let assembly = compiler.compile_into_spirv_assembly(
           &source,
           shader_type,
           name,
           "main", //always entry at main atm.
           Some(&compile_options)
       ).expect("Failed to gen assemply");
       println!("txt: \n{}", assembly.as_text());
    */
}

//Tries to compile all shaders via glslang
fn main() {
    compile_shader(
        "primary_pass.comp",
        "primary_pass.spv",
        shaderc::ShaderKind::Compute,
    );
    compile_shader(
        "ray_assemble_pass.comp",
        "ray_assemble_pass.spv",
        shaderc::ShaderKind::Compute,
    );
    compile_shader(
        "merge_pass.comp",
        "merge_pass.spv",
        shaderc::ShaderKind::Compute,
    );

    compile_shader(
        "radiance_pass.comp",
        "radiance_pass.spv",
        shaderc::ShaderKind::Compute,
    );
    compile_shader(
        "radiance_denoiser.comp",
        "radiance_denoiser.spv",
        shaderc::ShaderKind::Compute,
    );
    compile_shader(
        "specular_pass.comp",
        "specular_pass.spv",
        shaderc::ShaderKind::Compute,
    );

    //Debug pass
    compile_shader(
        "debug.frag",
        "debug_frag.spv",
        shaderc::ShaderKind::Fragment,
    );
    compile_shader("debug.vert", "debug_vert.spv", shaderc::ShaderKind::Vertex);
}
