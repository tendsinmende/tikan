/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

const SHADER_DIR_PATH: &str = "tikan-shader/shader";

///Returns the whole path of a shader with the given name.
pub fn get_shader_path(name: &str) -> String {
    SHADER_DIR_PATH.to_string() + "/" + name
}
