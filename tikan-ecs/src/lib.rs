//! #Mecs
//! An macros based ecs system.
//!
//! On my quest to find the nicest version of creating entities with components in rust I
//! now try to employ a simple container system for resources as well as Entity traits
/*
#![feature(trace_macros)]

trace_macros!(true);
*/

pub mod container;
pub use container::*;

///Implements an ecs that can attach a given set component types to an entity.

pub trait AbstractEntity {
    type Data;
    fn pointer(&self) -> &DataPointer<Self::Data>;
}

#[macro_export]
macro_rules! impl_ecs{
    (
        name: $name:ident,
        entity: Entity{
            components: [$(
                $component_name:ident : $component_type:ty
            ),*],
            member: [$(
                $member_name:ident : $member_type:ty
            ),*]
        }
    ) => (
        impl_container!(EcsContainer, $($component_name : $component_type),*);

        ///Indentifies a Entity within the system. The Id can be copied but might become invalid
        /// after time.
        pub type EntityId = u64;

        pub struct Entity{
            id: EntityId,
            data: DataPointer<($($component_type),*)>,
            $(
                $member_name : $member_type
            ),*
        }

        impl AbstractEntity for Entity{
            type Data = ($($component_type),*);
            fn pointer(&self) -> &DataPointer<Self::Data>{
                &self.data
            }
        }

        //The ecs implementation
        pub struct $name{
            container: EcsContainer,
            entities: Vec<Entity>,
            free_ids: Vec<EntityId>,
        }

        impl $name{
            pub fn new() -> Self{
                $name{
                    container: EcsContainer::new(),
                    entities: Vec::new(),
                    free_ids: Vec::new(),
                }
            }

            pub fn new_entity(&mut self, $($member_name : $member_type),*) -> EntityId{
                if self.free_ids.len() > 0{
                    let id = self.free_ids.pop().expect("Could not pop free id");
                    Entity{
                        id,
                        data: self.container.new_slot(),
                        $(
                            $member_name : $member_name
                        ),*
                    };

                    id

                }else{
                    let id = self.entities.len() as u64;
                    self.entities.push(Entity{
                        id,
                        data: self.container.new_slot(),
                        $(
                            $member_name : $member_name
                        ),*
                    });

                    id
                }
            }

            //Lets one access each type easily in the container
            $(
                ///Lets you access the value of this type for the specifies `entity`
                ///Returns None if there was no entity for the given entity_id.
                pub fn $component_name(&mut self, entity_id: &EntityId) -> Option<&mut Option<$component_type>>{
                    let index = if let Some(en) = self.entities.get(*entity_id as usize){
                        en.pointer().index()

                    }else{
                        return None;
                    };

                    self.container.$component_name().get_mut(index)
                }
            )*

            $(
                pub fn $member_name(&mut self, entity_id: &EntityId) -> Option<&mut $member_type>{
                    if let Some(en) = self.entities.get_mut(*entity_id as usize){
                        Some(&mut en.$member_name)
                    }else{
                        return None;
                    }
                }
            )*

            pub fn get(&self, entity_id: &EntityId) -> Option<($(&Option<$component_type>),*)>{
                if let Some(en) = self.entities.get(*entity_id as usize){
                    Some(self.container.get(en.pointer()))
                }else{
                    None
                }
            }

            pub fn get_mut(&mut self, entity_id: &EntityId) -> Option<($(&mut Option<$component_type>),*)>{
                if let Some(en) = self.entities.get(*entity_id as usize){
                    Some(self.container.get_mut(en.pointer()))
                }else{
                    None
                }
            }

            pub fn get_container(&self) -> &EcsContainer{
                &self.container
            }

            pub fn get_container_mut(&mut self) -> &mut EcsContainer{
                &mut self.container
            }
        }
    )
}

#[cfg(test)]
mod ecs_test {
    use crate::*;

    pub struct Vel {
        x: f32,
        y: f32,
    }

    pub struct Pos {
        x: f32,
        y: f32,
    }

    pub struct Name {
        str: String,
    }

    impl_ecs!(
        name: Ecs,
        entity: Entity{
            components: [
                pos: Pos,
                vel: Vel,
                name: Name
            ],
            member: [
                description: String
            ]
        }
    );

    #[test]
    pub fn arbitary_ecs() {
        let mut ecs = Ecs::new();
        let _test_entity = ecs.new_entity("NoiceTestDescription".to_owned());
    }
}
